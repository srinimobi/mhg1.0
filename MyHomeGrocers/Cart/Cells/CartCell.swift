//
//  CartCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/3/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    @IBOutlet weak var wishlistImageView: UIImageView!
    @IBOutlet weak var deleteImageView: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        // containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        //        containerView.layer.shadowColor = UIColor.gray.cgColor
        //        containerView.layer.shadowOpacity = 1.0
        //        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        //        containerView.layer.shadowRadius = 3.5
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        productImageView.layer.cornerRadius = productImageView.frame.size.height/2
        productImageView.layer.masksToBounds = true
        productImageView.layer.borderColor = UIColor.customOrange().cgColor
        productImageView.layer.borderWidth = 5
        
        productTitleLabel.textColor = UIColor.black
        productTitleLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        priceLabel.textColor = UIColor.black
        priceLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        countLabel.textColor = UIColor.white
        countLabel.backgroundColor = UIColor.customOrange()
        countLabel.font = UIFont(name: "Poppins-Medium", size: 20)
        countLabel.layer.cornerRadius = countLabel.frame.size.height/2
        countLabel.layer.masksToBounds = true
        
        countView.layer.cornerRadius = 12
        countView.layer.masksToBounds = true
        countView.layer.borderColor = UIColor.customOrange().cgColor
        countView.layer.borderWidth = 2
        
        plusButton.setTitleColor(UIColor.customOrange(), for: .normal)
        plusButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 20)
        minusButton.setTitleColor(UIColor.customOrange(), for: .normal)
        minusButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 20)
        
        deleteButton.layer.cornerRadius = deleteButton.frame.size.height/2
        deleteButton.layer.masksToBounds = true
        
        wishlistButton.layer.cornerRadius = wishlistButton.frame.size.height/2
        wishlistButton.layer.masksToBounds = true
    }
    
    func configure(productDict: NSDictionary) {
        productTitleLabel.text = productDict["name"] as? String
        priceLabel.text = productDict["price"] as? String
        countLabel.text = productDict["quantity"] as? String
        
        let imageUrl = productDict["thumb"] as! String
        
        if let imgurl = URL(string: imageUrl){
            self.productImageView.sd_setImage(with: imgurl)
        }
    }
}
