//
//  CartVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/3/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

protocol LeaveStayVCDelegate {
    func clickLeaveToClearTheCart()
}

class CartVC: UIViewController {
    
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var priceDetailsButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var btnWishlist: UIButton!
    @IBOutlet weak var btnClearCart: UIButton!
    
    var authorizationKey = ""
    var cartProductsArr: NSArray = []
    var editingIndex = 0
    var updatedQuantity = 0
    var wishlistIndexes = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
        getCartProducts()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnWishlistClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnClearCartClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LeaveOrStayVC") as! LeaveOrStayVC
        vc.mainLabelDetails = "CLEAR CART"
        vc.subLabelDetails = "Are you sure you want to clear the cart?"
        vc.btnStayDetails = "Clear"
        vc.btnLeaveDetails = "Cancel"
        vc.authorizationKey = authorizationKey
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        // self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func clickLeaveToClearTheCart() {
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.emptyCart(), parameters: nil, authorizationKey: authorizationKey, method: .DELETE, showHud: true, successHandler: { (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = SuccessResponse(json: result as! [String : Any])
                if dictData != nil {
                    if dictData?.statusCode == 1 {
                        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "EmptyCVC") as! EmptyCartVC
                        self.navigationController?.pushViewController(vc ,animated: true)
                        //Alert.showAlert(message: "Your cart has been clearad!")
                    }
                }
                else {
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    @IBAction func checkoutButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShippingModeVC") as! ShippingModeVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setup() {
        bottomView.layer.cornerRadius = 20
        bottomView.layer.masksToBounds = false
        bottomView.clipsToBounds = false
        
        bottomView.layer.shadowColor = UIColor.customOrange().cgColor
        bottomView.layer.shadowOpacity = 1.0
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 2)
        bottomView.layer.shadowRadius = 3.5
        
        priceLabel.textColor = .black
        priceLabel.font = UIFont(name: "Poppins-Bold", size: 15)
        priceDetailsButton.setTitleColor(UIColor.customOrange(), for: .normal)
        priceDetailsButton.titleLabel?.font = UIFont(name: "Poppins-Regular", size: 12)
        
        checkoutButton.backgroundColor = UIColor.customOrange()
        checkoutButton.layer.cornerRadius = 20
        checkoutButton.layer.masksToBounds = true
        checkoutButton.setTitleColor(.white, for: .normal)
        checkoutButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 18)
        
        shareButton.backgroundColor = UIColor.customOrange()
        shareButton.layer.cornerRadius = shareButton.frame.size.height/2
        
        btnWishlist.backgroundColor = UIColor.customOrange()
        btnWishlist.layer.cornerRadius = btnWishlist.frame.size.height/2
        btnWishlist.titleLabel?.textColor = UIColor.white
        btnWishlist.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 10)
        
        btnClearCart.layer.borderColor = UIColor.customOrange().cgColor
        btnClearCart.layer.cornerRadius = btnClearCart.frame.size.height/2
        btnClearCart.layer.borderWidth = 2
        btnClearCart.titleLabel?.textColor = UIColor.customOrange()
        btnClearCart.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 10)
    }
    
    func getCartProducts() {
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.getCartProducts(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = CartResponse(json: result as! [String : Any])
                if dictData != nil {
                    self.cartProductsArr = dictData!.data.products
                    
                    // * Total cart items price
                    let double = dictData?.data.total_raw["value"] as! Double
                    self.priceLabel.text = "$" + double.description
                    self.tableView.reloadData()
                }
                else {
                    // * Failure
                    if dict["statusCode"] as! Int == 1 {
                        let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                            (alert: UIAlertAction!) in
                            //Alert.showAlert(message: "No cart items are availabel!")
                            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EmptyCVC") as! EmptyCartVC
                            DispatchQueue.main.async {
                                self.navigationController?.pushViewController(vc ,animated: true)
                            }
                        }))
                        Alert.showAlertforOkAction(message: "No cart items are availabel!", okAction: okAction)
                    }
                    else {
                        Alert.showAlert(message: "Unknown error, Please try again!")
                    }
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension CartVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartProductsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        
        cell.configure(productDict: cartProductsArr[indexPath.row] as! NSDictionary)
        
        cell.plusButton.tag = indexPath.row
        cell.plusButton.addTarget(self, action: #selector(plusButtonTapped(_:)), for: .touchUpInside)
        
        cell.minusButton.tag = indexPath.row
        cell.minusButton.addTarget(self, action: #selector(minusButtonTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 152
    }
    
    @objc func plusButtonTapped(_ sender: UIButton) {
        print("PLUS : ", sender.tag)
        self.showHUD()
        AddDeleteItemFromCart.CallAPI_AddItemToCart(authorizationKey: authorizationKey, productDict: cartProductsArr[sender.tag] as! NSDictionary, productAddToCartFlag: false) { (result) -> () in
            if (result) {
                self.getCartProducts()
            }
            self.dismissHUD(isAnimated: true)
        }
    }
    
    @objc func minusButtonTapped(_ sender: UIButton){
        print("MINUS : ", sender.tag)
        self.showHUD()
        AddDeleteItemFromCart.CallAPI_DeleteItemToCart(authorizationKey: authorizationKey, productDict: cartProductsArr[sender.tag] as! NSDictionary, productDeleteFromCartFlag: false) { (result) -> () in
            if (result) {
                self.getCartProducts()
            }
            self.dismissHUD(isAnimated: true)
        }
    }
}
