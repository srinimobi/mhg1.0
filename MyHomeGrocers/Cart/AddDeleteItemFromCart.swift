//
//  AddDeleteItemFromCart.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 11/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class AddDeleteItemFromCart: NSObject {
    static func CallAPI_AddItemToCart(authorizationKey: String, productDict: NSDictionary, productAddToCartFlag: Bool, completion: @escaping (Bool) -> ()) {
        print(authorizationKey)
        let parameters: [String: Any]
        if productAddToCartFlag {
            parameters = [
                "product_id": productDict["id"] as Any,
                "quantity": "1",
                "option":[]] as [String: Any]
        }
        else {
            parameters = [
                "product_id": productDict["product_id"] as Any,
                "quantity": "1",
                "option":[]] as [String: Any]
        }
        
        WebService.parseData(urlStr: API.addItemToCart(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                completion(false)
            }
            else {
                let dictData = AddItemCartResponse(json: result as! [String : Any])
                if dictData != nil {
                    completion(true)
                }
                else {
                    // * Failure
                    completion(false)
                }
            }
        }) { (errMsg) in
            completion(false)
        }
    }
    
    static func CallAPI_DeleteItemToCart(authorizationKey: String, productDict: NSDictionary, productDeleteFromCartFlag: Bool, completion: @escaping (Bool) -> ()) {
        print(authorizationKey)
        let parameters: [String: Any]
        if productDeleteFromCartFlag {
            parameters = [
                "key": productDict["id"] as Any] as [String: Any]
        }
        else {
            parameters = [
                "key": productDict["cart_id"] as Any] as [String: Any]
        }
        
        WebService.parseData(urlStr: API.deleteCartItem(), parameters: parameters, authorizationKey: authorizationKey, method: .DELETE, showHud: true, successHandler: { (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                completion(false)
            }
            else {
                let dictData = DeleteCartItemResponse(json: result as! [String : Any])
                if dictData != nil {
                    completion(true)
                }
                else {
                    // * Failure
                    completion(false)
                }
            }
        }) { (errMsg) in
            completion(false)
        }
    }
}
