//
//  EmptyCartVC.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 19/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class EmptyCartVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSopNow: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSopNow.layer.cornerRadius = 25
        btnSopNow.layer.borderColor =  UIColor.white.cgColor
        btnSopNow.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 24)
        
        btnBack.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 18)
        mainLabel.font = UIFont(name: "Poppins-Bold", size: 28)
        subLabel.font = UIFont(name: "Poppins-Medium", size: 16)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        /*let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
         vc.authorizationKey = authorizationKey
         self.navigationController?.pushViewController(vc, animated: false)*/
    }
    
    @IBAction func btnShopNow(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashboardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
