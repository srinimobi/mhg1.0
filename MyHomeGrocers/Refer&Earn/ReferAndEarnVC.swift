//
//  ReferAndEarnVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 25/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ReferAndEarnVC: UIViewController {
    
    var authorizationKey = ""
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblReferAndEarn: UILabel!
    @IBOutlet weak var lblCredits: UILabel!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var lblImproveCredits: UILabel!
    @IBOutlet weak var btnReferNow: UIButton!
    @IBOutlet weak var lblTitle2: UILabel!
    
    override func viewDidLoad() {
      
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        
    }
    
    func setup() {
        
        btnProfile.layer.cornerRadius = 20
        btnProfile.layer.masksToBounds = true
        btnProfile.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 10)
        
        
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.shadowOpacity = 0.8
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 2
        
        
        
        lblReferAndEarn.font = UIFont(name: "Poppins-Semibold", size: 24)
        
        lblImproveCredits.backgroundColor = UIColor.white
        lblImproveCredits.layer.cornerRadius = lblImproveCredits.frame.size.height/2
        
        containerView2.layer.masksToBounds = false
        containerView2.clipsToBounds = false
        containerView2.layer.cornerRadius = containerView2.frame.size.height/2
        containerView2.layer.shadowColor = UIColor.gray.cgColor
        containerView2.layer.shadowOpacity = 0.8
        containerView2.layer.shadowRadius = 5
        containerView2.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        
        
        lblImproveCredits.font = UIFont(name: "Poppins-Medium", size: 12)
        lblImproveCredits.textColor = UIColor.customOrange()
        lblImproveCredits.layer.masksToBounds = true
        
        lblCredits.backgroundColor = UIColor.lightOrange()
        lblCredits.layer.cornerRadius = lblCredits.frame.size.height/2
        lblCredits.layer.borderColor = UIColor.customOrange().cgColor
        lblCredits.layer.borderWidth = 2
        lblCredits.textColor = UIColor.customOrange()
        lblCredits.font = UIFont(name: "Poppins-Medium", size: 12)
        lblCredits.layer.masksToBounds = true

        
        btnReferNow.backgroundColor = UIColor.customOrange()
        btnReferNow.layer.cornerRadius = btnReferNow.frame.size.height/2
        btnReferNow.backgroundColor = UIColor.customOrange()
        btnReferNow.setTitleColor(UIColor.white, for: .normal)
        btnReferNow.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 11)
        btnReferNow.layer.masksToBounds = true
        
        
        lblTitle2.font = UIFont(name: "Poppins-Semibold", size: 24)
        
    }
    

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func btnProfileClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SidePanelVC") as! SidePanelVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
     }
    
    @IBAction func btnSearchClicked(_ sender: Any) {
        
    }
    
    
    
    /*
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIView {
func addBottomShadow() {
    layer.masksToBounds = false
    layer.shadowRadius = 2
    layer.shadowOpacity = 1
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOffset = CGSize(width: 0 , height: 2)
    layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                 y: bounds.maxY - layer.shadowRadius,
                                                 width: bounds.width,
                                                 height: layer.shadowRadius)).cgPath
}
}
