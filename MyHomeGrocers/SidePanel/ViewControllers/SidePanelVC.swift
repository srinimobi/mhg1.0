//
//  SidePanelVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/25/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class SidePanelVC: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deliverLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    let titlesArray = ["My Orders","Wishlist","Wallet","Address Details","Refer and Earn","MHG Rewards","FAQ","Legal","Logout"]
    let imageArray = [UIImage(named: "1. MyOrders-active"),UIImage(named: "wishlist"),UIImage(named: "wallet"),UIImage(systemName: "mappin.and.ellipse"),UIImage(systemName: "person.and.person"),UIImage(named: "money-active"),UIImage(systemName: "bubble.left.and.bubble.right.fill"),UIImage(systemName: "scroll"),UIImage(named: "logout-active")]
    
    var authorizationKey = ""
    var customerName = ""
    var customerEmail = ""
    var customerPhoneNumber = ""
    var customerPinCode = ""
    var isFromUpdateProfile = false
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
        getAccountDetails()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        if isFromUpdateProfile {
            getAccountDetails()
        }
    }
    
    func setup() {
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 3.5
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderColor = UIColor.customOrange().cgColor
        profileImageView.layer.borderWidth = 5
        
        nameLabel.textColor = UIColor.customOrange()
        nameLabel.font = UIFont(name: "Poppins-SemiBold", size: 16)
        
        mailLabel.textColor = UIColor.black
        mailLabel.font = UIFont(name: "Poppins-Regular", size: 11)
        
        phoneNumberLabel.textColor = UIColor.black
        phoneNumberLabel.font = UIFont(name: "Poppins-Regular", size: 11)
        
        deliverLabel.textColor = UIColor.customOrange()
        deliverLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        
        editButton.layer.cornerRadius = editButton.frame.size.height/2
        editButton.layer.masksToBounds = true
        editButton.addTarget(self, action: #selector(editButtonClicked), for: .touchUpInside)
        
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        /*let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)*/
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editButtonClicked() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getAccountDetails() {
        print(authorizationKey)
        WebService.parseData(urlStr: API.accountDetails(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { [self] (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = AccountDetailsResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    self.nameLabel.text = (dictData?.data.firstname)! + " " + (dictData?.data.lastname)!
                    self.mailLabel.text = dictData?.data.email
                    self.phoneNumberLabel.text = dictData?.data.telephone
                    
                    customerName = (dictData?.data.firstname)!
                    let fullString = "Deliver to \(customerName) - Location Pincode"
                    let termsString = "\(customerName)"
                    let rangeOfTermsString = (fullString as NSString).range(of: termsString)
                    let attributedString = NSMutableAttributedString(string:fullString)
                    attributedString.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.customOrange(), NSAttributedString.Key.font:  UIFont(name: "Poppins-Bold", size: 14) as Any], range: rangeOfTermsString)
                    deliverLabel.attributedText = attributedString
                }
                
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
        }) { (errMsg) in
            Alert.showAlert(message: errMsg)
        }
    }
    
    func fillDetails() {
        nameLabel.text = customerName
        phoneNumberLabel.text = customerPhoneNumber
        mailLabel.text = customerEmail
        
        let fullString = "Deliver to \(customerName)"
        let termsString = "\(customerName)"
        let rangeOfTermsString = (fullString as NSString).range(of: termsString)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.customOrange(), NSAttributedString.Key.font:  UIFont(name: "Poppins-Bold", size: 14) as Any], range: rangeOfTermsString)
        
        deliverLabel.attributedText = attributedString
    }
}

extension SidePanelVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SidePanelCell", for: indexPath) as! SidePanelCell
        cell.titleLabel.text = titlesArray[indexPath.row]
        cell.iconImageView.image = imageArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrdersListVC") as! OrdersListVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "WishlistVC") as! WishlistVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
            
        /*  case 2:
         let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
         vc.authorizationKey = self.authorizationKey
         self.navigationController?.pushViewController(vc, animated: true)
         */
        case 3:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 4:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ReferAndEarnVC") as! ReferAndEarnVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
            
        /*   case 5:
         let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "RewardsVC") as! RewardsVC
         vc.authorizationKey = self.authorizationKey
         self.navigationController?.pushViewController(vc, animated: true)
         
         case 6:
         let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
         vc.authorizationKey = self.authorizationKey
         self.navigationController?.pushViewController(vc, animated: true)
         
         case 7:
         let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "LegalInfoVC") as! LegalInfoVC
         vc.authorizationKey = self.authorizationKey
         self.navigationController?.pushViewController(vc, animated: true) */
        
        case 8:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
        default: break
        }
    }
}
