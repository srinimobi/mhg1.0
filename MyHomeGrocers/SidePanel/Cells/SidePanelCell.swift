//
//  SidePanelCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/25/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class SidePanelCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      /*  iconImageView.layer.borderColor = UIColor.black.cgColor
        iconImageView.layer.borderWidth = 2
        iconImageView.layer.cornerRadius = 12
        iconImageView.layer.masksToBounds = true    */
        
        titleLabel.textColor = UIColor.customOrange()
        titleLabel.font = UIFont(name: "Poppins-Bold", size: 20)
        iconImageView.tintColor = UIColor.customOrange()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
