//
//  LegalInfoVC.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 26/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class LegalInfoVC: UIViewController {
    
    var authorizationKey = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
