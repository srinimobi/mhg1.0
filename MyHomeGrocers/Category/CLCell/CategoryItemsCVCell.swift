//
//  CategoryItemsCVCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryItemsCVCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var wishlistView: UIView!
    @IBOutlet weak var productContainer: UIView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var gotoCartButton: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var outOfStockLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        descriptionLabel.font = UIFont(name: "Poppins-Medium", size: 10)
        descriptionLabel.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        
        quantityLabel.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        quantityLabel.font = UIFont(name: "Poppins-SemiBold", size: 10)
        
        priceLabel.textColor = UIColor.customOrange()
        priceLabel.font = UIFont(name: "Poppins-Medium", size: 14)
        
        btnNew.layer.borderWidth = 0.0
        btnNew.layer.cornerRadius = 8.0
        
        contentView.layer.cornerRadius = 10.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        contentView.layer.masksToBounds = true
        
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 120.0)//CGSizeMake(0, 2.0);
        layer.shadowRadius = 125.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:bounds, cornerRadius:contentView.layer.cornerRadius).cgPath
        
        productContainer.layer.borderColor = UIColor.customOrange().cgColor
        productContainer.layer.borderWidth = 5
        productContainer.layer.cornerRadius = productContainer.frame.size.height/2
        productContainer.layer.masksToBounds = false
        productContainer.clipsToBounds = false
        
        wishlistView.layer.cornerRadius = wishlistView.frame.size.height/2
        wishlistView.layer.masksToBounds = false
        wishlistView.clipsToBounds = false
        
        percentageLabel.layer.cornerRadius = percentageLabel.frame.size.height/2
        percentageLabel.layer.masksToBounds = true
        //percentageLabel.backgroundColor = UIColor.customOrange()
        percentageLabel.font = UIFont(name: "Poppins-SemiBold", size: 10)
        percentageLabel.textColor = .white
        
        gotoCartButton.layer.cornerRadius = 12
        gotoCartButton.layer.masksToBounds = true
        gotoCartButton.backgroundColor = UIColor.lightOrange()
        gotoCartButton.setTitleColor(UIColor.customOrange(), for: .normal)
        gotoCartButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 11)
        
        // * Out Of Stock
        outOfStockLabel.layer.borderWidth = 3.0
        outOfStockLabel.layer.borderColor = UIColor.customOrange().cgColor
        outOfStockLabel.layer.cornerRadius = 8.0
    }
    
    func configureProducts(products: NSDictionary) {
      //  productImage.image = products["image"] as? UIImage
        titleLabel.text = products["name"] as? String
        priceLabel.text = products["price"] as? String
        quantityLabel.text = (products["defaultQuantity"] as? String)! + " " + "kg - Pouch"
        descriptionLabel.text = products["description"] as? String
        
        let discountString = "\(products["discount"] ?? 0)"
         percentageLabel.text = discountString + "" + "%"
        
         /*let imageUrl = products["image"] as! String
         if let imgurl = URL(string: imageUrl){
         self.productImage.sd_setImage(with: imgurl)
         }*/
    
        outOfStockLabel.isHidden = true
        if products["out_of_stock"] as! Bool != false {
            outOfStockLabel.isHidden = false
        }
    }
}
