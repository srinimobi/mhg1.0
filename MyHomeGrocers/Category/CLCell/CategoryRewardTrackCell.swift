//
//  CategoryRewardTrackCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryRewardTrackCell: UITableViewCell {

    @IBOutlet weak var rewardsView: UIView!
    @IBOutlet weak var trackView: UIView!
    @IBOutlet weak var lblRewards: UILabel!
    @IBOutlet weak var btnKnowMore: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var circleView: UIView!
    
    let btnAttributes: [NSAttributedString.Key: Any] = [
          .font: UIFont.systemFont(ofSize: 14),
          .foregroundColor: UIColor.customOrange(),
          .underlineStyle: NSUnderlineStyle.single.rawValue
      ] // .double.rawValue, .thick.rawValue
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rewardsView.layer.borderWidth = 0.0
        rewardsView.layer.cornerRadius = 5.0
        
        rewardsView.layer.shadowColor = UIColor.red.cgColor
        rewardsView.layer.shadowOpacity = 1
        rewardsView.layer.shadowOffset = .zero
        rewardsView.layer.shadowRadius = 5
        
        trackView.layer.borderWidth = 2.0
        trackView.layer.borderColor = UIColor.systemGreen.cgColor
        trackView.layer.cornerRadius = 25.0
        
        lblRewards.layer.masksToBounds = true
        lblRewards.layer.borderWidth = 0.0
        lblRewards.layer.cornerRadius = 20.0
        
        
        statusView.layer.borderWidth = 0.0
        statusView.layer.cornerRadius = 17.5
        
        circleView.layer.borderWidth = 2.0
        circleView.layer.borderColor = UIColor.systemGreen.cgColor
        circleView.layer.cornerRadius = 13
        
        let attributeString = NSMutableAttributedString(
                string: "knowmore",
                attributes: btnAttributes
             )
        btnKnowMore.setAttributedTitle(attributeString, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
