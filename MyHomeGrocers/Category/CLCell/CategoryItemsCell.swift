//
//  CategoryItemsCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryItemsCell: UITableViewCell {
    
    @IBOutlet weak var categoryItemsCollectionView: UICollectionView!
    
    var productsArr: NSArray = []
    
    weak var delegate: CategoryLevelVC?
    var authorizationKey = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(product: NSArray) {
        productsArr = product
        categoryItemsCollectionView.reloadData()
    }
}

extension CategoryItemsCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryItemsCVCellI", for: indexPath) as! CategoryItemsCVCell
        cell.configureProducts(products: productsArr[indexPath.item] as! NSDictionary)
        
        cell.gotoCartButton.tag = indexPath.item
        cell.gotoCartButton.addTarget(self, action: #selector(goToCartButtonTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 30, height: 302)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    @objc func goToCartButtonTapped(_ sender: UIButton){
        print("GO TO CART : ", sender.tag)
        AddDeleteItemFromCart.CallAPI_AddItemToCart(authorizationKey: authorizationKey, productDict: productsArr[sender.tag] as! NSDictionary, productAddToCartFlag: false) { (result) -> () in
            if (result) {
                // Do action
                //self.delegate?.getCategoriesData()
            }
        }
    }
}
