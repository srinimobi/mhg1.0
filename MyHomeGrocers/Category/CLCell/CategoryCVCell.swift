//
//  CategoryCVCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.customOrange()
        
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        titleLabel.textColor = UIColor.white
        
        layer.borderWidth = 0.0
        layer.cornerRadius = 8.0
    }
    
    func configure(categorie: NSDictionary) {
        titleLabel.text = categorie["name"] as? String
    }
}
