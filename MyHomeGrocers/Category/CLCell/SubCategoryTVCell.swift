//
//  SubCategoryTVCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SubCategoryTVCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        headerLabel.font = UIFont(name: "Poppins-Medium", size: 13)
        headerLabel.textColor = UIColor.customOrange()
        
        containerView.layer.borderColor = UIColor.customOrange().cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 12
        containerView.layer.masksToBounds = true
        
        viewAllButton.layer.cornerRadius = 20
        viewAllButton.layer.masksToBounds = true
        viewAllButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 13)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension SubCategoryTVCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCVCellI", for: indexPath) as! SubCategoryCVCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 16, height: 66)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
