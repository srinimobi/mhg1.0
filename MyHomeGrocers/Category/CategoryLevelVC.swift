//
//  CategoryLevelVC.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryLevelVC: UIViewController {

    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnShortBy: UIButton!
    @IBOutlet weak var categoryLevelCollectionView: UICollectionView!
    @IBOutlet weak var categoryLevelTableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    
    var categoryId = ""
    var authorizationKey = ""
    var categoriesArr: NSArray = []
    var productsArr: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getCategoriesData()
        
        btnProfile.layer.masksToBounds = true
        btnProfile.layer.cornerRadius = 23.0
        
        btnShortBy.layer.masksToBounds = true
        btnShortBy.layer.cornerRadius = 8.0
    }
    
    @IBAction func doPressBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doPressProfileButton(_ sender: Any) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getCategoriesData() {
        let param = [
            "category_id": categoryId,
            "per_page":"10",
            "page_number":"5"
        ] as [String: Any]
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.categoryProduct(), parameters: param, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { [self] (result) in
            print("CategoryProductData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                // * New model + JSON Datat initilized
                let dictData = CategoryProductResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    if let categories = dictData?.data.categories {
                        print("categories:\(categories)")
                        self.categoriesArr = categories
                    }
                    if let products = dictData?.data.products {
                        print("products:\(products)")
                        self.productsArr = products
                    }
                    DispatchQueue.main.async {
                        self.categoryLevelCollectionView.reloadData()
                        self.categoryLevelTableView.reloadData()
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension CategoryLevelVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryItemsCellI", for: indexPath) as! CategoryItemsCell
            cell.configure(product: productsArr)
            cell.authorizationKey = authorizationKey
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCardCellI", for: indexPath) as! CategoryCardCell
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTVCellI", for: indexPath) as! SubCategoryTVCell
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTVCellI", for: indexPath) as! SubCategoryTVCell
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryRewardTrackCellI", for: indexPath) as! CategoryRewardTrackCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 592
        case 1:
            return 200
        case 2,3:
            return 320
        case 4:
            return 150
        default:
            return 44
        }
    }
}

extension CategoryLevelVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCVCellI", for: indexPath) as! CategoryCVCell
            cell.configure(categorie: categoriesArr[indexPath.item] as! NSDictionary)
        return cell
    }
}
