//
//  ShippingModeCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ShippingModeCell: UITableViewCell {
    
    @IBOutlet weak var pickupButton: UIButton!
    @IBOutlet weak var deliveryButton: UIButton!
    @IBOutlet weak var shippingModelabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        //containerView.layer.cornerRadius = 25
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        shippingModelabel.textColor = UIColor.black
        //shippingModelabel.font = UIFont(name: "Poppins-Regular", size: 15)
        
        deliveryButton.layer.cornerRadius = 12
        deliveryButton.layer.masksToBounds = true
        //deliveryButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 13)
        pickupButton.layer.cornerRadius = 12
        pickupButton.layer.masksToBounds = true
        //pickupButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 13)
        
        pickupButton.addTarget(self, action: #selector(pickupButtonTapped), for: .touchUpInside)
        deliveryButton.addTarget(self, action: #selector(deliveryButtonTapped), for: .touchUpInside)
    }
    
    @objc func pickupButtonTapped() {
        pickupButton.backgroundColor = UIColor.customOrange()
        pickupButton.setTitleColor(.white, for: .normal)
        
        deliveryButton.backgroundColor = UIColor.lightOrange()
        deliveryButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        NotificationCenter.default.post(name: Notification.Name("RefreshShippingModeCell"), object: nil, userInfo: nil)
    }
    
    @objc func deliveryButtonTapped() {
        deliveryButton.backgroundColor = UIColor.customOrange()
        deliveryButton.setTitleColor(.white, for: .normal)
        pickupButton.backgroundColor = UIColor.lightOrange()
        pickupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        NotificationCenter.default.post(name: Notification.Name("RefreshShippingModeCell"), object: nil, userInfo: nil)
    }
}
