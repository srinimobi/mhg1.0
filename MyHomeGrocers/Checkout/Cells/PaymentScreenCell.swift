//
//  PaymentScreenCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class PaymentScreenCell: UITableViewCell {
    
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var savingsValue: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    @IBOutlet weak var savingsView: UIView!
    @IBOutlet weak var rewardBalanceLabel: UILabel!
    @IBOutlet weak var rewardBalanceValue: UILabel!
    @IBOutlet weak var rewardBalanceImage: UIImageView!
    @IBOutlet weak var rewardBalanceButton: UIButton!
    @IBOutlet weak var walletBalanceValue: UILabel!
    @IBOutlet weak var walletBalanceLabel: UILabel!
    @IBOutlet weak var walletBalanceImage: UIImageView!
    @IBOutlet weak var walletBalanceButton: UIButton!
    @IBOutlet weak var shippingValue: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var cartValue: UILabel!
    @IBOutlet weak var cartLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        //containerView.layer.cornerRadius = 25
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        let shadowColor = UIColor.lightGray
        containerView.layer.shadowColor = shadowColor.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        savingsView.layer.cornerRadius = 8
        savingsView.layer.masksToBounds = true
        
        cartLabel.textColor = UIColor.descriptionGray()
        //cartLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        cartValue.textColor = UIColor.descriptionGray()
        //cartValue.font = UIFont(name: "Poppins-Medium", size: 15)
        
        shippingLabel.textColor = UIColor.descriptionGray()
        //shippingLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        shippingValue.textColor = UIColor.descriptionGray()
        //shippingValue.font = UIFont(name: "Poppins-Regular", size: 15)
        
        walletBalanceLabel.textColor = UIColor.descriptionGray()
        //walletBalanceLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        walletBalanceValue.textColor = UIColor.descriptionGray()
        //walletBalanceValue.font = UIFont(name: "Poppins-Medium", size: 15)
        
        rewardBalanceLabel.textColor = UIColor.descriptionGray()
        //rewardBalanceLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        rewardBalanceValue.textColor = UIColor.descriptionGray()
        //rewardBalanceValue.font = UIFont(name: "Poppins-Medium", size: 15)
        
        savingsLabel.textColor = UIColor.descriptionGray()
        //savingsLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        savingsValue.textColor = UIColor.descriptionGray()
        //savingsValue.font = UIFont(name: "Poppins-Medium", size: 15)
        
        totalLabel.textColor = UIColor.black
        //totalLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        totalValue.textColor = UIColor.descriptionGray()
        //totalValue.font = UIFont(name: "Poppins-Medium", size: 15)
    }
}
