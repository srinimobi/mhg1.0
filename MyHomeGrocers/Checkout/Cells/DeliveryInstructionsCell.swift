//
//  DeliveryInstructionsCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 09/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class DeliveryInstructionsCell: UITableViewCell {
    
    @IBOutlet weak var lblDeliveryInstructions: UILabel!
    @IBOutlet weak var containerView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //lblDeliveryInstructions.font = UIFont(name: "Poppins", size: 15)
        
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
