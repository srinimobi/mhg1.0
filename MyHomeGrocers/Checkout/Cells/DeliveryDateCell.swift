//
//  DeliveryDateCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class DeliveryDateCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup() {
//        containerView.layer.cornerRadius = 25
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        dateButton.layer.cornerRadius = 12
        dateButton.layer.masksToBounds = true
        //dateButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 13)
        
        dateLabel.textColor = UIColor.black
        //dateLabel.font = UIFont(name: "Poppins-Regular", size: 15)
    }

}
