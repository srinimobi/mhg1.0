//
//  CardCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup() {
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        let shadowColor = UIColor.lightOrange()
        containerView.layer.shadowColor = shadowColor.cgColor
        containerView.layer.shadowOpacity = 1.0
        //containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 12
        
        cardNumber.textColor = UIColor.descriptionGray()
        //cardNumber.font = UIFont(name: "Poppins-SemiBold", size: 15)
    }

}
