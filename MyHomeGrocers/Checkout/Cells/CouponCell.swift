//
//  CouponCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CouponCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var couponTextField: UITextField!
    
    var couponText = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
        applyButton.addTarget(self, action: #selector(applyCouponTapped), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        //        containerView.layer.cornerRadius = 25
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        applyButton.layer.cornerRadius = 12
        applyButton.layer.masksToBounds = true
        //applyButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 13)
        
        // couponTextField.delegate = self
    }
    
    @objc func applyCouponTapped() {
        // textFieldDidEndEditing(couponTextField)
        couponText = couponTextField.text!
        NotificationCenter.default.post(name: Notification.Name("CouponApplied"), object: nil, userInfo: ["couponId": couponText])
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        couponText = textField.text!
    //    }
}
