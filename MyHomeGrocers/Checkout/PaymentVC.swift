//
//  PaymentVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/8/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editCartButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    var authorizationKey = ""
    var cartValue = ""
    var shippingRate = ""
    var rewardBalance = ""
    var savings = ""
    var totalValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup() {
        bottomView.layer.cornerRadius = 25
        bottomView.layer.masksToBounds = false
        bottomView.clipsToBounds = false
        
        let shadowColor = UIColor.customOrange()
        bottomView.layer.shadowColor = shadowColor.cgColor
        bottomView.layer.shadowOpacity = 1.0
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 25)
        bottomView.layer.shadowRadius = 20
        
        editCartButton.layer.cornerRadius = 16
        editCartButton.layer.masksToBounds = true
        
        placeOrderButton.layer.cornerRadius = 20
        placeOrderButton.layer.masksToBounds = true
        placeOrderButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 18)
    }

    @IBAction func editCartTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func placeOrderTapped(_ sender: Any) {
        
      //  Alert.showAlert(message: "Order has been Placed")
        
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderCVC") as! OrderConfirmationVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PaymentVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentScreenCell") as! PaymentScreenCell
            cell.cartValue.text = cartValue
            cell.rewardBalanceValue.text = rewardBalance
            cell.totalValue.text = totalValue
            cell.savingsValue.text = savings
            cell.shippingValue.text = shippingRate
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodHeaderCell") as! PaymentMethodHeaderCell
            return cell
        
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 247
        case 1:
            return 44
        default:
            return 45
        }
    }
}
