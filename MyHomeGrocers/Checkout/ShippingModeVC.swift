//
//  ShippingModeVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/7/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ShippingModeVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var editCartButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var checkmarkImage: UIImageView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    var authorizationKey = ""
    var resultData: NSDictionary = [:]
    
    var totalsArray: NSArray = []
    var mhgRewardsDict: NSDictionary = [:]
    var shippingMethodsDict: NSDictionary = [:]
    var totalSavings: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
        getCheckoutDetails()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.couponApplied(notification:)),
            name: Notification.Name("CouponApplied"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refreshShippingModeCell),
            name: Notification.Name("RefreshShippingModeCell"),
            object: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        //self.tableView.reloadData()
    }
    
    @objc func refreshShippingModeCell() {
        self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    }
    
    @objc func couponApplied(notification: NSNotification) {
        let couponDict = notification.userInfo! as NSDictionary
        let couponId = couponDict["couponId"] as! String
        let parameters = ["coupon": couponId] as [String: Any]
        self.showHUD()
        WebService.parseData(urlStr: API.applyCoupon(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("CouponData:\(result)")
            let dict = result as! NSDictionary
            if let statusCode = dict["statusCode"] as? Int {
                if statusCode == 1 {
                    Alert.showAlert(message: "Success: Coupon applied.." )
                }
            }
            else if let statusCode = dict["statusCode"] as? Int {
                if statusCode == 0 {
                    Alert.showAlert(message: dict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    func setup() {
        bottomView.layer.cornerRadius = 25
        bottomView.layer.masksToBounds = false
        bottomView.clipsToBounds = false
        
        let shadowColor = UIColor.customOrange()
        bottomView.layer.shadowColor = shadowColor.cgColor
        bottomView.layer.shadowOpacity = 1.0
        bottomView.layer.shadowOffset = CGSize(width: 0, height: 25)
        bottomView.layer.shadowRadius = 20
        
        proceedButton.layer.cornerRadius = 20
        proceedButton.layer.masksToBounds = true
        
        editCartButton.layer.cornerRadius = 16
        editCartButton.layer.masksToBounds = true
        
        proceedButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 18)
    }
    
    @IBAction func editCarttapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsButtonTapped(_ sender: Any) {
    }
    
    @IBAction func proceedButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        vc.authorizationKey = authorizationKey
        /*  vc.cartValue = (((resultData["totals"] as! NSArray)[0] as! NSDictionary)["text"] as? String)!
         vc.savings = String(resultData["total_savings"] as! Int)
         vc.totalValue = (((resultData["totals"] as! NSArray)[0] as! NSDictionary)["text"] as? String)!
         vc.rewardBalance = (((resultData["mhg_rewards"] as! NSDictionary)["amount"] as? String)!)
         vc.shippingRate = (((((resultData["shipping_methods"] as! NSDictionary)["flat"] as! NSDictionary)["quote"] as! NSDictionary)["flat"] as! NSDictionary)["text"] as? String)!  */
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCheckoutDetails() {
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.getCheckoutDetails(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { (result) in
            print("WishlistData:\(result)")
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = CheckoutResponse(json: result as! [String : Any])
                if dictData != nil {
                    self.totalsArray = dictData!.data.totals
                    self.mhgRewardsDict = dictData!.data.mhg_rewards
                    self.shippingMethodsDict = dictData!.data.shipping_methods
                    //self.totalSavings = dictData!.total_savings
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            /*let dict = result as! NSDictionary
            if let resultDict = dict["data"] as? NSDictionary {
                self.resultData = resultDict
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            else if let statusCode = dict["statusCode"] as? Int {
                if statusCode == 0 {
                    Alert.showAlert(message: dict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }){ (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension ShippingModeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponCell") as! CouponCell
            cell.couponTextField.delegate = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
            /*print(resultData)
            if resultData != [:] {
                cell.cartValue.text = ((resultData["totals"] as! NSArray)[0] as! NSDictionary)["text"] as? String
                cell.shippingValue.text = ((((resultData["shipping_methods"] as! NSDictionary)["flat"] as! NSDictionary)["quote"] as! NSDictionary)["flat"] as! NSDictionary)["text"] as? String
                cell.rewardBalanceValue.text = ((resultData["mhg_rewards"] as! NSDictionary)["amount"] as? String)
                cell.savingsValue.text = String(resultData["total_savings"] as! Int)
                cell.totalValue.text = ((resultData["totals"] as! NSArray)[0] as! NSDictionary)["text"] as? String
            }*/
            cell.cellCofiguration(totals: totalsArray, MHGRewards: mhgRewardsDict, shippingMethods: shippingMethodsDict, totalSavings: totalSavings)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShippingModeCell") as! ShippingModeCell
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryAddressCell") as! DeliveryAddressCell
            cell.addressLabel.text = selectedAddress
            cell.changeButton.addTarget(self, action: #selector(changeAddressTapped), for: .touchUpInside)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryDateCell") as! DeliveryDateCell
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryInstructionsCell") as! DeliveryInstructionsCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 58
        case 1:
            return 247
        case 5:
            return 180
        default:
            return 110
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
    @objc func changeAddressTapped() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
