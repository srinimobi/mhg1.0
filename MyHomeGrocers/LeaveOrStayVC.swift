//
//  LeaveOrStayVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 24/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class LeaveOrStayVC: UIViewController {
    
    weak var delegate: CartVC?

    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var btnLeave: UIButton!
    @IBOutlet weak var btnStay: UIButton!
    @IBOutlet weak var horizontalStackView: UIStackView!
    @IBOutlet weak var verticalStackView: UIStackView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    var authorizationKey = ""
    var mainLabelDetails : String = ""
    var subLabelDetails : String = ""
    var btnStayDetails : String = ""
    var btnLeaveDetails : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        viewAlert.layer.cornerRadius = 20
        viewAlert.layer.masksToBounds = true
        horizontalStackView.layer.borderWidth = 2
        horizontalStackView.layer.borderColor = UIColor.white.cgColor
        horizontalStackView.layer.cornerRadius = horizontalStackView.frame.size.height/2
        horizontalStackView.layer.masksToBounds = true
        
        btnStay.layer.masksToBounds = true
        btnLeave.layer.masksToBounds = true
        btnLeave.titleLabel?.font = UIFont (name: "Poppins-Semibold", size: 20)
        btnStay.titleLabel?.font = UIFont (name: "Poppins-Semibold", size: 20)
        
        mainLabel.font = UIFont(name: "Poppins-Semibold", size: 20)
        subLabel.font = UIFont(name: "Poppins-Medium", size: 16)
        mainLabel.text = mainLabelDetails
        subLabel.text = subLabelDetails
        btnStay.setTitle(btnStayDetails, for: .normal)
        btnLeave.setTitle(btnLeaveDetails, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLeaveClicked(_ sender: Any) {
        if mainLabel.text == "CLEAR CART" {
            self.dismiss(animated: true, completion: nil)
        }
        else  if mainLabel.text == "Are you sure you want to leave?" {
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SidePanelVC") as! SidePanelVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func btnStayClicked(_ sender: Any) {
        if mainLabel.text == "Are you sure you want to leave?" {
            self.dismiss(animated: true, completion: nil)}
        else  if mainLabel.text == "CLEAR CART"{
            
            /*let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EmptyCVC") as! EmptyCartVC
            vc.authorizationKey = authorizationKey
            self.navigationController?.pushViewController(vc ,animated: true)
            Alert.showAlert(message: "Your cart has been clearad!")*/
            
            self.dismiss(animated: true, completion: nil)
            delegate?.clickLeaveToClearTheCart()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
