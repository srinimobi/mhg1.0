//
//  WishlistVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/15/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class WishlistVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareButton: UIButton!
    
    var authorizationKey = ""
    var wishlistProductsArr: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        shareButton.layer.cornerRadius = shareButton.frame.size.height/2
        shareButton.backgroundColor = UIColor.customOrange()
        shareButton.layer.masksToBounds = true
        
        getWishlistProducts()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getWishlistProducts() {
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.getWishlistProducts(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { (result) in
            
            print("WishlistData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = WishlistResponse(json: result as! [String : Any])
                if dictData != nil {
                    self.wishlistProductsArr = dictData!.data
                    self.tableView.reloadData()
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension WishlistVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistProductsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistTableCell", for: indexPath) as! WishlistTableCell
        
        //   cell.configure(productDict: wishlistProductsArr[indexPath.row] as! NSDictionary)
        //   cell.cartButton.tag = indexPath.row
        //   cell.cartButton.addTarget(self, action: #selector(addItemToCart(sender:)), for: .touchUpInside)
        //   cell.plusButton.addTarget(self, action: #selector(plusButtonTapped(sender:)), for: .touchUpInside)
        //   cell.minusButton.addTarget(self, action: #selector(minusButtonTapped(sender:)), for: .touchUpInside)
        //   cell.deleteButton.addTarget(self, action: #selector(deleteButtonTapped(sender:)), for: .touchUpInside)
        
        cell.configure(productDict: wishlistProductsArr[indexPath.row] as! NSDictionary)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexpath: IndexPath) {
        switch indexpath.row {
        default:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*   @objc func addItemToCart(sender: UIButton) {
     let product = wishlistProductsArr[sender.tag] as! NSDictionary
     let productId = product["product_id"] as! String
     
     let parameters = [
     "product_id": productId,
     "quantity": "1",
     "option": []
     ] as [String: Any]
     
     WebService.parseData(urlStr: API.addItemToCart(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
     print("cartData:\(result)")
     let dict = result as! NSDictionary
     if let resultDict = dict["data"] as? NSDictionary {
     if let statusDict = dict["status"] as? NSDictionary {
     //                    if statusDict["code"] as! Int == 1 {
     //
     //                            self.wishlistProductsArr = resultDict
     //
     //                        DispatchQueue.main.async {
     //                            self.tableView.reloadData()
     //                        }
     //                    }
     if dict["statusCode"] as! Int == 1 {
     Alert.showAlert(message: dict["message"] as! String)
     }
     }
     }
     
     else if let errorDict = dict["error"] as? NSDictionary {
     if errorDict["password"] != nil {
     Alert.showAlert(message: errorDict["password"] as! String)
     }
     }  else if let statusDict = dict["status"] as? NSDictionary {
     if dict["statusCode"] as! Int == 0 {
     Alert.showAlert(message: statusDict["message"] as! String)
     }
     }
     
     else {
     Alert.showAlert(message: "Unknown error, Please try again!")
     }
     
     
     }) { (errMsg) in
     Alert.showAlert(message: errMsg)
     }
     }
     
     @objc func deleteButtonTapped(sender: UIButton) {
     let okAction = (UIAlertAction(title: "Remove", style: .default, handler: {
     (alert: UIAlertAction!) in
     let productDict = self.wishlistProductsArr[sender.tag] as! NSDictionary
     let productId = productDict["product_id"] as! String
     
     self.deleteItemFromWishlist(productId: productId)
     }))
     
     let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
     
     Alert.showWithActions(title: "REMOVE PRODUCT", message: "Are you sure you want to remove product from wishlist ?", okAction: okAction, cancelAction: cancelAction)
     }
     
     func deleteItemFromWishlist(productId: String) {
     
     print(authorizationKey)
     WebService.parseData(urlStr: API.removeProductToWishlist(productId: productId), parameters: nil, authorizationKey: authorizationKey, method: .DELETE, showHud: true, successHandler: { (result) in
     print("WishlistData:\(result)")
     let dict = result as! NSDictionary
     if let statusDict = dict["status"] as? NSDictionary {
     if statusDict["code"] as! Int == 1 {
     
     let okAction = (UIAlertAction(title: "Okay", style: .default, handler: {
     (alert: UIAlertAction!) in
     
     self.getWishlistProducts()
     
     DispatchQueue.main.async {
     self.tableView.reloadData()
     }
     }))
     
     Alert.showAlertforOkAction(message: "Product removed from wishlist", okAction: okAction)
     
     }
     }
     
     
     else if let errorDict = dict["error"] as? NSDictionary {
     if errorDict["password"] != nil {
     Alert.showAlert(message: errorDict["password"] as! String)
     }
     }  else if let statusDict = dict["status"] as? NSDictionary {
     if statusDict["code"] as! Int == 0 {
     Alert.showAlert(message: statusDict["message"] as! String)
     }
     }
     
     else {
     Alert.showAlert(message: "Unknown error, Please try again!")
     }
     
     
     }) { (errMsg) in
     Alert.showAlert(message: errMsg)
     }
     }*/
}
