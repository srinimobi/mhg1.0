//
//  WishlistTableCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/15/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class WishlistTableCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        // containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        //        containerView.layer.shadowColor = UIColor.gray.cgColor
        //        containerView.layer.shadowOpacity = 1.0
        //        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        //        containerView.layer.shadowRadius = 3.5
        
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        containerView.layer.shadowRadius = 2.5
        containerView.layer.cornerRadius = 20
        
        productImageView.layer.cornerRadius = productImageView.frame.size.height/2
        productImageView.layer.masksToBounds = true
        productImageView.layer.borderColor = UIColor.customOrange().cgColor
        productImageView.layer.borderWidth = 5
        
        deleteButton.layer.cornerRadius = deleteButton.frame.size.height/2
        deleteButton.layer.masksToBounds = true
        
        titleLabel.text = "Product Name"
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Poppins", size: 14)
        
        priceLabel.text = "Price: $xx.xx"
        priceLabel.textColor = UIColor.black
        priceLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        cartButton.layer.cornerRadius = 15
        cartButton.layer.masksToBounds = true
        cartButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 11)
    }
    
    func configure(productDict: NSDictionary) {
        titleLabel.text = productDict["name"] as? String
        priceLabel.text = productDict["price"] as? String
        
        let imageUrl = productDict["thumb"] as! String
        if let imgurl = URL(string: imageUrl){
            self.productImageView.sd_setImage(with: imgurl)
        }
    }
}
