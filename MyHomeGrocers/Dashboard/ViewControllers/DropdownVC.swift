//
//  DropdownVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/24/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class DropdownVC: UIViewController {
    
    var authorizationKey = ""

    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var logoutLabel: UILabel!
    @IBOutlet weak var orderHistoryLabel: UILabel!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var cartLabel: UILabel!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup() {
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        
        self.containerView.layer.cornerRadius = 12
        self.containerView.layer.masksToBounds = true
        self.containerView.backgroundColor = .white
        
        notificationsLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        notificationsLabel.textColor = UIColor.customOrange()
        
        cartLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        cartLabel.textColor = UIColor.customOrange()
        
        rewardLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        rewardLabel.textColor = UIColor.customOrange()
        
        orderHistoryLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        orderHistoryLabel.textColor = UIColor.customOrange()
        
        logoutLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        logoutLabel.textColor = UIColor.customOrange()
        
        dismissButton.layer.cornerRadius = dismissButton.frame.size.height/2
        dismissButton.layer.masksToBounds = true
    }

    @IBAction func dismissButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func notificationsButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCart(_ sender: Any) {
    /*    let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EmptyCVC") as! EmptyCartVC
        self.navigationController?.pushViewController(vc, animated: true) */
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnRewardsClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RewardsVC") as! RewardsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOrderHistoryClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrdersListVC") as! OrdersListVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
