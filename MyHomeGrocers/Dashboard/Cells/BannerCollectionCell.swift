//
//  BannerCollectionCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/16/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class BannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.contentMode = .scaleAspectFit
        //imageView.clipsToBounds = true
    }
    
    func configure(banner: NSDictionary) {
        let imageUrl = banner["image"] as! String
        
        if let imgurl = URL(string: imageUrl){
            self.imageView.sd_setImage(with: imgurl)
        }
    }
}
