//
//  ProductsCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/16/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductsCell: UITableViewCell {
    
    weak var delegate: DashboardVC?
    var authorizationKey = ""

    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var btnHeaderViewAll: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var previousProductsArr: NSArray = []
    var cartArr: NSArray = []
    var newArrivalsArr: NSArray = []
    var offersArr: NSArray = []

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        headerTitle.font = UIFont(name: "Poppins-Medium", size: 13)
        headerTitle.textColor = .black
        
        btnHeaderViewAll.layer.borderWidth = 0.0
        btnHeaderViewAll.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(previously_ordered_products: NSArray) {
        previousProductsArr = previously_ordered_products
        collectionView.reloadData()
    }
    
    func configure(cart_products: NSArray, headerTitle: String) {
        print("COUNT - CART", cart_products.count)
        if headerTitle == "My Cart" {
            if cart_products.count != 0 {
                cartArr = cart_products
            }
        }
        collectionView.reloadData()
    }
    
    func configure(newArrivals: NSArray, headerTitle: String) {
        if headerTitle == "New Arrivals" {
            if newArrivals.count != 0 {
                print("NA COUNT : ", newArrivalsArr.count)
                newArrivalsArr = newArrivals
            }
        }
        collectionView.reloadData()
    }
    
    func configure(inoffer: NSArray, headerTitle: String) {
        if headerTitle == "Special Offers" {
            if inoffer.count != 0 {
                print("NA COUNT : ", inoffer.count)
                offersArr = inoffer
            }
        }
        collectionView.reloadData()
    }
}

extension ProductsCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if headerTitle.text == "My Cart" {
            return cartArr.count
        }
        
        if headerTitle.text == "New Arrivals" {
            return newArrivalsArr.count
        }
        
        if headerTitle.text == "Special Offers" {
            return offersArr.count
        }
        
        return previousProductsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        
        if headerTitle.text == "Previously Ordered" {
            cell.configurePreviouslyOrderCell(previously_ordered_products: previousProductsArr[indexPath.item] as! NSDictionary)
            cell.gotoCartButton.tag = indexPath.item
            cell.gotoCartButton.addTarget(self, action: #selector(reorderButtonTapped(_:)), for: .touchUpInside)
        }
        
        if headerTitle.text == "My Cart" {
            cell.configureMyCartCell(cart_products: cartArr[indexPath.item] as! NSDictionary)
            
            cell.plusButton.tag = indexPath.item
            cell.plusButton.addTarget(self, action: #selector(plushButtonNTapped(_:)), for: .touchUpInside)
            
            cell.minusButton.tag = indexPath.item
            cell.minusButton.addTarget(self, action: #selector(minusButtonNTapped(_:)), for: .touchUpInside)
        }
        
        if headerTitle.text == "New Arrivals" {
            cell.configureNewArrivalsCell(new_arrivalDict: newArrivalsArr[indexPath.item] as! NSDictionary)
            cell.gotoCartButton.tag = indexPath.item
            cell.gotoCartButton.addTarget(self, action: #selector(NAGoToCartButtonNTapped(_:)), for: .touchUpInside)
        }
        
        if headerTitle.text == "Special Offers" {
            cell.configureSpecialOffersCell(in_offerDict: offersArr[indexPath.item] as! NSDictionary)
            cell.gotoCartButton.tag = indexPath.item
            cell.gotoCartButton.addTarget(self, action: #selector(SOGoToCartButtonTapped(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 30, height: 302)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let productDict = previousProductsArr[indexPath.row] as! NSDictionary
        //let productId = productDict["id"] as! String
        //NotificationCenter.default.post(name: Notification.Name("ProductSelected"), object: nil, userInfo: ["productId": productId])
        delegate?.clickItemToNavigateToDetailsScreen()
    }
    
    @objc func NAGoToCartButtonNTapped(_ sender: UIButton){
        print("GO TO CART : ", sender.tag)
        AddDeleteItemFromCart.CallAPI_AddItemToCart(authorizationKey: authorizationKey, productDict: newArrivalsArr[sender.tag] as! NSDictionary, productAddToCartFlag: true) { (result) -> () in
            if (result) {
                // Do action
                self.delegate?.getDashboardData()
            }
        }
    }
    
    @objc func SOGoToCartButtonTapped(_ sender: UIButton){
        print("GO TO CART : ", sender.tag)
        AddDeleteItemFromCart.CallAPI_AddItemToCart(authorizationKey: authorizationKey, productDict: offersArr[sender.tag] as! NSDictionary, productAddToCartFlag: true) { (result) -> () in
            if (result) {
                // Do action
                self.delegate?.getDashboardData()
            }
        }
    }
    
    @objc func plushButtonNTapped(_ sender: UIButton){
        print("GO TO CART : ", sender.tag)
        AddDeleteItemFromCart.CallAPI_AddItemToCart(authorizationKey: authorizationKey, productDict: cartArr[sender.tag] as! NSDictionary, productAddToCartFlag: true) { (result) -> () in
            if (result) {
                // Do action
                self.delegate?.getDashboardData()
            }
        }
    }
    
    @objc func minusButtonNTapped(_ sender: UIButton){
        print("GO TO CART : ", sender.tag)
        AddDeleteItemFromCart.CallAPI_DeleteItemToCart(authorizationKey: authorizationKey, productDict: cartArr[sender.tag] as! NSDictionary, productDeleteFromCartFlag: true) { (result) -> () in
            if (result) {
                // Do action
                self.delegate?.getDashboardData()
            }
        }
    }
    
    @objc func reorderButtonTapped(_ sender: UIButton){
        print("RE ORDER : ", sender.tag)
    }
}
