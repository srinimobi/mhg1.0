//
//  ProductCollectionViewCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/16/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var wishlistView: UIView!
    @IBOutlet weak var productContainer: UIView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var gotoCartButton: UIButton!
    @IBOutlet weak var btnNew: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var lblCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        titleLabel.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        descriptionLabel.font = UIFont(name: "Poppins-Medium", size: 7)
        descriptionLabel.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        
        quantityLabel.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        quantityLabel.font = UIFont(name: "Poppins-SemiBold", size: 10)
        
        priceLabel.textColor = UIColor.customOrange()
        priceLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        /*contentView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
         contentView.layer.borderWidth = 1
         contentView.layer.cornerRadius = 10
         contentView.layer.masksToBounds = false
         contentView.clipsToBounds = false
         
         contentView.layer.shadowColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
         contentView.layer.shadowOpacity = 1.0
         contentView.layer.shadowOffset = CGSize(width: 0, height: 25)
         contentView.layer.shadowRadius = 25*/
        
        btnNew.layer.borderWidth = 0.0
        btnNew.layer.cornerRadius = 8.0
        
        countView.layer.borderWidth = 2.0
        countView.layer.borderColor = UIColor.customOrange().cgColor
        countView.layer.cornerRadius = 10.0
        
        lblCount.layer.masksToBounds = true
        lblCount.layer.borderWidth = 0.0
        lblCount.layer.cornerRadius = 17.5
        
        contentView.layer.cornerRadius = 10.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        contentView.layer.masksToBounds = true
        
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 120.0)//CGSizeMake(0, 2.0);
        layer.shadowRadius = 125.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:bounds, cornerRadius:contentView.layer.cornerRadius).cgPath
        
        productContainer.layer.borderColor = UIColor.customOrange().cgColor
        productContainer.layer.borderWidth = 5
        productContainer.layer.cornerRadius = productContainer.frame.size.height/2
        productContainer.layer.masksToBounds = false
        productContainer.clipsToBounds = false
        
        shareView.layer.cornerRadius = shareView.frame.size.height/2
        shareView.layer.masksToBounds = false
        shareView.clipsToBounds = false
        
        wishlistView.layer.cornerRadius = wishlistView.frame.size.height/2
        wishlistView.layer.masksToBounds = false
        wishlistView.clipsToBounds = false
        
        percentageLabel.layer.cornerRadius = percentageLabel.frame.size.height/2
        percentageLabel.layer.masksToBounds = true
        //percentageLabel.backgroundColor = UIColor.customOrange()
        percentageLabel.font = UIFont(name: "Poppins-SemiBold", size: 10)
        percentageLabel.textColor = .white
        
        gotoCartButton.layer.cornerRadius = 12
        gotoCartButton.layer.masksToBounds = true
        gotoCartButton.backgroundColor = UIColor.lightOrange()
        gotoCartButton.setTitleColor(UIColor.customOrange(), for: .normal)
        gotoCartButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 11)
    }
    
    func configurePreviouslyOrderCell(previously_ordered_products: NSDictionary) {
        titleLabel.text = previously_ordered_products["name"] as? String
        priceLabel.text = "$" + (previously_ordered_products["price"] as? NSNumber ?? 0).stringValue
        
        let imageUrl = previously_ordered_products["image"] as! String
        if let imgurl = URL(string: imageUrl){
            self.productImage.sd_setImage(with: imgurl)
        }
        
        btnNew.isHidden = false
        countView.isHidden = true
        gotoCartButton.isHidden = false
        gotoCartButton.setTitle("Reorder", for: UIControl.State.normal)
    }
    
    func configureMyCartCell(cart_products: NSDictionary) {
        titleLabel.text = cart_products["name"] as? String
        
        let priceString: String = "\(cart_products["price"] ?? 0.00)"
        priceLabel.text = priceString
        
        let quantityString: String = "\(cart_products["cart_quantity"] ?? 0)"
        lblCount.text = quantityString
        
        let discountString = "\(cart_products["discount"] ?? 0)"
        percentageLabel.text = discountString + "" + "%"
        
        btnNew.isHidden = false
        countView.isHidden = false
        gotoCartButton.isHidden = true
    }
    
    func configureNewArrivalsCell(new_arrivalDict: NSDictionary) {
        // productImage.image = new_arrivalDict["image"] as? UIImage
        titleLabel.text = new_arrivalDict["name"] as? String
        
        let priceString: String = "\(new_arrivalDict["price"] ?? 0.00)"
        priceLabel.text = priceString
        
        let quantityString: String = "\(new_arrivalDict["cart_quantity"] ?? 0)"
        quantityLabel.text = quantityString
        
        let discountString = "\(new_arrivalDict["discount"] ?? 0)"
        percentageLabel.text = discountString + "" + "%"
        
        /*let imageUrl = new_arrivalDict["image"] as! String
        if let imgurl = URL(string: imageUrl){
            self.productImage.sd_setImage(with: imgurl)
        }*/
        
        //let quantity: Float = new_arrivalDict["cart_quantity"] as! Float
        //let price: Float = new_arrivalDict["price"] as! Float
        //priceLabel.text = String(price)
        //quantityLabel.text = String(quantity)
        
        btnNew.isHidden = true
        countView.isHidden = true
        gotoCartButton.isHidden = false
        gotoCartButton.setTitle("Add to Cart", for: UIControl.State.normal)
    }
    
    func configureSpecialOffersCell(in_offerDict: NSDictionary) {
        // productImage.image = in_offerDict["image"] as? UIImage
        titleLabel.text = in_offerDict["name"] as? String
        
        let discountString = "\(in_offerDict["discount"] ?? 0)"
        percentageLabel.text = discountString + "" + "%"
        
        let priceString: String = "\(in_offerDict["price"] ?? 0.00)"
        priceLabel.text = priceString
        
        let quantityString: String = "\(in_offerDict["cart_quantity"] ?? 0)"
        quantityLabel.text = quantityString
        
        /*let imageUrl = in_offerDict["image"] as! String
        if let imgurl = URL(string: imageUrl){
            self.productImage.sd_setImage(with: imgurl)
        }*/
        
        btnNew.isHidden = true
        countView.isHidden = true
        gotoCartButton.isHidden = false
        gotoCartButton.setTitle("Add to Cart", for: UIControl.State.normal)
    }
}
