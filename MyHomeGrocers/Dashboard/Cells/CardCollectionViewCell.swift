//
//  CardCollectionViewCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 14/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardView.layer.borderWidth = 0.0
        cardView.layer.cornerRadius = 5.0
        
        cardView.layer.shadowColor = UIColor.red.cgColor
        cardView.layer.shadowOpacity = 1
        cardView.layer.shadowOffset = .zero
        cardView.layer.shadowRadius = 5
    }
}
