//
//  SearchResultCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 15/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.text = "Result-#"
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 16)
        titleLabel.textColor = UIColor.gray
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
