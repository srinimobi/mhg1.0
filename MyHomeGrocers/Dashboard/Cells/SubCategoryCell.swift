//
//  SubCategoryCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/22/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SubCategoryCell: UITableViewCell {
   
    weak var delegate: DashboardVC?
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    var subCategoriesArr: NSArray = []
    var categoriesArr: NSArray = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.dataSource = self
        collectionView.delegate = self
        
        headerLabel.font = UIFont(name: "Poppins-Medium", size: 13)
        headerLabel.textColor = UIColor.customOrange()
        
        containerView.layer.borderColor = UIColor.customOrange().cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 12
        containerView.layer.masksToBounds = true
        
        viewAllButton.layer.cornerRadius = 20
        viewAllButton.layer.masksToBounds = true
        viewAllButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 13)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(categories: NSArray, indexObject: Int) {
        if categories.count != 0 {
            let categoryDict = categories[indexObject] as! NSDictionary
            headerLabel.text = categoryDict["name"] as? String
            subCategoriesArr = categoryDict["sub_categories"] as! NSArray
            print("NA COUNT : ", subCategoriesArr.count)
            print("NA 1OBJ : ", subCategoriesArr[0])
            
            //categoriesArr = categories
            collectionView.reloadData()
        }
    }
    
  /*  func getSubCategories() {
        if let sub1 = (categoriesArr as! NSDictionary)["sub_categories"] as? NSArray {
            subCategoriesArr = sub1
        }
//        if let sub2 = (categoriesArr[1] as! NSDictionary)["sub_categories"] as? NSArray {
//            subCategoriesArr = subCategoriesArr + sub2
//        }
//        subCategoriesArr = ((categoriesArr[0] as! NSDictionary)["sub_categories"] as! NSArray) + ((categoriesArr[1] as! NSDictionary)["sub_categories"] as! NSArray) + ((categoriesArr[2] as! NSDictionary)["sub_categories"] as! NSArray)
    }*/

}

extension SubCategoryCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCollectionCell", for: indexPath) as! SubCategoryCollectionCell
        if subCategoriesArr.count != 0 {
            cell.configure(sub_categories: subCategoriesArr[indexPath.item] as! NSDictionary)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 - 16, height: 66)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let productDict = subCategoriesArr[indexPath.row] as! NSDictionary
//        let productId = productDict["id"] as! String
//        NotificationCenter.default.post(name: Notification.Name("ProductSelected"), object: nil, userInfo: ["productId": productId])
//    }
}
