//
//  CategoryCollectionCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/16/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class CategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        categoryTitleLabel.font = UIFont(name: "Poppins-SemiBold", size: 12)
        categoryTitleLabel.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        categoryTitleLabel.numberOfLines = 0
        
        /*containerView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false

        containerView.layer.shadowColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 25
        
        contentView.layer.shadowColor = UIColor.customOrange().cgColor
        contentView.layer.shadowOpacity = 1.0
        contentView.layer.shadowOffset = CGSize(width: 0, height: 25)
        contentView.layer.shadowRadius = 25*/
        
        contentView.layer.cornerRadius = 10.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        contentView.layer.masksToBounds = true

        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
        layer.shadowRadius = 25.0
        layer.shadowOpacity = 1.0
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:bounds, cornerRadius:contentView.layer.cornerRadius).cgPath
    }
    
    func configure(category: NSDictionary) {
        categoryTitleLabel.text = category["name"] as? String
        let imageUrl = category["image"] as! String        
        if let imgurl = URL(string: imageUrl){
            self.categoryImageView.sd_setImage(with: imgurl)
        }
    }
}
