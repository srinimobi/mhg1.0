//
//  SubCategoryCollectionCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 2/22/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SubCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
    
            super.awakeFromNib()
            // Initialization code
            
            titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 12)
            titleLabel.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
            titleLabel.numberOfLines = 0
            
            contentView.layer.cornerRadius = 10.0
            contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.lightOrange().cgColor
            contentView.layer.masksToBounds = true

            layer.backgroundColor = UIColor.white.cgColor
            layer.shadowColor = UIColor.lightOrange().cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
            layer.shadowRadius = 2.0
            layer.shadowOpacity = 1.0
            layer.masksToBounds = false
            layer.shadowPath = UIBezierPath(roundedRect:bounds, cornerRadius:contentView.layer.cornerRadius).cgPath
        }
        
        func configure(sub_categories: NSDictionary) {
            titleLabel.text = sub_categories["name"] as? String
        }

}
