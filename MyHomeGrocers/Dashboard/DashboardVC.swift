//
//  ViewController.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/15/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

protocol NavigateToCategoryLevelVCDelegate {
    func clickItemToNavigate()
    func clickItemToNavigateToDetailsScreen()
}

class DashboardVC: UIViewController {
    
    @IBOutlet weak var fifthTab: UITabBarItem!
    @IBOutlet weak var fourthTab: UITabBarItem!
    @IBOutlet weak var thirdTab: UITabBarItem!
    @IBOutlet weak var secondTab: UITabBarItem!
    @IBOutlet weak var firstTab: UITabBarItem!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tabBarContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var authorizationKey = ""
    var bannersArr: NSArray = []
    var categoriesArr: NSArray = []
    var previousProductsArr: NSArray = []
    var cartArr: NSArray = []
    var newArrivalsArr: NSArray = []
    var inOfferArr: NSArray = []
    var productsArr: NSDictionary = [:]
    var subCategoriesArr: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setup()
        self.tabBarController?.delegate = self
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.productSelected(notification:)),
            name: Notification.Name("ProductSelected"),
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // * Need to check
        getDashboardData()
        
        tabBar.clipsToBounds = true
        resetTabbarItemSelectionColor()
        firstTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customOrange()], for: .normal)
        
        let firstTabImage = UIImage(named: "MHG-logo-active.png")?.withRenderingMode(.alwaysOriginal)
        firstTab.image = firstTabImage
        
        let secondTabImage = UIImage(named: "2. MyOrders-inactive.png")?.withRenderingMode(.alwaysOriginal)
        secondTab.image = secondTabImage
        
        let thirdTabImage = UIImage(named: "2.Special-offers-inactive.png")?.withRenderingMode(.alwaysOriginal)
        thirdTab.image = thirdTabImage
        
        let fourthTabImage = UIImage(named: "2. bell-inactive.png")?.withRenderingMode(.alwaysOriginal)
        fourthTab.image = fourthTabImage
        
        let fifthTabImage = UIImage(named: "1.Cart-inactive.png")?.withRenderingMode(.alwaysOriginal)
        fifthTab.image = fifthTabImage
    }
    
    func setup() {
        profileButton.layer.cornerRadius = 23
        profileButton.layer.masksToBounds = true
        profileButton.titleLabel?.font = UIFont(name: "Poppins-Regular", size: 11)
        
        addButton.layer.cornerRadius = addButton.frame.size.height/2
        addButton.backgroundColor = .clear
        addButton.layer.masksToBounds = true
        addButton.layer.borderWidth = 1
        addButton.layer.borderColor = UIColor.customOrange().cgColor
        addButton.titleLabel?.font = UIFont(name: "Poppins-Regular", size: 25)
        customerNameLabel.textColor = UIColor.customOrange()
        customerNameLabel.font = UIFont(name: "Poppins-Regular", size: 9)
        
        /*tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 165
        
        searchBar.barTintColor = UIColor(red: 255/255, green: 92/255, blue: 1/255, alpha: 1)
        searchBar.setCustomBackgroundColor (color: UIColor(red: 255/255, green: 92/255, blue: 1/255, alpha: 0.5))*/
        
        tabBarContainer.layer.cornerRadius = 25
        tabBarContainer.layer.masksToBounds = false
        tabBarContainer.clipsToBounds = false
        
        let shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        tabBarContainer.layer.shadowColor = shadowColor.cgColor
        tabBarContainer.layer.shadowOpacity = 1.0
        tabBarContainer.layer.shadowOffset = CGSize(width: 0, height: 25)
        tabBarContainer.layer.shadowRadius = 20
    }
    
    func resetTabbarItemSelectionColor() {
        firstTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customGray()], for: .normal)
        secondTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customGray()], for: .normal)
        thirdTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customGray()], for: .normal)
        fourthTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customGray()], for: .normal)
        fifthTab.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.customGray()], for: .normal)
    }
    
    @objc func productSelected(notification: NSNotification) {
        let productDict = notification.userInfo! as NSDictionary
        let productId = productDict["productId"] as! String
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        vc.productId = productId
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func profileButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SidePanelVC") as! SidePanelVC
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func dropdownButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropdownVC") as! DropdownVC
        
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    func getDashboardData() {
        self.showHUD()
        WebService.parseData(urlStr: API.dashboardData(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { (result) in
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                // * New model + JSON Datat initilized
                let dictData = HomeResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    if let banners = dictData?.data.banner {
                        self.bannersArr = banners
                    }
                    if let categories = dictData?.data.categories {
                        self.categoriesArr = categories
                    }
                    if let previousproducts = dictData?.data.previously_ordered_products {
                        self.previousProductsArr = previousproducts
                    }
                    if let cartProducts = dictData?.data.cart_products {
                        self.cartArr = cartProducts
                    }
                    if let productsDict = dictData?.data.products {
                        //self.productsArr = productsDict
                        let dictData = ProductsDictResponse(json: productsDict as! [String : Any])
                        
                        if let newArrivals = dictData?.new_arrivals {
                            self.newArrivalsArr = newArrivals
                        }
                        
                        if let inOffer = dictData?.in_offer {
                            self.inOfferArr = inOffer
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    // * Navigate to Category Level Screen
    func clickItemToNavigate(categoryDict: NSDictionary) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CategoryLevelVC") as! CategoryLevelVC
        vc.authorizationKey = authorizationKey
        vc.categoryId = categoryDict["id"] as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func clickItemToNavigateToDetailsScreen() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DashboardVC: UITableViewDataSource, UITableViewDelegate {
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if section == 0 {
            return 4
        }
        return 1*/
        
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //if indexPath.section == 0 {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannersCell", for: indexPath) as! BannersCell
            if bannersArr.count != 0 {
                cell.configure(banners: bannersArr)
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            cell.delegate = self
            cell.configure(categories: categoriesArr)
            return cell
        
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.headerTitle.text = "Previously Ordered"
            cell.delegate = self
            cell.configure(previously_ordered_products: previousProductsArr)
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.headerTitle.text = "My Cart"
            cell.delegate = self
            cell.configure(cart_products: cartArr, headerTitle: cell.headerTitle.text!)
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.headerTitle.text = "New Arrivals"
            cell.delegate = self
            cell.authorizationKey = authorizationKey
            cell.configure(newArrivals: newArrivalsArr, headerTitle: cell.headerTitle.text!)
            return cell
        
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCell", for: indexPath) as! ProductsCell
            cell.headerTitle.text = "Special Offers"
            cell.delegate = self
            cell.authorizationKey = authorizationKey
            cell.configure(inoffer: inOfferArr, headerTitle: cell.headerTitle.text!)
            return cell
            
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.delegate = self
            cell.configure(categories: categoriesArr, indexObject: 0)
            return cell
            
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.configure(categories: categoriesArr, indexObject: 1)
            return cell
            
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.configure(categories: categoriesArr, indexObject: 2)
            return cell
            
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardCellIdentifier", for: indexPath) as! DCardCell
            return cell
            
        case 10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoryCell
            cell.configure(categories: categoriesArr, indexObject: 3)
            return cell
            
        case 11:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RewardTrackCellI", for: indexPath) as! RewardTrackCell
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 175
        case 1:
            return 530
        case 2,4,5:
            return 350
        case 3:
            return 350
            /*if (self.cartArr.count != 0) { return 350 }
            else { return 0 }*/
        case 6,7,8,10:
            return 320
        case 9:
            return 200
        case 11:
            return 150
        default:
            return 44
        }
        
        /*if indexPath.section == 0 {
         switch indexPath.row {
         case 0:
         return 165
         case 1:
         return 520
         case 2,3:
         return 350
         
         default:
         return 44
         }
         } else if indexPath.section == 1 {
         return 400
         } else {
         if productsArr.count != 0 {
         if productsArr.count % 2 == 1 {
         return CGFloat(((productsArr.count / 2) + 1) * 350)
         } else {
         return CGFloat((productsArr.count / 2) * 350)
         }
         } else {
         return 0
         }
         }*/
        //return UITableView.automaticDimension
    }
}

extension DashboardVC: UITabBarDelegate, UITabBarControllerDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 2:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrdersListVC") as! OrdersListVC
            vc.authorizationKey = authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 4:
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
            self.navigationController?.pushViewController(vc, animated: true)
        
        case 5:
            /*   if cartArr.count == 0 {
             let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "EmptyCVC") as! EmptyCartVC
             self.navigationController?.pushViewController(vc, animated: true)
             }
             else
             {  */
            let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
            vc.authorizationKey = self.authorizationKey
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }
}

extension UIImage {
    static func imageFromLayer (layer: CALayer) -> UIImage? {
        UIGraphicsBeginImageContext(layer.frame.size)
        guard let currentContext = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in: currentContext)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage
    }
}

extension UISearchBar {
    func setCustomBackgroundColor (color: UIColor) {
        let backgroundLayer = CALayer()
        backgroundLayer.frame = frame
        backgroundLayer.backgroundColor = color.cgColor
        if let image = UIImage.imageFromLayer(layer: backgroundLayer) {
            self.setBackgroundImage(image, for: .any, barMetrics: .default)
        }
    }
}

extension DashboardVC : UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
