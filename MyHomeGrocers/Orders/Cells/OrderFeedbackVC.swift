//
//  OrderFeedbackVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 20/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit
import FloatRatingView


class OrderFeedbackVC: UIViewController, FloatRatingViewDelegate {

    
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var forthLabel: UILabel!
    @IBOutlet weak var btnHelp: UIButton!
    
    var authorizationKey = ""
    
    override func viewDidLoad() {
     
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Reset float rating view's background color
        
        firstLabel.font = UIFont(name: "Poppins-Bold", size: 20)
        secondLabel.font = UIFont(name: "Poppins-Medium", size: 14)
        thirdLabel.font = UIFont(name: "Poppins-Bold", size: 24)
        forthLabel.font = UIFont(name: "Poppins-Medium", size: 18)
        
        btnHelp.layer.cornerRadius = 20
        
        floatRatingView.backgroundColor = UIColor.clear
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        floatRatingView.type = .wholeRatings

    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHelpClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    
}

