//
//  ProcessingTableCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ProcessingTableCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deliveryDatelabel: UILabel!
    @IBOutlet weak var orderTotalLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var idLabel: UILabel!
    
    var authorizationKey = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setup() {
        // containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        //        containerView.layer.shadowColor = UIColor.gray.cgColor
        //        containerView.layer.shadowOpacity = 1.0
        //        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        //        containerView.layer.shadowRadius = 3.5
        
        containerView.layer.shadowColor = UIColor.lightOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 2
        containerView.layer.cornerRadius = 20
        
        
        idLabel.textColor = UIColor.descriptionGray()
        idLabel.text = "Order ID: #"
        idLabel.font = UIFont(name: "Poppins-SemiBold", size: 12)
        
        statusLabel.textColor = UIColor.customOrange()
        statusLabel.text = "Processing"
        statusLabel.font = UIFont(name: "Poppins-Bold", size: 18)
        
        orderTotalLabel.textColor = UIColor.descriptionGray()
        orderTotalLabel.text = "Order total : $xx.xxx"
        orderTotalLabel.font = UIFont(name: "Poppins-Medium", size: 10)
        
        deliveryDatelabel.textColor = UIColor.descriptionGray()
        deliveryDatelabel.text = "Delivery date : dd/mm/yyyy"
        deliveryDatelabel.font = UIFont(name: "Poppins-Medium", size: 10)
        
        cancelButton.backgroundColor = UIColor.white
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 9)
        //  cancelButton.layer.borderWidth = 0.5
        //  cancelButton.layer.borderColor = UIColor.lightOrange().cgColor
        cancelButton.layer.cornerRadius = 17
        cancelButton.layer.masksToBounds = false
        cancelButton.layer.shadowColor = UIColor.red.cgColor
        cancelButton.layer.shadowOpacity = 0.5
        cancelButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        cancelButton.layer.shadowRadius = 2
    }
    
    /*   func configure(orderDict: NSDictionary) {
     guard let orderId = orderDict["order_id"] as? String else {
     return
     }
     idLabel.text = "Order ID: #" + orderId
     statusLabel.text = orderDict["status"] as? String
     guard let orderTotal = orderDict["order_total"] as? String else {
     return
     }
     orderTotalLabel.text = "Order Total: " + orderTotal
     
     guard let deliveryDate = orderDict["delivery_time"] as? String else {
     return
     }
     deliveryDatelabel.text = "Delivery Date: " + deliveryDate
     
     productImageView.image = UIImage(named: "processing")
     }
     */
    
}
