//
//  CustomerOrderTableCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 09/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CustomerOrderTableCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var orderStatusImage: UIImageView!
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var deliveryDateLbl: UILabel!
    @IBOutlet weak var reorderBtn: UIButton!
    @IBOutlet weak var rateorderBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        statusLbl.textColor = UIColor.customOrange()
        rateorderBtn.setTitleColor(UIColor.customOrange(), for: UIControl.State.normal)
        reorderBtn.setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        reorderBtn.layer.masksToBounds = false
        reorderBtn.layer.borderWidth = 0
        reorderBtn.layer.cornerRadius = 10.0

        rateorderBtn.layer.masksToBounds = false
        rateorderBtn.layer.borderWidth = 2.0
        rateorderBtn.layer.cornerRadius = 10.0
        rateorderBtn.layer.borderColor = UIColor.customOrange().cgColor
    }

    @IBAction func doPressReOrder(_ sender: Any) {
        
    }
    
    @IBAction func doPressRateorder(_ sender: Any) {
        
    }
    
    func configureCell(customerOrderDict: NSDictionary) {
        let dictData = CustomerOrderDict(json: customerOrderDict as! [String : Any])
        orderIdLbl.text = dictData?.order_id
        statusLbl.text = dictData?.status
        orderTotalLbl.text = dictData?.total
        deliveryDateLbl.text = dictData?.delivery_time
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
