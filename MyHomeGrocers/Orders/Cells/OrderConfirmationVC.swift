//
//  OrderConfirmationVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 22/04/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class OrderConfirmationVC: UIViewController {

    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var btnContinueShopping: UIButton!
    @IBOutlet weak var firstLabel: UILabel!
    
    var authorizationKey = ""
   // var resultData: NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        // Do any additional setup after loading the view.
        setup()
    }
    
    @IBAction func btnTrackOrder(_ sender: Any) {
    }
    
    @IBAction func btnContinueShopping(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setup() {
        
        firstLabel.font = UIFont(name: "Poppins-Semibold", size: 20)
        
        btnTrackOrder.layer.borderWidth = 2
        btnTrackOrder.layer.borderColor = UIColor.customOrange().cgColor
        btnTrackOrder.layer.cornerRadius = 30
        btnTrackOrder.layer.masksToBounds = true
        btnTrackOrder.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 20)
        btnTrackOrder.setTitleColor(UIColor.customOrange(), for: .normal)
        
        btnContinueShopping.backgroundColor = UIColor.customOrange()
        btnContinueShopping.layer.borderWidth = 2
        btnContinueShopping.layer.borderColor = UIColor.customOrange().cgColor
        btnContinueShopping.layer.cornerRadius = 30
        btnContinueShopping.layer.masksToBounds = true
        btnContinueShopping.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 20)
 }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
