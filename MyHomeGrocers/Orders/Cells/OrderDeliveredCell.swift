//
//  OrderDeliveredCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class OrderDeliveredCell: UITableViewCell {
    
    @IBOutlet weak var rateOrderButton: UIButton!
    @IBOutlet weak var reorderButton: UIButton!
    @IBOutlet weak var deliveryDatelabel: UILabel!
    @IBOutlet weak var orderTotalLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var idLabel: UILabel!
    
    var authorizationKey = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setup() {
        
        idLabel.text = "Order ID: #"
        idLabel.textColor = UIColor.descriptionGray()
        idLabel.font = UIFont(name: "Poppins-", size: 12)
        
        statusLabel.textColor = UIColor.black
        statusLabel.text = "Delivered"
        statusLabel.font = UIFont(name: "Poppins-Bold", size: 18)
        
        orderTotalLabel.textColor = UIColor.descriptionGray()
        orderTotalLabel.text = "Order Total: $xxx"
        orderTotalLabel.font = UIFont(name: "Poppins-Medium", size: 10)
        
        deliveryDatelabel.textColor = UIColor.descriptionGray()
        deliveryDatelabel.text = "Delivery Date: dd/mm/yyyy"
        deliveryDatelabel.font = UIFont(name: "Poppins-Medium", size: 10)
        
        reorderButton.backgroundColor = UIColor.customOrange()
        reorderButton.layer.cornerRadius = 20
        reorderButton.setTitleColor(UIColor.white, for: .normal)
        reorderButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 18)
        
        rateOrderButton.backgroundColor = UIColor.clear
        rateOrderButton.layer.cornerRadius = 20
        rateOrderButton.layer.borderWidth = 2
        rateOrderButton.layer.borderColor = UIColor.customOrange().cgColor
        rateOrderButton.setTitleColor(UIColor.customOrange(), for: .normal)
        rateOrderButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 18)    
        
        
    }

}
