//
//  OrderCancellationVC.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 13/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class OrderCancellationVC: UIViewController {

    @IBOutlet weak var containeView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var lblReport: UILabel!
    @IBOutlet weak var btnContinueShopping: UIButton!
    
    var authorizationKey = ""
    let shadowLayer = CAShapeLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup() {
        
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        containeView.layer.cornerRadius = 30
        containeView.layer.masksToBounds = true
        
        imageContainerView.layer.cornerRadius = imageContainerView.layer.frame.size.height/2
        shadowLayer.path = UIBezierPath(roundedRect: imageContainerView.bounds,
                                           cornerRadius: imageContainerView.layer.cornerRadius).cgPath
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.fillColor = imageContainerView.backgroundColor?.cgColor
        shadowLayer.shadowColor = UIColor.red.cgColor
        shadowLayer.shadowOffset = CGSize(width: 0, height: 0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2
           imageContainerView.layer.insertSublayer(shadowLayer, at: 0)
        lblReport.font = UIFont(name: "Poppins-Medium", size: 24)
        lblReport.textColor = UIColor.red
        btnContinueShopping.layer.cornerRadius = btnContinueShopping.frame.size.height/2
        btnContinueShopping.backgroundColor = UIColor.customOrange()
        btnContinueShopping.setTitleColor(UIColor.white, for: .normal)
        btnContinueShopping.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 18)
    }
    
    @IBAction func btnContinueShoppingClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
