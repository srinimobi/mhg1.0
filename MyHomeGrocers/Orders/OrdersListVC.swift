//
//  OrdersListVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 12/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class OrdersListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var authorizationKey = ""
    var ordersArr: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getOrders()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rateOrderButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderFBVC") as! OrderFeedbackVC
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func reorderButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CancelOrderVC") as! CancelOrderVC
        vc.authorizationKey = self.authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getOrders() {
        print(authorizationKey)
        self.showHUD()
        WebService.parseData(urlStr: API.getOrderList(), parameters: nil, authorizationKey: authorizationKey, method: .GET, showHud: true, successHandler: { (result) in
            
            print("DashboardData:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = CustomerOrdersResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    self.ordersArr = (dictData?.data.orders)!
                    self.tableView.reloadData()
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension OrdersListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ordersArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ICustomerOrderTableCell", for: indexPath) as! CustomerOrderTableCell
        cell.configureCell(customerOrderDict: ordersArr[indexPath.row] as! NSDictionary)
        return cell
    }
}
