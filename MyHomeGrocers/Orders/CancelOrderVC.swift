//
//  CancelOrderVC.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 11/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class CancelOrderVC: UIViewController {
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var lblCancelOrder: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var lblRefundStatus: UILabel!
    @IBOutlet weak var txtComments: UITextField!
    var authorizationKey = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtComments.addBottomBorder()
        setup()
    }
    
    func setup() {
        
        lblCancelOrder.font = UIFont(name: "Poppins-Semibold", size: 20)
        lblOrderID.font = UIFont(name: "Poppins-Medium", size: 12)
        lblTitle.font = UIFont(name: "Poppins-Semibold", size: 20)
        lblRefundStatus.font = UIFont(name: "Poppins-Semibold", size: 18)
        lblRefundStatus.layer.cornerRadius = 20
        lblRefundStatus.layer.masksToBounds = true
        
        btnHelp.layer.cornerRadius = btnHelp.frame.size.height/2
        btnHelp.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 16)
        btnHelp.setTitleColor(UIColor.white, for: .normal)
        
        btnCancelOrder.layer.cornerRadius = btnCancelOrder.frame.size.height/2
        btnCancelOrder.backgroundColor = UIColor.red
        btnCancelOrder.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 20)
        btnCancelOrder.setTitleColor(UIColor.white, for: .normal)
     
    
    }

    @IBAction func btnCancelOrderClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderCancellationVC") as! OrderCancellationVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
