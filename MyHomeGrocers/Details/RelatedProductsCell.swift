//
//  RelatedProductsCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 14/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class RelatedProductsCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var productCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.font = UIFont(name: "Poppins-Medium", size: 16)
        btnViewAll.backgroundColor = UIColor.customOrange()
        btnViewAll.layer.cornerRadius = btnViewAll.frame.size.height/2
        btnViewAll.titleLabel?.font = UIFont(name: "Poppins", size: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension RelatedProductsCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCollectionCell", for: indexPath) as! ProductsCollectionCell
      //  if bannersArr.count != 0 {
     //   cell.configure(banner: bannersArr[indexPath.row] as! NSDictionary)
            //cell.imageView.contentMode = .scaleAspectFit
     //   }
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewFlowLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: UIScreen.main.bounds.width-40, height: 148)
//    }
    
 /*   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewFlowLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: deviceFrame.size.width, height: )
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentIndex:CGFloat = self.collectionView.contentOffset.x / self.collectionView.frame.size.width
        pageControl.currentPage = Int(currentIndex)
    }  */

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 186, height: 344)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

