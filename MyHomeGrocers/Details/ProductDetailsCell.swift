//
//  ProductDetailsCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 1/31/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductDetailsCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup() {
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 16)
        
        quantityButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 21)
        quantityButton.layer.cornerRadius = 5
        quantityButton.layer.borderWidth = 1
        quantityButton.layer.borderColor = UIColor.customOrange().cgColor
        quantityButton.layer.masksToBounds = true
        
        priceLabel.textColor = UIColor.black
        priceLabel.font = UIFont(name: "Poppins-Medium", size: 19)
        
        oldPriceLabel.textColor = UIColor.descriptionGray()
        oldPriceLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        
        descriptionLabel.textColor = UIColor.descriptionGray()
        descriptionLabel.font = UIFont(name: "Poppins-Medium", size: 7)
    }

}
