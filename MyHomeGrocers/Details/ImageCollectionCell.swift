//
//  ImageCollectionCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 1/31/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
}
