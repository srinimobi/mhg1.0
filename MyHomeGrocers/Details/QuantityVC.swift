//
//  QuantityVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 16/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class QuantityVC: UIViewController {

    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnApply: UIButton!
    
    var authorizationKey = ""
    var quantityArray = ["5 LBS","10LBS","15 LBS","20 LBS","25 LBS","30 LBS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        
        containerView.layer.cornerRadius = 30
        containerView.layer.masksToBounds = false
       
        btnApply.layer.cornerRadius = 10
        btnApply.layer.masksToBounds = true
        btnApply.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 24)
        btnApply.setTitleColor(UIColor.customOrange(), for: .normal)
        
        lblTitle.font = UIFont(name: "Poppins-Semibold", size: 22)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDismissClicked(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QuantityVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  quantityArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuantityCell", for: indexPath) as! QuantityCell
        cell.lblQuantity.text = quantityArray[indexPath.item]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath)-> CGSize {
        return CGSize(width: 100, height: 50)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  let cell = collectionView.cellForItem(at: indexPath) as? QuantityCell {
            cell.select()
    }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if  let cell = collectionView.cellForItem(at: indexPath) as? QuantityCell {
            cell.deselect()
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
