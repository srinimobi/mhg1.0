//
//  ProductImageCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 1/31/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductImageCell: UITableViewCell {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.cornerRadius = 15
        containerView.layer.masksToBounds = true
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 3.5
        
        containerView.layer.borderColor = UIColor.lightOrange().cgColor
        containerView.layer.borderWidth = 1
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)

        // Configure the view for the selected state
    }

}

extension ProductImageCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
      //  cell.productImageView.image = UIImage(named: "productBig")
        return cell
    }
}
