//
//  OffersCollectionCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 14/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class OffersCollectionCell: UICollectionViewCell {
    @IBOutlet weak var offersContainerView: UIView!
    @IBOutlet weak var lblOffer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        offersContainerView.layer.masksToBounds = false
        offersContainerView.clipsToBounds = false
        offersContainerView.layer.shadowColor = UIColor.customOrange().cgColor
        offersContainerView.layer.shadowRadius = 4
        offersContainerView.layer.shadowOpacity = 0.4
        offersContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        offersContainerView.layer.cornerRadius = 20
        lblOffer.font = UIFont(name: "Poppins-Semibold", size: 22)
        
    }
    
}
