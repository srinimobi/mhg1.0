//
//  ProductsCollectionCell.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 14/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductsCollectionCell: UICollectionViewCell {
    @IBOutlet weak var productContainerView: UIView!
    @IBOutlet weak var lblNew: UILabel!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var lblProductDetails: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnAddToWishlist: UIButton!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnQuantity: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        productContainerView.clipsToBounds = false
        productContainerView.layer.masksToBounds = false
        productContainerView.layer.shadowColor = UIColor.gray.cgColor
        productContainerView.layer.shadowRadius = 3
        productContainerView.layer.shadowOpacity = 0.5
        productContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        productContainerView.layer.cornerRadius = 20
        
        lblNew.backgroundColor = UIColor.customOrange()
        lblNew.layer.cornerRadius = lblNew.frame.size.height/2
        lblNew.font = UIFont(name: "Poppins-Medium", size: 10)
        lblNew.layer.masksToBounds = true
        
        lblPercentage.layer.cornerRadius = lblPercentage.frame.size.height/2
        lblPercentage.font = UIFont(name: "Poppins-Medium", size: 10)
        lblPercentage.backgroundColor = UIColor.systemGreen
        lblPercentage.layer.masksToBounds = true
        
        btnAddToWishlist.backgroundColor = UIColor.customOrange()
        btnAddToWishlist.layer.cornerRadius = btnAddToWishlist.frame.size.height/2
        
        btnAddToCart.backgroundColor = UIColor.lightOrange()
        btnAddToCart.layer.cornerRadius = btnAddToCart.frame.size.height/2
        btnAddToCart.setTitleColor(UIColor.customOrange(), for: .normal)
        btnAddToCart.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 10)
        
        imgProduct.layer.borderColor = UIColor.customOrange().cgColor
        imgProduct.layer.borderWidth = 5
        imgProduct.layer.cornerRadius = imgProduct.frame.size.height/2
        
        btnQuantity.layer.cornerRadius = 10
        btnQuantity.layer.borderColor = UIColor.customOrange().cgColor
        btnQuantity.layer.borderWidth = 1
        btnQuantity.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 12)
    }
    
}
