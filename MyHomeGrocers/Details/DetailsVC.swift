//
//  DetailsVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 1/31/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var saveForLaterButton: UIButton!
    var productId = ""
    var authorizationKey = ""
    var productDict: NSDictionary = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
        
        // getProductDetails()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setup() {
        bottomView.layer.cornerRadius = 20
        bottomView.layer.masksToBounds = true
        bottomView.clipsToBounds = false
    }
    
    func getProductDetails() {
        let parameters = ["product_id": productId]
        WebService.parseData(urlStr: API.getProductDetails(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("DashboardData:\(result)")
            let dict = result as! NSDictionary
            if let resultDict = dict["data"] as? NSDictionary {
                self.productDict = resultDict
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
            
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }
            
            
        }) { (errMsg) in
            Alert.showAlert(message: errMsg)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnQuantityTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QuantityVC") as! QuantityVC
        self.present(vc, animated: true, completion: nil)
        //self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductImageCell") as! ProductImageCell
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailsCell") as! ProductDetailsCell
            // cell.titleLabel.text = productDict["name"] as? String
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedProductsCell") as! RelatedProductsCell
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductOffersCell") as! ProductOffersCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 3 {
            return 200
        }
        else if indexPath.row == 2 {
            return 400
        }
        return 380
    }
    
    
}
