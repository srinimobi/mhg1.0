//
//  QuantityCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 16/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class QuantityCell: UICollectionViewCell {
    
    @IBOutlet weak var lblQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = self.frame.size.height/2
        self.backgroundColor = UIColor.clear
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.lightOrange().cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        
       // lblQuantity.layer.cornerRadius = lblQuantity.frame.size.height/2
        lblQuantity.layer.masksToBounds = true
        
        lblQuantity.font = UIFont(name: "Poppins-Semibold", size: 20)
    }
    func select() {
        lblQuantity.backgroundColor = UIColor.customOrange()
        lblQuantity.textColor = UIColor.white
    }
    
    func deselect() {
        lblQuantity.backgroundColor = UIColor.white
        lblQuantity.textColor = UIColor.customOrange()
    }
    
}
