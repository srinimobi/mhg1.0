//
//  API.swift
//  Created by Uma B on 6/20/18.
//  Copyright © 2018 Uma B. All rights reserved.

import UIKit

// * UAT
let BASEURL = "https://uat.myhomegrocers.com/api/"

// * PRODUCTION

class API: NSObject {
    class func register() -> String {
        return BASEURL + "rest/register/send"
    }
    
    class func login() -> String {
        return BASEURL + "rest/login"
    }
    
    class func verifyLoginOtp() -> String {
        return BASEURL + "rest/login/verify"
    }
    
    class func verifySignupOtp() -> String {
        return BASEURL + "rest/register/verify"
    }
    
    class func forgotPasswordMobileOtp() -> String {
        return BASEURL + "rest/forgotten/send"
    }
    
    class func validateUserId() -> String {
        return BASEURL + "rest/register/validate"
    }
    
    class func dashboardData() -> String {
        return BASEURL + "rest/home"
    }
    
    class func verifyOtpAndSetPassword() -> String {
        return BASEURL + "rest/forgotten/verifyotp_setpassword"
    }
    
    class func accountDetails() -> String {
        return BASEURL + "rest/account"
    }
    
    class func addressList() -> String {
        return BASEURL + "rest/account/address"
    }
    
    class func updateProfile() -> String {
        return BASEURL + "rest/account/updateProfile"
    }
    
    class func changePassword() -> String {
        return BASEURL + "rest/account/password"
    }
    
    class func verifyUpdateProfileOtp() -> String {
        return BASEURL + "rest/account/updateProfileVerifyOtp"
    }
    
    class func getOrderList() -> String {
        return BASEURL + "/rest/order&page=1"
    }
    
    class func getCartProducts() -> String {
        return BASEURL + "rest/cart"
    }
    
    class func updateCart() -> String {
        return BASEURL + "rest/cart/updateCartQuantity"
    }
    
    class func deleteCartItem() -> String {
        return BASEURL + "rest/cart/deleteCartItem"
    }
    
    class func addItemToCart() -> String {
        return BASEURL + "rest/cart/addItemCart"
    }
    
    class func getWishlistProducts() -> String {
        return BASEURL + "rest/wishlist"
    }
    
    class func addProductToWishlist(productId: String) -> String {
        return BASEURL + "rest/wishlist&id=" + productId
    }
    
    class func removeProductToWishlist(productId: String) -> String {
        return BASEURL + "rest/wishlist&id=" + productId
    }
    
    class func getProductDetails() -> String {
        return BASEURL + "/rest/product"
    }
    
    class func getCheckoutDetails() -> String {
        return BASEURL + "/rest/checkout"
    }
    
    class func applyCoupon() -> String {
        return BASEURL + "/rest/checkout/applyCoupon"
    }
    
    class func searchItems() -> String {
        return BASEURL + "/rest/search"
    }
    
    class func specials() -> String {
        return BASEURL + "/rest/specials&per_page=10&page_number=1"
    }
    
    class func categoryProduct() -> String {
        return BASEURL + "/rest/category_product"
    }
    
    class func productList() -> String {
        return BASEURL + "/rest/product_list&filter=popular_product"
    }
    
    class func emptyCart() -> String {
        return BASEURL + "/rest/cart/emptycart"
    }
}
