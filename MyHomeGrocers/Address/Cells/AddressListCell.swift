//
//  AddressListCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/30/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class AddressListCell: UITableViewCell {
    
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var apartmentLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipcodeLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    //@IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var circleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //nameLabel.textColor = UIColor.descriptionGray()
        //nameLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        addressLine1.textColor = UIColor.descriptionGray()
        addressLine1.font = UIFont(name: "Poppins-Medium", size: 12)
        
        apartmentLabel.textColor = UIColor.descriptionGray()
        apartmentLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        stateLabel.textColor = UIColor.descriptionGray()
        stateLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        cityLabel.textColor = UIColor.descriptionGray()
        cityLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        zipcodeLabel.textColor = UIColor.descriptionGray()
        zipcodeLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        phoneNumberLabel.textColor = UIColor.descriptionGray()
        phoneNumberLabel.font = UIFont(name: "Poppins-Medium", size: 12)
        
        circleImage.tintColor = UIColor.customOrange()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(address: NSDictionary) {
        //addressTitleLabel.text = address["address_type"] as? String
        zipcodeLabel.text = address["postcode"] as? String
        cityLabel.text = address["city"] as? String
        stateLabel.text = address["zone"] as? String
        
        var firstName = ""
        var lastName = ""
        var addLine1 = ""
        var addLine2 = ""
        
        if let fName = address["firstname"] as? String {
            firstName = fName
        }
        
        if let lName = address["lastname"] as? String {
            lastName = lName
        }
        
        if let line1 = address["address_1"] as? String {
            addLine1 = line1
        }
        
        if let line2 = address["address_2"] as? String {
            addLine2 = line2
        }
        
        addressLine1.text = firstName + " " + lastName
        apartmentLabel.text = addLine1 + ", " + addLine2
        cityLabel.text = address["city"] as? String
        stateLabel.text = address["zone"] as? String
        zipcodeLabel.text = address["postcode"] as? String
        phoneNumberLabel.text = "99876543210" //address["postcode"] as? String
    }
}
