//
//  AddAddressCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 04/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class AddAddressCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.lightOrange()
        self.layer.cornerRadius = 10
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 15)
}
    func select() {
        self.backgroundColor = UIColor.customOrange()
        titleLabel.textColor = UIColor.white
    }
    
    func deselect() {
        self.backgroundColor = UIColor.lightOrange()
        titleLabel.textColor = UIColor.black
    }
   
    
}
