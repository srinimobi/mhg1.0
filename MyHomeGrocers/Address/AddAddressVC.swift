//
//  AddAddressVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/29/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

// Textfield borderline extension
extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.customOrange().cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}

class AddAddressVC: UIViewController  {
    @IBOutlet weak var saveAsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var txtAddressLine1: UITextField!
    @IBOutlet weak var txtAppartment: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var txtAlternatePhoneNumber: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var authorizationKey = ""
    var titleArray = ["Home","Work","Other"]
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    /*
     let txtAddressLine1 = TextFieldView()
     let txtAppartment = TextFieldView()
     let txtCity = TextFieldView()
     let txtState = TextFieldView()
     let txtZipcode = TextFieldView()
     let txtLandmark = TextFieldView()
     let txtAlternatePhoneNumber = TextFieldView()   */
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setup()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // Setup Function
    func setup() {
        
        //     layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 90, height: 40)
        layout.minimumInteritemSpacing = 40
        collectionView!.collectionViewLayout = layout
        
        actionButton.layer.cornerRadius = 25
        actionButton.layer.masksToBounds = true
        
        /*homeButton.layer.cornerRadius = 10
         homeButton.layer.masksToBounds = true
         
         workButton.layer.cornerRadius = 10
         workButton.layer.masksToBounds = true
                  
         otherButton.layer.cornerRadius = 10
         otherButton.layer.masksToBounds = true*/
        
        currentLocationButton.layer.cornerRadius = 25
        currentLocationButton.layer.masksToBounds = true
        
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 18)
        
        currentLocationButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 18)
        
        txtAddressLine1.addBottomBorder()
        txtAppartment.addBottomBorder()
        txtCity.addBottomBorder()
        txtState.addBottomBorder()
        txtZipcode.addBottomBorder()
        txtLandmark.addBottomBorder()
        txtAlternatePhoneNumber.addBottomBorder()
        
        /*titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 18)
         
         currentLocationButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 18)
         
         // addressTextFieldView.textField.placeholder = "Address Line 1*"
         //addressView.addSubview(addressTextFieldView)
         
         txtAddressLine1.bindEdgesToSuperview()
         
         //    txtAddressLine1.nextButton.isHidden = true
         //     txtAddressLine1.nextButton.flex.isIncludedInLayout = false
         
         // apartmentTextFieldView.textField.placeholder = "Apartment / Suite"
         // apartmentView.addSubview(apartmentTextFieldView)
         
         txtAppartment.bindEdgesToSuperview()
         
         //    txtAppartment.nextButton.isHidden = true
         //   txtAppartment.nextButton.flex.isIncludedInLayout = false
         
         // cityTextFieldView.textField.placeholder = "City*"
         //  cityView.addSubview(cityTextFieldView)
         
         txtCity.bindEdgesToSuperview()
         
         //  txtCity.nextButton.isHidden = true
         //  txtCity.nextButton.flex.isIncludedInLayout = false
         
         //  stateTextFieldView.textField.placeholder = "State*"
         //  stateView.addSubview(stateTextFieldView)
         
         txtState.bindEdgesToSuperview()
         
         // txtState.nextButton.isHidden = true
         //  txtState.nextButton.flex.isIncludedInLayout = false
         
         // zipCodeTextFieldView.textField.placeholder = "Zipcode*"
         // zipCodeView.addSubview(zipCodeTextFieldView)
         
         txtZipcode.bindEdgesToSuperview()
         
         //  txtZipcode.nextButton.isHidden = true
         //  txtZipcode.nextButton.flex.isIncludedInLayout = false
         
         // landmarkTextFieldView.textField.placeholder = "Landmark"
         // landmarkView.addSubview(landmarkTextFieldView)
         
         txtLandmark.bindEdgesToSuperview()
         
         //  txtLandmark.nextButton.isHidden = true
         // txtLandmark.nextButton.flex.isIncludedInLayout = false
         
         alternateNumberTextFieldView.textField.placeholder = "Alternate Phone Number"
         alternateNumberView.addSubview(alternateNumberTextFieldView)
         
         txtAlternatePhoneNumber.bindEdgesToSuperview()
         
         txtAlternatePhoneNumber.nextButton.isHidden = true
         txtAlternatePhoneNumber.nextButton.flex.isIncludedInLayout = false*/
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LeaveOrStayVC") as! LeaveOrStayVC
        vc.mainLabelDetails = "Are you sure you want to leave?"
        vc.subLabelDetails = "All the changes you have made in the Address screen will be lost!"
        vc.btnStayDetails = "Stay"
        vc.btnLeaveDetails = "Leave"
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    @IBAction func actionButtonClicked(_ sender: Any) {
        view.endEditing(true)
        if txtAddressLine1.text == "" ||
            txtCity.text == "" ||
            txtState.text == "" ||
            txtZipcode.text == "" {
            Alert.showAlert(message: "Enter all the mandatory fields")
        }
        else if collectionView.indexPathsForSelectedItems == [] {
            Alert.showAlert(message: "Please Select Address Type")
        }
        else {
            let addressLine1 = txtAddressLine1.text
            let appartment = txtAppartment.text
            let city = txtCity.text
            //let state = txtState.text
            let zipCode = txtZipcode.text
            //let landMark = txtLandmark.text
            //let alternatePhoneNumber = txtAlternatePhoneNumber.text
            
            let parameters = [
                "firstname": "sasi",
                "lastname": "dev",
                "company": "MGH Co. Ltd.",
                "address_1": addressLine1!,
                "address_2": appartment!,
                "city": city!,
                "postcode": zipCode!,
                "country_id": "223",
                "zone_id": "3636",
                "default": "0",
                //"state": state!,
                //"landmark": landMark!,
                //"alternatephonenumber": alternatePhoneNumber!,
            ] as [String: Any]
            self.showHUD()
            print(authorizationKey)
            WebService.parseData(urlStr: API.addressList(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
                print("result is:\(result as? NSDictionary)")
                
                let dict = result as! NSDictionary
                if dict["statusCode"] as! Int == 0 {
                    Alert.showAlert(message: dict["message"] as! String)
                }
                else {
                    let dictData = AddAccountAddressResponse(json: result as! [String : Any])
                    if dictData != nil {
                        /*let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                            (alert: UIAlertAction!) in
                            let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                            vc.authorizationKey = self.authorizationKey
                            vc.isFromUpdateProfile = true
                            DispatchQueue.main.async {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }))
                        Alert.showAlertforOkAction(message: dictData!.message, okAction: okAction)*/
                        
                        Alert.showAlert(message: dictData!.message)
                    }
                    else {
                        // * Failure
                        Alert.showAlert(message: "Unknown error, Please try again!")
                    }
                }
                self.dismissHUD(isAnimated: true)
            }) { (errMsg) in
                self.dismissHUD(isAnimated: true)
                Alert.showAlert(message: errMsg)
            }
        }
    }
}

extension AddAddressVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddAddressCell", for: indexPath) as! AddAddressCell
        cell.titleLabel.text = titleArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath)-> CGSize {
        return CGSize(width: 90, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  let cell = collectionView.cellForItem(at: indexPath) as? AddAddressCell {
            cell.select()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if  let cell = collectionView.cellForItem(at: indexPath) as? AddAddressCell {
            cell.deselect()
        }
    }
}

/*   extension UITextField {
 func useUnderline() -> Void {
 let border = CALayer()
 let borderWidth = CGFloat(2.0) // Border Width
 border.borderColor = UIColor.orange.cgColor
 border.frame = CGRect(origin: CGPoint(x: 0,y :self.frame.size.height - borderWidth), size: CGSize(width: self.frame.size.width, height: self.frame.size.height))
 border.borderWidth = borderWidth
 self.layer.addSublayer(border)
 self.layer.masksToBounds = true
 }
 }   */

/*  extension UITextField {
 func addBottomBorder(){
 let bottomLine = CALayer()
 bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
 bottomLine.backgroundColor = UIColor.customOrange().cgColor
 borderStyle = .none
 layer.addSublayer(bottomLine)
 }
 }   */
