//
//  AddressListVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/30/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class AddressListVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addNewButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var authorizationKey = ""
    var addressesArr: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getAddressList()
        setup()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setup() {
        tableView.register(UINib(nibName: "AddressListCell", bundle: nil), forCellReuseIdentifier: "AddressListCell")
        addNewButton.layer.cornerRadius = 15
        addNewButton.layer.masksToBounds = true
    }
    
    @IBAction func addNewButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAddressList() {
        self.showHUD()
        print(GlobalMethods.retriveAuthKey())
        print(authorizationKey)
        WebService.parseData(urlStr: API.addressList(), parameters: nil, authorizationKey: GlobalMethods.retriveAuthKey(), method: .GET, showHud: true, successHandler: { (result) in
            
            print("ACCOUNT DETAIlS:\(result)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = AccountAddressListResponse(json: result as! [String : Any])
                if dictData != nil {
                    self.addressesArr = dictData!.data
                    self.tableView.reloadData()
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
}

extension AddressListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  addressesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCellIdentifier", for: indexPath) as! AddressListCell
        
        /*cell.layer.masksToBounds = false
         cell.layer.shadowColor = UIColor.gray.cgColor
         cell.layer.shadowOpacity = 1.0
         cell.layer.shadowOffset = CGSize(width: 0, height: 2)
         cell.layer.shadowRadius = 5
         cell.layer.cornerRadius = 15*/
        
        cell.addressView.layer.cornerRadius = 8
        cell.addressView.layer.masksToBounds = false
        cell.addressView.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.addressView.layer.shadowColor = UIColor.black.cgColor
        cell.addressView.layer.shadowOpacity = 0.23
        cell.addressView.layer.shadowRadius = 4
        
        cell.configure(address: addressesArr[indexPath.row] as! NSDictionary)
        return cell
        
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 200
     }*/
    
    /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //let cell = tableView.dequeueReusableCell(withIdentifier: "AddressListCell", for: indexPath) as! AddressListCell
     /*cell.containerView.layer.shadowColor = UIColor.gray.cgColor
     cell.containerView.layer.shadowOpacity = 1.0
     cell.containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
     cell.containerView.layer.shadowRadius = 5
     cell.containerView.layer.cornerRadius = 15*/
     
     let address = addressesArr[indexPath.row] as! NSDictionary
     
     var addressType = ""
     var postCode = ""
     var city = ""
     var state = ""
     if let type = address["address_type"] as? String {
     addressType = type
     }
     if let code = address["postcode"] as? String {
     postCode = code
     }
     if let x = address["city"] as? String {
     city = x
     }
     if let y = address["zone"] as? String {
     state = y
     }
     
     let addressTypeZip = addressType + " " + postCode
     let addressTypeZipCity = addressTypeZip + " " + city
     let addresstypeZipCityState = addressTypeZipCity + " " + state
     
     
     var firstName = ""
     var lastName = ""
     var company = ""
     var addLine1 = ""
     var addLine2 = ""
     
     if let fName = address["firstname"] as? String {
     firstName = fName
     }
     
     if let lName = address["lastname"] as? String {
     lastName = lName
     }
     
     if let companyName = address["company"] as? String {
     company = companyName
     }
     
     if let line1 = address["address_1"] as? String {
     addLine1 = line1
     }
     
     if let line2 = address["address_2"] as? String {
     addLine2 = line2
     }
     
     let name = firstName + " " + lastName
     let nameCompany = name + " " + company
     let nameCompanyAddress = nameCompany + "" + addresstypeZipCityState
     selectedAddress = nameCompanyAddress
     self.navigationController?.popViewController(animated: true)
     }   */
}


