//
//  AddPhotoCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 04/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class AddPhotoCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 18)
        iconImageView.image = UIImage(named: "photo")
        iconImageView.tintColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
