//
//  UpdateProfileVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/25/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit


class UpdateProfileVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    
    var authorizationKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        txtFirstName.addBottomBorder()
        txtLastName.addBottomBorder()
        txtEmail.addBottomBorder()
        txtMobileNumber.addBottomBorder()
        setup()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setup() {
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        
        changePasswordButton.layer.cornerRadius = 15
        changePasswordButton.layer.masksToBounds = true
        changePasswordButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 11)
        changePasswordButton.addTarget(self, action: #selector(changePasswordClicked), for: .touchUpInside)
        
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.borderColor = UIColor.lightOrange().cgColor
        imgProfile.layer.borderWidth = 5
        
        /*    txtFirstName.placeholder = "First
         txtFirstName.addSubview(firstNameTextFieldView)
         txtFirstName.bindEdgesToSuperview()
         txtFirstName.nextButton.isHidden = true
         
         
         lastNameTextFieldView.textField.placeholder = "Last Name*"
         txtLastName.addSubview(lastNameTextFieldView)
         txtLastName.bindEdgesToSuperview()
         txtLastName.nextButton.isHidden = true
         txtLastName.nextButton.flex.isIncludedInLayout = false
         
         emailTextFieldView.textField.placeholder = "Email*"
         emailView.addSubview(emailTextFieldView)
         
         txtEmail.bindEdgesToSuperview()
         
         txtEmail.nextButton.isHidden = true
         txtEmail.nextButton.flex.isIncludedInLayout = false
         
         mobileTextFieldView.textField.placeholder = "Mobile Number*"
         mobileView.addSubview(mobileTextFieldView)
         
         txtMobileNumber.bindEdgesToSuperview()
         
         txtMobileNumber.nextButton.isHidden = true
         txtMobileNumber.nextButton.flex.isIncludedInLayout = false     */
        
        notNowButton.layer.borderWidth = 2
        notNowButton.layer.borderColor = UIColor.customOrange().cgColor
        notNowButton.layer.cornerRadius = 25
        notNowButton.layer.masksToBounds = true
        notNowButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        //notNowButton.layer.borderWidth = 2
        //notNowButton.layer.borderColor = UIColor.orange.cgColor
        
        updateButton.layer.cornerRadius = 25
        updateButton.layer.masksToBounds = true
        updateButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LeaveOrStayVC") as! LeaveOrStayVC
        vc.mainLabelDetails = "Are you sure you want to leave?"
        vc.subLabelDetails = "All the changes you have made in the profile screen will be lost!"
        vc.btnStayDetails = "Stay"
        vc.btnLeaveDetails = "Leave"
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    @IBAction func changePasswordClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func addPhotoButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddPhotoVC") as! AddPhotoVC
        
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    @IBAction func notNowButtonTapped(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LeaveOrStayVC") as! LeaveOrStayVC
        vc.mainLabelDetails = "Are you sure you want to leave?"
        vc.subLabelDetails = "All the changes you have made in the profile screen will be lost!"
        vc.btnStayDetails = "Stay"
        vc.btnLeaveDetails = "Leave"
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        view.endEditing(true)
        
        if txtFirstName.text == "" || txtLastName.text == "" || txtEmail.text == "" || txtMobileNumber.text == "" {
            Alert.showAlert(message: "Enter all the mandatory fields")
        }
        else {
            let firstName = txtFirstName.text
            let lastName = txtLastName.text
            let emailId = txtEmail.text
            let mobileNumber = txtMobileNumber.text
            
            let parameters = [
                "firstname": firstName!,
                "lastname": lastName!,
                "email": emailId!,
                "telephone": mobileNumber!,
                "telephone_dial_code": "91",
                "telephone_iso2_code": "in"
            ] as [String: Any]
            self.showHUD()
            print(parameters)
            print(authorizationKey)
            WebService.parseData(urlStr: API.updateProfile(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in

                print("result is:\(result as? NSDictionary)")
                
                let dict = result as! NSDictionary
                if dict["statusCode"] as! Int == 0 {
                    Alert.showAlert(message: dict["message"] as! String)
                }
                else {
                    let dictData = SuccessResponse(json: result as! [String : Any])
                    if dictData != nil {
                        // * Success
                        let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                            (alert: UIAlertAction!) in
                            /*let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                            vc.authorizationKey = self.authorizationKey
                            //vc.mobileNumber = self.mobileNumber
                            vc.isFromUpdateProfile = true
                            DispatchQueue.main.async {
                                self.navigationController?.pushViewController(vc, animated: true)
                            }*/
                        }))
                        
                        Alert.showAlertforOkAction(message: dict["message"] as! String, okAction: okAction)
                    }
                    else {
                        // * Failure
                        Alert.showAlert(message: "Unknown error, Please try again!")
                    }
                }
                self.dismissHUD(isAnimated: true)
            }) { (errMsg) in
                self.dismissHUD(isAnimated: true)
                Alert.showAlert(message: errMsg)
            }
        }
    }
}

/*
 extension UIView {
 /// Adds constraints to the superview so that this view has same size and position.
 /// Note: This fails the build if the `superview` is `nil` – add it as a subview before calling this.
 func bindEdgesToSuperview() {
 guard let superview = superview else {
 preconditionFailure("`superview` was nil – call `addSubview(view: UIView)` before calling `bindEdgesToSuperview()` to fix this.")
 }
 translatesAutoresizingMaskIntoConstraints = false
 ["H:|-0-[subview]-0-|", "V:|-0-[subview]-0-|"].forEach { visualFormat in
 superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: visualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
 }
 }
 }     */


