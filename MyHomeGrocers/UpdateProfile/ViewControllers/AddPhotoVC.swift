//
//  AddPhotoVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 04/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class AddPhotoVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var authorizationKey = ""
    var titleArray = ["Photo Library","Take Photo","Browse"]
    var imageArray = [UIImage(systemName: "photo.fill.on.rectangle.fill"),UIImage(systemName: "camera.fill") ,UIImage(systemName: "folder.fill")]
   
    
    override func viewDidLoad() {
      
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        tableView.layer.cornerRadius = 20
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPhotoVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPhotoCell", for: indexPath) as! AddPhotoCell
        //let image = imageArray[indexPath.row]
        cell.titleLabel.text = self.titleArray[indexPath.row]
        cell.iconImageView.image = self.imageArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.navigationController?.popViewController(animated: true)
      
            
        }
        

    }
}


        
