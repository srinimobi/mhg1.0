//
//  ChangePasswordVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/25/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var currentPasswordView: UIView!
    
    let currentPasswordTextFieldView = TextFieldView()
    let newPasswordTextFieldView = TextFieldView()
    let confirmPasswordTextFieldView = TextFieldView()
    
    var authorizationKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup() {
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        
        passwordTitleLabel.font = UIFont(name: "Poppins-Bold", size: 11)
        label1.font = UIFont(name: "Poppins-Regular", size: 11)
        label2.font = UIFont(name: "Poppins-Regular", size: 11)
        label3.font = UIFont(name: "Poppins-Regular", size: 11)
        
        actionButton.layer.cornerRadius = 25
        actionButton.layer.masksToBounds = true
        actionButton.backgroundColor = UIColor.customOrange()
        actionButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        currentPasswordTextFieldView.textField.placeholder = "Current Password*"
        currentPasswordTextFieldView.textField.isSecureTextEntry = true
        currentPasswordView.addSubview(currentPasswordTextFieldView)
        currentPasswordTextFieldView.bindEdgesToSuperview()
        currentPasswordTextFieldView.nextButton.isHidden = true
        currentPasswordTextFieldView.nextButton.flex.isIncludedInLayout = false
        
        newPasswordTextFieldView.textField.placeholder = "New Password*"
        newPasswordTextFieldView.textField.isSecureTextEntry = true
        newPasswordView.addSubview(newPasswordTextFieldView)
        newPasswordTextFieldView.bindEdgesToSuperview()
        newPasswordTextFieldView.nextButton.isHidden = true
        newPasswordTextFieldView.nextButton.flex.isIncludedInLayout = false
        
        confirmPasswordTextFieldView.textField.placeholder = "Confirm New Password*"
        confirmPasswordTextFieldView.textField.isSecureTextEntry = true
        confirmPasswordView.addSubview(confirmPasswordTextFieldView)
        confirmPasswordTextFieldView.bindEdgesToSuperview()
        confirmPasswordTextFieldView.nextButton.isHidden = true
        confirmPasswordTextFieldView.nextButton.flex.isIncludedInLayout = false
    }

    @IBAction func backButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LeaveOrStayVC") as! LeaveOrStayVC
        vc.mainLabelDetails = "Are you sure you want to leave?"
        vc.subLabelDetails = "All the changes you have made in the password screen will be lost!"
        vc.btnStayDetails = "Stay"
        vc.btnLeaveDetails = "Leave"
        let navController = UINavigationController.init(rootViewController: vc)
        navController.modalPresentationStyle = .overCurrentContext
        navController.navigationController?.isNavigationBarHidden = true
        self.present(navController, animated: true, completion: {})
    }
    
    
    @IBAction func actionButtonTapped(_ sender: Any) {
            view.endEditing(true)
            if currentPasswordTextFieldView.textField.text == "" || newPasswordTextFieldView.textField.text == "" || confirmPasswordTextFieldView.textField.text == ""  {
                Alert.showAlert(message: "Enter all the mandatory fields")
            } else if newPasswordTextFieldView.textField.text != confirmPasswordTextFieldView.textField.text {
                Alert.showAlert(message: "Passwords do not match")
            } else {
                let currentPassword = currentPasswordTextFieldView.textField.text
                let newPassword = newPasswordTextFieldView.textField.text
                let confirmPassword = confirmPasswordTextFieldView.textField.text
                
                let parameters = [
                    "password": newPassword!,
                    "confirm": confirmPassword!
                    ] as [String: Any]
                
                print(parameters)
                WebService.parseData(urlStr: API.changePassword(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
                    print("result is:\(result as? NSDictionary)")
                    
                    let dict = result as! NSDictionary
                    
                    //    if let statusDict = dict["status"] as? NSDictionary {
                            if dict["statusDict"] as! Int == 1 {
                                let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                                    (alert: UIAlertAction!) in
//                                    let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
//                                    vc.authorizationKey = self.authorizationKey
//                                    vc.isFromUpdateProfile = true
                                    DispatchQueue.main.async {
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                }))

                                Alert.showAlertforOkAction(message: "Password has been updated", okAction: okAction)
                               // Alert.showAlert(message: "Password has been updated")
                            }
                   //     }
                    
                        
                    else if let errorDict = dict["error"] as? NSDictionary {
                        if errorDict["password"] != nil {
                            Alert.showAlert(message: errorDict["password"] as! String)
                        }
                    }
                        //    else if let statusDict = dict["status"] as? NSDictionary {
                        if dict["statusCode"] as! Int == 0 {
                            if dict["message"] as! String == "" {
                                Alert.showAlert(message: "Password confirmation does not match password!")
                            } else {
                            Alert.showAlert(message: dict["message"] as! String)
                            }
                        }
               //     }
                        
                    else {
                        Alert.showAlert(message: "Unknown error, Please try again!")
                    }
                    
                }) { (errMsg) in
                    Alert.showAlert(message: errMsg)
                }
            }
            
        }
}

extension UIView {
    /// Adds constraints to the superview so that this view has same size and position.
    /// Note: This fails the build if the `superview` is `nil` – add it as a subview before calling this.
    func bindEdgesToSuperview() {
        guard let superview = superview else {
            preconditionFailure("`superview` was nil – call `addSubview(view: UIView)` before calling `bindEdgesToSuperview()` to fix this.")
        }
        translatesAutoresizingMaskIntoConstraints = false
        ["H:|-0-[subview]-0-|", "V:|-0-[subview]-0-|"].forEach { visualFormat in
            superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: visualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        }
    }
} 
