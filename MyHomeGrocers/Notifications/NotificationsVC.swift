//
//  NotificationsVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/31/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var offersButton: UIButton!
    @IBOutlet weak var updatesButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    var isOffers = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup() {
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = true
        offersButton.layer.cornerRadius = 15
        offersButton.layer.masksToBounds = true
        updatesButton.layer.cornerRadius = 15
        updatesButton.layer.masksToBounds = true
        
        updatesButton.backgroundColor = UIColor.customOrange()
        updatesButton.setTitleColor(.white, for: .normal)
        updatesButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        offersButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        offersButton.backgroundColor = .clear
        offersButton.setTitleColor(.black, for: .normal)
        
        tableView.tableFooterView = UIView()
    }

    @IBAction func updatesButtonClicked(_ sender: Any) {
        updatesButton.backgroundColor = UIColor.customOrange()
        updatesButton.setTitleColor(.white, for: .normal)
        offersButton.backgroundColor = .clear
        offersButton.setTitleColor(.black, for: .normal)
        isOffers = false
        self.tableView.reloadData()
    }
    
    
    @IBAction func offersButtonClicked(_ sender: Any) {
        offersButton.backgroundColor = UIColor.customOrange()
        offersButton.setTitleColor(.white, for: .normal)
        updatesButton.backgroundColor = .clear
        updatesButton.setTitleColor(.black, for: .normal)
        isOffers = true
        self.tableView.reloadData()
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NotificationsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isOffers {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OfferTableCell", for: indexPath) as! OfferTableCell
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveredTableCell", for: indexPath) as! DeliveredTableCell
            if cell.titleLabel.text == "Processing" || cell.titleLabel.text == "Order Accepted" {
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    return 120
                }
                cell.btnRateOrder.isHidden = true
                cell.btnReOrder.isHidden = true
                cell.containerView.addBottomShadow();
                cell.containerView.layer.shadowOpacity = 0.4
                cell.containerView.layer.shadowColor = UIColor.customOrange().cgColor
                cell.containerView.layer.shadowRadius = 2
                
                return cell
            }
            
                
            cell.titleLabel.textColor = UIColor.black
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isOffers {
            return 120
        }
        return 180
    }
}
