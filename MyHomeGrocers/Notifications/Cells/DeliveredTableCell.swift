//
//  DeliveredTableCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/31/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class DeliveredTableCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnRateOrder: UIButton!
    @IBOutlet weak var btnReOrder: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.layer.cornerRadius = 20
//        containerView.layer.masksToBounds = false
//        containerView.clipsToBounds = false
//        
//        containerView.layer.shadowColor = UIColor.customOrange().cgColor
//        containerView.layer.shadowOpacity = 1.0
//        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
//        containerView.layer.shadowRadius = 5
        
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        titleLabel.textColor = UIColor.customOrange()
        //titleLabel.text = "Order Accepted"
        titleLabel.text = "Delivered"
        
        descriptionLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        descriptionLabel.textColor = UIColor.descriptionGray()
        
        dateLabel.font = UIFont(name: "Poppins-Light", size: 10)
        dateLabel.textColor = UIColor.descriptionGray()
        
        btnRateOrder.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        btnRateOrder.layer.cornerRadius = 20
        btnRateOrder.layer.masksToBounds = true
        btnReOrder.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        btnReOrder.layer.cornerRadius = 20
        btnReOrder.layer.masksToBounds = true
        btnRateOrder.layer.borderColor = UIColor.customOrange().cgColor
        btnRateOrder.layer.borderWidth = 2
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
