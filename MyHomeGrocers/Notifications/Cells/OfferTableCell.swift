//
//  OfferTableCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/31/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class OfferTableCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        containerView.layer.cornerRadius = 30
//        containerView.layer.masksToBounds = false
//        containerView.clipsToBounds = false
//        
//        containerView.layer.shadowColor = UIColor.customOrange().cgColor
//        containerView.layer.shadowOpacity = 1.0
//        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
//        containerView.layer.shadowRadius = 5
        
        titleLabel.font = UIFont(name: "Poppins-SemiBold", size: 15)
        titleLabel.textColor = .black
        
        descriptionLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        descriptionLabel.textColor = UIColor.descriptionGray()
        
        dateLabel.font = UIFont(name: "Poppins-Light", size: 10)
        dateLabel.textColor = UIColor.descriptionGray()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
