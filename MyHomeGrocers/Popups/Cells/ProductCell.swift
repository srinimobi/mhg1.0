//
//  ProductCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 11/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var newPriceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.layer.cornerRadius = 20
        contentView.layer.masksToBounds = true
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowRadius = 5
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.layer.borderColor = UIColor.customOrange().cgColor
        imageView.layer.borderWidth = 5
        
        addToCartButton.layer.cornerRadius = addToCartButton.frame.size.height/2
        addToCartButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 11)
        
        
        nameLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        quantityLabel.font = UIFont(name: "Poppins-Semibold", size: 10)
        newPriceLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        oldPriceLabel.font = UIFont(name: "Poppins-Medium", size: 10)
        percentageLabel.font = UIFont(name: "Poppins-Medium", size: 12)
    }
    
}
