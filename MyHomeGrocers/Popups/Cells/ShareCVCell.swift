//
//  ShareCVCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 10/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ShareCVCell: UICollectionViewCell {
    
    @IBOutlet weak var shareLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = contentView.layer.frame.size.height/2
        contentView.layer.borderWidth = 5
        contentView.layer.borderColor = UIColor.descriptionGray().cgColor
        
        shareLabel.font = UIFont(name: "Poppins-Regular", size: 10)
    }
    
}
