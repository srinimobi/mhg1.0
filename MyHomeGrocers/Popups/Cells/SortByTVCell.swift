//
//  SortByTVCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 10/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SortByTVCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var radioButton: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        typeLabel.font = UIFont(name: "Poppins-Medium", size: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
