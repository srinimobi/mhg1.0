//
//  SelectTimeCell.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 13/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SelectTimeCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        timeLabel.font = UIFont(name: "Poppins-Semibold", size: 13)
        
    }
    
    func selected() {
        contentView.backgroundColor = UIColor.customOrange()
        timeLabel.textColor = UIColor.white
        timeLabel.backgroundColor = UIColor.clear
    }
    
    func deselected() {
        contentView.backgroundColor = UIColor.white
        timeLabel.textColor = UIColor.black
        timeLabel.backgroundColor = UIColor.clear
    }
    
}
