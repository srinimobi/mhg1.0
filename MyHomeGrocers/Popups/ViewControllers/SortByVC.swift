//
//  SortByVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 10/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SortByVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var sortbyArr: NSArray = []
    var filterbyArr: NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func applyButtonTapped(_ sender: Any) {
        
    }
    
    func setup() {
        
        containerView.layer.cornerRadius = 30
        containerView.layer.masksToBounds = true
        
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 23)
        applyButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 25)
        applyButton.layer.cornerRadius = 15
        applyButton.layer.masksToBounds = true
    }

}

extension SortByVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortByTVCell", for: indexPath) as! SortByTVCell
        return cell
    }
    
    
}
