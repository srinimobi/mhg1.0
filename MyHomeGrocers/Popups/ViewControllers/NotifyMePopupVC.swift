//
//  NotifyMePopupVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 13/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class NotifyMePopupVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var notifyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 30
        
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        emailTextField.font = UIFont(name: "Poppins-Regular", size: 15)
        
        notifyButton.layer.masksToBounds = true
        notifyButton.layer.cornerRadius = notifyButton.frame.size.height/2
        notifyButton.titleLabel?.font = UIFont(name: "Poppins-Semibold", size: 23)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func notifyButtonClicked(_ sender: Any) {
        
    }
    
}
