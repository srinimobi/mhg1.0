//
//  ShareVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 10/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class ShareVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var siteLabel: UILabel!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var dismissBar: UIButton!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var linkView: UIView!
    @IBOutlet weak var firstCollectionView: UICollectionView!
    @IBOutlet weak var secondCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissBarTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func copyButtonTapped(_ sender: Any) {
        
    }
    
    
    func setup() {
        
        containerView.layer.cornerRadius = 30
        containerView.layer.masksToBounds = true
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.layer.frame.size.height/2
        imageView.layer.borderColor = UIColor.customOrange().cgColor
        imageView.layer.borderWidth = 5
        
        linkView.layer.masksToBounds = true
        linkView.layer.cornerRadius = 15
        linkView.layer.borderColor = UIColor.customOrange().cgColor
        linkView.layer.borderWidth = 1
        
        nameLabel.font = UIFont(name: "Poppins-Semibold", size: 15)
        siteLabel.font = UIFont(name: "Poppins-Regular", size: 10)
        linkLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        
        dismissBar.layer.masksToBounds = true
        dismissBar.layer.cornerRadius = dismissBar.layer.frame.size.height/2
        
    }

}

extension ShareVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView1: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView1: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView1.dequeueReusableCell(withReuseIdentifier: "ShareCVCell", for: indexPath) as! ShareCVCell
        return cell
    }
    
   /* func collectionView(_ collectionView1: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView1.dequeueReusableCell(withReuseIdentifier: "SelectDateCell", for: indexPath) as! SelectDateCell
        cell.select()
    }*/
    
}

