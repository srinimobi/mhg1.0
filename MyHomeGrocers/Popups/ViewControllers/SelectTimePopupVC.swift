//
//  SelectTimePopupVC.swift
//  MyHomeGrocers
//
//  Created by Sasi_D_King on 13/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import UIKit

class SelectTimePopupVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UIView!
    
    var timeArr: NSArray = ["17:00:00-18:00:00","18:00:00-19:00:00","19:00:00-20:00:00"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 30
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectTimePopupVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTimeCell", for: indexPath) as! SelectTimeCell
        cell.timeLabel.text = timeArr[indexPath.row]
        return cell
        
    }
    
    
}
