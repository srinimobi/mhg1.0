//
//  WelcomeImageCollectionCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/29/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class WelcomeImageCollectionCell: UICollectionViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundImageView.clipsToBounds = true
        subImageView.contentMode = .scaleAspectFit
        
        titleLabel.font = UIFont(name: "Poppins-Bold", size: 45)
        titleLabel.textColor = UIColor.white
        
        descriptionLabel.font = UIFont(name: "Poppins-Medium", size: 20)
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.numberOfLines = 0
    }
}
