//
//  TextFieldCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/26/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {

    let textFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 56, height: 60))

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addSubview(textFieldView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
