//
//  DoubleTextFieldCell.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/27/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class DoubleTextFieldCell: UITableViewCell {
    
    let firstNameTextFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: (UIScreen.main.bounds.width - 56)/2, height: 60))
    let lastNameTextFieldView = TextFieldView(frame: CGRect(x: 8, y: 0, width: (UIScreen.main.bounds.width - 56)/2 - 8, height: 60))

    @IBOutlet weak var secondContainer: UIView!
    @IBOutlet weak var firstContainer: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        firstContainer.addSubview(firstNameTextFieldView)
        secondContainer.addSubview(lastNameTextFieldView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
