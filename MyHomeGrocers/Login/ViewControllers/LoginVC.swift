//
//  LoginVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 7/13/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class LoginVC: UIViewController, UITextFieldDelegate {
    let root = UIView()
    let topView = LoginTopView()
    let greetingView = GreetingView()
    let containerView = UIView()
    let nameContainer = UIView()
    let firstName = TextFieldView()
    let lastName = TextFieldView()
    let emailView = TextFieldView()
    let passwordView = TextFieldView()
    let mobileView = TextFieldView()
    let forgotPasswordView = ForgotPasswordView()
    let socialLoginView = SocialLoginView()
    let actionButtonView = ActionButtonView()
    let backAndLogoView = BackAndLogoView()
    let skipButton = UIButton()
    let changeView = ChangeEmailOrNumberView()
    let bottomTermsView = BottomTermsView()
    let otpView = OTPView()
    
    var authorizationKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout()
        setup()
        topView.loginButton.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
        topView.signupButton.addTarget(self, action: #selector(signupButtonClicked), for: .touchUpInside)
        loginButtonClicked()
        
        forgotPasswordView.actionButton.addTarget(self, action: #selector(forgotPasswordButtonClicked), for: .touchUpInside)
        backAndLogoView.backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        root.pin.all(view.pin.safeArea)
        root.flex.layout()
    }
    
    func layout() {
        root.flex.define { (flex) in
            flex.addItem(topView).marginTop(50).height(100).width(UIScreen.main.bounds.width)
            flex.addItem(backAndLogoView).marginTop(50).height(100).width(UIScreen.main.bounds.width)
            
            flex.addItem(greetingView).marginTop(80)
            flex.addItem(containerView).marginTop(50).width(UIScreen.main.bounds.width).define { (flex) in
                
                flex.addItem(changeView).marginTop(24)
                
                flex.addItem(skipButton).width(102).height(30).marginRight(20).marginTop(24).alignSelf(.end)
                flex.addItem(nameContainer).direction(.row).marginTop(50).define { (flex) in
                    flex.addItem(firstName).width(42%).marginLeft(28)
                    flex.addItem(lastName).width(42%).marginLeft(8).marginRight(28)
                }
                flex.addItem(mobileView).height(50).marginTop(50).marginHorizontal(28)
                flex.addItem(emailView).height(50).marginTop(20).marginHorizontal(28)
                flex.addItem(passwordView).marginTop(20).height(50).marginHorizontal(28)
                
                flex.addItem(otpView).marginTop(20).height(180)
                flex.addItem(forgotPasswordView).marginTop(16).marginRight(28).height(50)
                flex.addItem(socialLoginView).marginHorizontal(28).marginTop(24)
                flex.addItem(actionButtonView).marginTop(32)
                flex.addItem(bottomTermsView).marginTop(32)
                //                flex.addItem().height(100)
                flex.addItem().marginTop(-20).height(1000)
            }.backgroundColor(.white)
        }
        self.view.addSubview(root)
    }
    
    func setup() {
        skipButton.setTitleColor(UIColor.customOrange(), for: .normal)
        skipButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        skipButton.backgroundColor = .clear
        skipButton.setTitle(">>SKIP", for: .normal)
        
        emailView.textField.delegate = self
        passwordView.textField.delegate = self
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 25
        
        actionButtonView.actionButton.addTarget(self, action: #selector(actionbuttonTapped), for: .touchUpInside)
        
        changeView.backButton.addTarget(self, action: #selector(changeBackButtonClicked), for: .touchUpInside)
        
        skipButton.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)
    }
    
    @objc func actionbuttonTapped() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if actionButtonView.actionButton.titleLabel?.text == "Login" {
            let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            self.navigationController?.pushViewController(dashboardVC, animated: true)
        } else {
            configurePasswordView()
        }
    }
    
    @objc func changeBackButtonClicked() {
        loginButtonClicked()
    }
    
    @objc func loginButtonClicked() {
        configure(isLogin: true, isChangePassword: false)
        topView.configure(isLogin: true)
        greetingView.configure(isLogin: true)
        actionButtonView.configure(isLogin: true)
    }
    
    @objc func signupButtonClicked() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(dashboardVC, animated: false)
    }
    
    @objc func forgotPasswordButtonClicked() {
        configure(isLogin: false, isChangePassword: true)
        greetingView.configurePasswordView()
        actionButtonView.configurePasswordView()
    }
    
    @objc func backButtonClicked() {
        loginButtonClicked()
    }
    
    @objc func skipButtonTapped() {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
    
    func configure(isLogin: Bool, isChangePassword: Bool) {
        if isChangePassword {
            topView.isHidden = true
            topView.flex.isIncludedInLayout = false
            backAndLogoView.isHidden = false
            backAndLogoView.flex.isIncludedInLayout = true
            
            forgotPasswordView.isHidden = true
            forgotPasswordView.flex.isIncludedInLayout = false
            socialLoginView.isHidden = true
            socialLoginView.flex.isIncludedInLayout = false
            
            nameContainer.isHidden = true
            nameContainer.flex.isIncludedInLayout = false
            emailView.isHidden = false
            emailView.flex.isIncludedInLayout = true
            emailView.nextButton.isHidden = true
            emailView.nextButton.flex.isIncludedInLayout = false
            mobileView.nextButton.isHidden = true
            mobileView.nextButton.flex.isIncludedInLayout = false
            passwordView.nextButton.isHidden = true
            passwordView.nextButton.flex.isIncludedInLayout = false
            
            skipButton.isHidden = true
            skipButton.flex.isIncludedInLayout = false
            
            changeView.isHidden = true
            changeView.flex.isIncludedInLayout = false
            
            emailView.textField.placeholder = "Current Password"
            mobileView.textField.placeholder = "New Password"
            passwordView.textField.placeholder = "Reenter New Password"
            
            bottomTermsView.isHidden = true
            bottomTermsView.flex.isIncludedInLayout = false
            
            changeView.isHidden = true
            
            otpView.isHidden = true
            otpView.flex.isIncludedInLayout = false
        }
        else if isLogin {
            topView.isHidden = false
            topView.flex.isIncludedInLayout = true
            backAndLogoView.isHidden = true
            backAndLogoView.flex.isIncludedInLayout = false
            
            nameContainer.isHidden = true
            nameContainer.flex.isIncludedInLayout = false
            skipButton.isHidden = false
            skipButton.flex.isIncludedInLayout = true
            changeView.isHidden = true
            changeView.flex.isIncludedInLayout = false
            emailView.isHidden = true
            emailView.flex.isIncludedInLayout = false
            passwordView.isHidden = true
            passwordView.flex.isIncludedInLayout = false
            
            forgotPasswordView.isHidden = false
            forgotPasswordView.flex.isIncludedInLayout = true
            socialLoginView.isHidden = false
            socialLoginView.flex.isIncludedInLayout = true
            
            mobileView.isHidden = false
            mobileView.flex.isIncludedInLayout = true
            mobileView.textField.placeholder = "Email ID / Mobile Number"
            //            passwordView.textField.placeholder = "Password"
            mobileView.nextButton.isHidden = false
            mobileView.nextButton.flex.isIncludedInLayout = true
            mobileView.nextButton.addTarget(self, action: #selector(actionbuttonTapped), for: .touchUpInside)
            
            actionButtonView.isHidden = true
            actionButtonView.flex.isIncludedInLayout = false
            
            forgotPasswordView.isHidden = true
            forgotPasswordView.flex.isIncludedInLayout = false
            
            bottomTermsView.isHidden = false
            bottomTermsView.flex.isIncludedInLayout = true
            
            otpView.isHidden = true
            otpView.flex.isIncludedInLayout = false
        } else {
            topView.isHidden = false
            topView.flex.isIncludedInLayout = true
            backAndLogoView.isHidden = true
            backAndLogoView.flex.isIncludedInLayout = false
            
            forgotPasswordView.isHidden = true
            forgotPasswordView.flex.isIncludedInLayout = false
            socialLoginView.isHidden = true
            socialLoginView.flex.isIncludedInLayout = false
            
            nameContainer.isHidden = false
            nameContainer.flex.isIncludedInLayout = true
            firstName.nextButton.isHidden = true
            firstName.nextButton.flex.isIncludedInLayout = false
            lastName.nextButton.isHidden = true
            lastName.nextButton.flex.isIncludedInLayout = false
            emailView.isHidden = false
            emailView.flex.isIncludedInLayout = true
            emailView.nextButton.isHidden = true
            emailView.nextButton.flex.isIncludedInLayout = false
            mobileView.nextButton.isHidden = true
            mobileView.nextButton.flex.isIncludedInLayout = false
            
            passwordView.isHidden = false
            passwordView.flex.isIncludedInLayout = true
            passwordView.nextButton.isHidden = true
            passwordView.nextButton.flex.isIncludedInLayout = false
            
            skipButton.isHidden = true
            skipButton.flex.isIncludedInLayout = false
            changeView.isHidden = true
            changeView.flex.isIncludedInLayout = false
            actionButtonView.isHidden = false
            actionButtonView.flex.isIncludedInLayout = true
            
            firstName.textField.placeholder = "First Name*"
            lastName.textField.placeholder = "Last Name*"
            emailView.textField.placeholder = "Email ID"
            mobileView.textField.placeholder = "Mobile Number*"
            passwordView.textField.placeholder = "Password"
            
            bottomTermsView.isHidden = true
            bottomTermsView.flex.isIncludedInLayout = false
            
            otpView.isHidden = true
            otpView.flex.isIncludedInLayout = false
        }
        
        nameContainer.flex.markDirty()
        emailView.flex.markDirty()
        mobileView.flex.markDirty()
        passwordView.flex.markDirty()
        forgotPasswordView.flex.markDirty()
        socialLoginView.flex.markDirty()
        actionButtonView.flex.markDirty()
        
        view.setNeedsLayout()
    }
    
    func configurePasswordView() {
        topView.isHidden = false
        topView.flex.isIncludedInLayout = true
        backAndLogoView.isHidden = true
        backAndLogoView.flex.isIncludedInLayout = false
        
        nameContainer.isHidden = true
        nameContainer.flex.isIncludedInLayout = false
        emailView.isHidden = true
        emailView.flex.isIncludedInLayout = false
        passwordView.isHidden = true
        passwordView.flex.isIncludedInLayout = false
        
        mobileView.isHidden = true
        mobileView.flex.isIncludedInLayout = false
        
        otpView.isHidden = false
        otpView.flex.isIncludedInLayout = true
        
        forgotPasswordView.isHidden = false
        forgotPasswordView.flex.isIncludedInLayout = true
        
        socialLoginView.isHidden = true
        socialLoginView.flex.isIncludedInLayout = false
        
        bottomTermsView.isHidden = true
        bottomTermsView.flex.isIncludedInLayout = false
        
        skipButton.isHidden = true
        skipButton.flex.isIncludedInLayout = false
        
        changeView.isHidden = false
        changeView.flex.isIncludedInLayout = true
        mobileView.textField.text = ""
        mobileView.textField.placeholder = "Password"
        
        actionButtonView.checkboxButton.isHidden = true
        actionButtonView.checkboxButton.flex.isIncludedInLayout = false
        actionButtonView.actionButton.setTitle("Login", for: .normal)
        
        nameContainer.flex.markDirty()
        emailView.flex.markDirty()
        mobileView.flex.markDirty()
        passwordView.flex.markDirty()
        forgotPasswordView.flex.markDirty()
        socialLoginView.flex.markDirty()
        actionButtonView.flex.markDirty()
        
        view.setNeedsLayout()
    }
}
