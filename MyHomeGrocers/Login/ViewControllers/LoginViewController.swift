//
//  LoginViewController.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/26/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
//import FBSDKCoreKit
//import FBSDKLoginKit
//import GoogleSignIn

class LoginViewController: UIViewController {
    @IBOutlet weak var signupUnderline: UILabel!
    
    @IBOutlet weak var socialLoginContainer: UIView!
    
    @IBOutlet weak var greetViewContainer: UIView!
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginUnderline: UILabel!
    
    let textFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 52, height: 50))
    var username = ""
    var authorizationKey = ""
    var isFacebookLogin = false
    
    var facebookEmail = ""
    var facebookName = ""
    var facebookSocialId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isFacebookLogin = false
        
        // temp
        //textFieldView.textField.text = "bannu.sudheer521@gmail.com"
    }
    
    func setup() {
        textFieldView.textField.delegate = self
        
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        greetingView.configure(isLogin: true)
        greetViewContainer.addSubview(greetingView)
        
        textFieldView.nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        textFieldContainer.addSubview(textFieldView)
        textFieldView.textField.autocapitalizationType = .none
        textFieldView.textField.autocorrectionType = .no
        
        skipButton.setTitleColor(UIColor.customOrange(), for: .normal)
        skipButton.titleLabel?.font = UIFont(name: "Poppins-SemiBold", size: 15)
        skipButton.backgroundColor = .clear
        skipButton.setTitle(">>SKIP", for: .normal)
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        termsLabel.textColor = UIColor.termsColor()
        termsLabel.lineBreakMode = .byWordWrapping
        termsLabel.numberOfLines = 0
        termsLabel.font = UIFont(name: "Poppins-Regular", size: 12)
        termsLabel.textAlignment = .center
        
        let fullString = "By creating an account, you agree to the MyHomeGrocers Terms of Service and Privacy Policy "
        
        let termsString = "Terms of Service"
        let rangeOfTermsString = (fullString as NSString).range(of: termsString)
        
        let policyString = "Privacy Policy"
        let rangeOfPolicyString = (fullString as NSString).range(of: policyString)
        
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font:  UIFont(name: "Poppins-Bold", size: 12) as Any],
                                       range: rangeOfTermsString)
        
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font:  UIFont(name: "Poppins-Bold", size: 12) as Any],
                                       range: rangeOfPolicyString)
        
        termsLabel.attributedText = attributedString
        
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginUnderline.backgroundColor = UIColor.customOrange()
        signupUnderline.backgroundColor = .clear
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        loginUnderline.isHidden = false
        signupUnderline.isHidden = true
        loginButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
        signupButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(signupVC, animated: false)
    }
    
    @IBAction func skipButtonClicked(_ sender: Any) {
        // * Write API Then Navigate to DashboardVC
        
        /*let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let dashboardVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(dashboardVC, animated: true)*/
    }
    
    @objc func nextButtonTapped() {
        if isFacebookLogin {
            //loginThroughFacebook()
        } else {
            
            textFieldDidEndEditing(textFieldView.textField)
            let charset = NSCharacterSet.letters
            
            // * Email
            if username.rangeOfCharacter(from: charset) != nil {
                if GlobalMethods.isValidEmail(email: username) {
                    let vc = GlobalMethods.getViewController(storyBoardID: "PasswordVC") as! PasswordVC
                    vc.emailId = username
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    Alert.showAlert(message: "Please enter valid Email id")
                }
            } else {
                // * Mobile
                getOtp()
            }
        }
    }
    
    /*func loginThroughFacebook() {
        let parameters = [
            "type": 4,
            "email": facebookEmail,
            "name": facebookName,
            "social_id": facebookSocialId] as [String: Any]
        
        print(parameters)
        
        WebService.parseData(urlStr: API.login(), parameters: parameters, authorizationKey: nil, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            // * Need to check
            let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                print("RESULT DICT : ", resultDict)
                if let statusDict = dict["status"] as? NSDictionary {
                    if statusDict["code"] as! Int == 1 {
                        let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
            
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }
        }) { (errMsg) in
            Alert.showAlert(message: errMsg)
        }
    }*/
    
    func getOtp() {
        textFieldDidEndEditing(textFieldView.textField)
        let parameters = [
            "type": 4,
            "telephone_dial_code": "91",
            "telephone_iso2_code": "in",
            "telephone": username] as [String: Any]
        self.showHUD()
        WebService.parseData(urlStr: API.login(), parameters: parameters, authorizationKey: nil, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = LoginMobileResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let authorizationKey = dictData!.data.token_type + " " + dictData!.data.access_token
                    let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                    vc.authorizationKey = authorizationKey
                    vc.mobileNumber = self.username
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            // * Need to check
            /*
            let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                //if let statusDict = dict["status"] as? NSDictionary {
                if dict["statusCode"] as! Int == 1 {
                    let tokenType = resultDict["token_type"] as! String
                    let accessToken = resultDict["access_token"] as! String
                    let authorizationKey = tokenType + " " + accessToken
                    print(authorizationKey)
                    let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                    vc.authorizationKey = authorizationKey
                    vc.mobileNumber = self.username
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                //}
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

//extension LoginViewController: GIDSignInDelegate {
//    @objc func facebookImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
//            let fbLoginManager : LoginManager = LoginManager()
//            fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
//            if (error == nil){
//                let fbloginresult : LoginManagerLoginResult = result!
//              // if user cancel the login
//              if (result?.isCancelled)!{
//                      return
//              }
//              if(fbloginresult.grantedPermissions.contains("email"))
//              {
//                self.getFBUserData()
//              }
//            }
//          }
//        }
//    //    @objc func loginFacebookAction(sender: AnyObject) {//action of the custom button in the storyboard
//    //        let fbLoginManager : LoginManager = LoginManager()
//    //        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
//    //        if (error == nil){
//    //            let fbloginresult : LoginManagerLoginResult = result!
//    //          // if user cancel the login
//    //          if (result?.isCancelled)!{
//    //                  return
//    //          }
//    //          if(fbloginresult.grantedPermissions.contains("email"))
//    //          {
//    //            self.getFBUserData()
//    //          }
//    //        }
//    //      }
//    //    }
//
//        func getFBUserData(){
//            if((AccessToken.current) != nil){
//                GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
//              if (error == nil){
//                //everything works print the user data
//    //            print(result)
//                let info = result as! [String : AnyObject]
//                print("FBData:\(info)")
//                self.facebookEmail = info["email"] as! String
//                self.facebookSocialId = info["id"] as! String
//                self.facebookName = info["name"] as! String
//
//                print(self.facebookEmail)
//                print(self.facebookSocialId)
//                print(self.facebookName)
//                self.isFacebookLogin = true
//                self.textFieldView.textField.text = self.facebookEmail
////                self.actionButtonView.actionButton.setTitle("Next", for: .normal)
//              }
//            })
//          }
//        }
//
//        @objc func googleImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
//                GIDSignIn.sharedInstance().delegate = self
//        //          GIDSignIn.sharedInstance().uiDelegate=self
//                  GIDSignIn.sharedInstance().signIn()
//            }
//
//    //    @objc func loginGoogleAction(sender: AnyObject) {
//    //        GIDSignIn.sharedInstance().delegate = self
//    ////          GIDSignIn.sharedInstance().uiDelegate=self
//    //          GIDSignIn.sharedInstance().signIn()
//    //    }
//
//        func signInWillDispatch(_ signIn: GIDSignIn!, error: Error!) {
//        }
//
//        func signIn(_ signIn: GIDSignIn!,
//            presentViewController viewController: UIViewController!) {
//          self.present(viewController, animated: true, completion: nil)
//        }
//
//        func signIn(_ signIn: GIDSignIn!,
//            dismissViewController viewController: UIViewController!) {
//          self.dismiss(animated: true, completion: nil)
//        }
//
//        public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
//          withError error: Error!) {
//            if (error == nil) {
//              // Perform any operations on signed in user here.
//    //          let userId = user.userID                  // For client-side use only!
//    //          let idToken = user.authentication.idToken // Safe to send to the server
//    //          let fullName = user.profile.name
//    //          let givenName = user.profile.givenName
//    //          let familyName = user.profile.familyName
//                print("GoogleData:\(user)")
//              let email = user.profile.email
//    //            print(email)
//                textFieldView.textField.text = email
////                self.actionButtonView.actionButton.setTitle("Next", for: .normal)
//              // ...
//            } else {
//    //          print("\(error.localized)")
//            }
//        }
//}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        username = textField.text ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
