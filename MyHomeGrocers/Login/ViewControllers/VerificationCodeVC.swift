//
//  VerificationCodeVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/18/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class VerificationCodeVC: UIViewController {
    
    @IBOutlet weak var greetViewContainer: UIView!
    @IBOutlet weak var signupUnderline: UILabel!
    @IBOutlet weak var loginUnderline: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var otpContainer: UIView!
    @IBOutlet weak var changeNumberContainer: UIView!
    
    let otpView = OTPView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 150))
    
    var authorizationKey = ""
    var mobileNumber = ""
    var otp = ""
    var isFromSignup = false
    var isFromForgotPassword = false
    var isFromUpdateProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup() {
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        if isFromSignup {
            greetingView.configure(isLogin: true)
        } else if isFromForgotPassword {
            //greetingView.titleLabel.text = "Forgot your Password ?"
            //greetingView.descriptionLabel.text = "Enter registered Mobile Number. We'll send a OTP for you to enter before Signing in."
            greetingView.configurePasswordView()
        } else {
            greetingView.configure(isLogin: true)
        }
        greetViewContainer.addSubview(greetingView)
        
        let changeView = ChangeEmailOrNumberView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 40))
        changeView.backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        changeNumberContainer.addSubview(changeView)
        
        otpView.descriptionLabel.text = "Please enter the verificationcode sent to " + mobileNumber
        
        otpView.tf1.delegate = self
        otpView.tf2.delegate = self
        otpView.tf3.delegate = self
        otpView.tf4.delegate = self
        otpView.tf5.delegate = self
        otpView.tf6.delegate = self
        
        otpView.tf1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpView.tf2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpView.tf3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpView.tf4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpView.tf5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpView.tf6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        otpContainer.addSubview(otpView)
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginUnderline.backgroundColor = UIColor.customOrange()
        signupUnderline.backgroundColor = .clear
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        loginUnderline.isHidden = false
        signupUnderline.isHidden = true
        loginButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
        signupButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        resendButton.setTitleColor(UIColor.customOrange(), for: .normal)
        resendButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(signupVC, animated: false)
    }
    
    @objc func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func signup() {
        let parameters = ["telephone_otp": otpView.OTPString()] as [String: Any]
        self.showHUD()
        WebService.parseData(urlStr: API.verifySignupOtp(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                // * New model + JSON Datat initilized
                let dictData = SignupVerifyOTPResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let vc = GlobalMethods.getViewController(storyBoardID: "LoginViewController") as! LoginViewController
                    //vc.authorizationKey = self.authorizationKey
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            // * Need to check
            /*let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                print("RESULT DICT : ", resultDict)
                if dict["statusCode"] as! Int == 1 {
                    let vc = GlobalMethods.getViewController(storyBoardID: "LoginViewController") as! LoginViewController
                    //vc.authorizationKey = self.authorizationKey
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["email_otp"] != nil {
                    Alert.showAlert(message: errorDict["email_otp"] as! String)
                } else if errorDict["telephone_otp"] != nil {
                    Alert.showAlert(message: errorDict["telephone_otp"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                } else if statusDict["code"] as! Int == 1 {
                    let vc = GlobalMethods.getViewController(storyBoardID: "LoginViewController") as! LoginViewController
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    func login() {
        // textFieldDidEndEditing(textFieldView.textField)
        let parameters = ["type": 2,
                          "otp": otpView.OTPString()] as [String: Any]
        print(parameters)
        self.showHUD()
        WebService.parseData(urlStr: API.verifyLoginOtp(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            // * New model + JSON Datat initilized
            // * This is for 'SignupEmailVerifyOTPResponse'
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = LoginverifyOTPResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                    let authorizationKey = dictData!.data.token_type + " " + dictData!.data.access_token
                    vc.authorizationKey = authorizationKey
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            // * Need to check
            
            /*let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                //if let statusDict = dict["status"] as? NSDictionary {
                    if dict["statusCode"] as! Int == 1 {
                        let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                        let tokenType = resultDict["token_type"] as! String
                        let accessToken = resultDict["access_token"] as! String
                        let authorizationKey = tokenType + " " + accessToken
                        vc.authorizationKey = authorizationKey
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                //}
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    func resetPassword(otp: String) {
        let vc = GlobalMethods.getViewController(storyBoardID: "ResetPasswordVC") as! ResetPasswordVC
        vc.otp = otp
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateProfile(otp: String) {
        let parameters = ["otp": "123456"] as [String: Any]
        print(parameters)
        self.showHUD()
        WebService.parseData(urlStr: API.verifyUpdateProfileOtp(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            
            if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                } else if statusDict["code"] as! Int == 1 {
                    let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) in
                        let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                        vc.authorizationKey = self.authorizationKey
                        //vc.isFromUpdateProfile = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }))
                    
                    Alert.showAlertforOkAction(message: statusDict["message"] as! String, okAction: okAction)
                }
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension VerificationCodeVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case otpView.tf1:
                otpView.tf2.becomeFirstResponder()
            case otpView.tf2:
                otpView.tf3.becomeFirstResponder()
            case otpView.tf3:
                otpView.tf4.becomeFirstResponder()
            case otpView.tf4:
                otpView.tf5.becomeFirstResponder()
            case otpView.tf5:
                otpView.tf6.becomeFirstResponder()
            case otpView.tf6:
                otpView.tf6.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField {
            case otpView.tf1:
                otpView.tf1.becomeFirstResponder()
            case otpView.tf2:
                otpView.tf1.becomeFirstResponder()
            case otpView.tf3:
                otpView.tf2.becomeFirstResponder()
            case otpView.tf4:
                otpView.tf3.becomeFirstResponder()
            case otpView.tf5:
                otpView.tf4.becomeFirstResponder()
            case otpView.tf6:
                otpView.tf5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let firstTwoDigits = (otpView.tf1.text ?? "") + (otpView.tf2.text ?? "")
        let middleTwoDigits = (otpView.tf3.text ?? "") + (otpView.tf4.text ?? "")
        let lastTwoDigits = (otpView.tf5.text ?? "") + (otpView.tf6.text ?? "")
        let currentString: NSString =  firstTwoDigits + middleTwoDigits + lastTwoDigits as NSString
        print("otp:\(currentString)")
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let firstTwoDigits = (otpView.tf1.text ?? "") + (otpView.tf2.text ?? "")
        let middleTwoDigits = (otpView.tf3.text ?? "") + (otpView.tf4.text ?? "")
        let lastTwoDigits = (otpView.tf5.text ?? "") + (otpView.tf6.text ?? "")
        let currentString: NSString =  firstTwoDigits + middleTwoDigits + lastTwoDigits as NSString
        print("otp:\(currentString)")
        if currentString.length == 6 {
            otp = currentString as String
            if isFromSignup {
                signup()
            } else if isFromForgotPassword {
                resetPassword(otp: otp)
            } else if isFromUpdateProfile {
                updateProfile(otp: otp)
            } else {
                login()
            }
        }
    }
}
