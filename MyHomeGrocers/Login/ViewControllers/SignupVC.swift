//
//  SignupVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/26/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var greetView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var signupView: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginView: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    
    @IBAction func shareButton(_ sender: Any) {
    }
    
    var firstName = ""
    var lastName = ""
    var mobileNumber = ""
    var emailId = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        greetingView.configure(isLogin: false)
        greetView.addSubview(greetingView)
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        termsLabel.textColor = UIColor.customGray()
        termsLabel.lineBreakMode = .byWordWrapping
        termsLabel.numberOfLines = 0
        termsLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        termsLabel.textAlignment = .left
        
        actionButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 25)
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.backgroundColor = UIColor.customOrange()
        actionButton.layer.cornerRadius = 24
        actionButton.layer.masksToBounds = true
        
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginView.backgroundColor = .clear
        signupView.backgroundColor = UIColor.customOrange()
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        
        loginView.isHidden = true
        signupView.isHidden = false
        signupButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
        loginButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
    }
    
    @IBAction func actionButtonTapped(_ sender: Any) {
        view.endEditing(true)
        if (firstName == "" || lastName == "" || emailId == "" || mobileNumber == "" || password == "") {
            Alert.showAlert(message: "Please enter all the required fields")
        } else {
            signup()
        }
    }
    
    @objc func loginButtonTapped() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func signup() {
        let parameters = [
            "firstname": firstName,
            "lastname": lastName,
            "telephone_dial_code": "91",
            "telephone_iso2_code": "in",
            "telephone": mobileNumber,
            "email": emailId,
            "password": password,
            "confirm": password
        ]
        self.showHUD()
        WebService.parseData(urlStr: API.register(), parameters: parameters, authorizationKey: nil, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")

            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                // * New model + JSON Datat initilized
                let dictData = RegisterUserResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let authorizationKey = dictData!.data.token_type + " " + dictData!.data.access_token

                    let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) in
                        let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                        vc.authorizationKey = authorizationKey
                        vc.mobileNumber = self.mobileNumber
                        vc.isFromSignup = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }))
                    
                    Alert.showAlertforOkAction(message: dict["message"] as! String, okAction: okAction)
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            // * Need to check
            /*let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                //  if let statusDict = dict["status"] as? NSDictionary {
                if dict["statusCode"] as! Int == 1 {
                    //  Alert.showAlert(message: statusDict["message"] as! String)
                    let tokenType = resultDict["token_type"] as! String
                    let accessToken = resultDict["access_token"] as! String
                    let authorizationKey = tokenType + " " + accessToken
                    
                    let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) in
                        let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                        vc.authorizationKey = authorizationKey
                        vc.mobileNumber = self.mobileNumber
                        vc.isFromSignup = true
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }))
                    
                    Alert.showAlertforOkAction(message: dict["message"] as! String, okAction: okAction)
                }
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["telephone"] != nil && errorDict["email"] != nil {
                    Alert.showAlert(message: "User already exists")
                } else if errorDict["telephone"] != nil {
                    Alert.showAlert(message: errorDict["telephone"] as! String)
                } else if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                } else if errorDict["email"] != nil {
                    Alert.showAlert(message: errorDict["email"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    func validateUsername(parameters: [String: String]) {
        self.showHUD()
        WebService.parseData(urlStr: API.validateUserId(), parameters: parameters, authorizationKey: nil, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                print("RESULT DICT : ", resultDict)
                if let statusDict = dict["status"] as? NSDictionary {
                    if statusDict["code"] as! Int == 1 {
                    }
                }
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["email"] != nil {
                    Alert.showAlert(message: errorDict["email"] as! String)
                } else if errorDict["telephone"] != nil {
                    Alert.showAlert(message: errorDict["telephone"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension SignupVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextFieldCell", for: indexPath) as! DoubleTextFieldCell
            cell.firstNameTextFieldView.nextButton.isHidden = true
            cell.firstNameTextFieldView.textField.placeholder = "First Name*"
            cell.lastNameTextFieldView.textField.placeholder = "Last Name*"
            cell.lastNameTextFieldView.nextButton.isHidden = true
            cell.firstNameTextFieldView.textField.tag = 100
            cell.lastNameTextFieldView.textField.tag = 200
            cell.firstNameTextFieldView.textField.delegate = self
            cell.lastNameTextFieldView.textField.delegate = self
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textFieldView.nextButton.isHidden = true
            switch indexPath.row {
            case 1:
                cell.textFieldView.textField.placeholder = "Mobile Number*"
            case 2:
                cell.textFieldView.textField.placeholder = "Email ID"
            case 3:
                cell.textFieldView.textField.placeholder = "Password"
            default:
                cell.textFieldView.textField.placeholder = ""
            }
            cell.textFieldView.textField.tag = indexPath.row
            cell.textFieldView.textField.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension SignupVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 3 {
            textField.isSecureTextEntry = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 100:
            firstName = textField.text ?? ""
        case 200:
            lastName = textField.text ?? ""
        case 1:
            mobileNumber = textField.text ?? ""
            if GlobalMethods.isValidPhoneNumber(phoneNumber: mobileNumber) {
                /*let parameters = [
                    "type": "telephone",
                    "telephone": mobileNumber]
                
                validateUsername(parameters: parameters)*/
            } else {
                Alert.showAlert(message: "Please enter valid Phone number")
            }
        case 2:
            emailId = textField.text ?? ""
            if GlobalMethods.isValidEmail(email: emailId) {
                /*let parameters = [
                    "type": "email",
                    "email": emailId]
                
                validateUsername(parameters: parameters)*/
            } else {
                Alert.showAlert(message: "Please enter valid Email id")
            }
        case 3:
            password = textField.text ?? ""
        default:
            return
        }
    }
}
