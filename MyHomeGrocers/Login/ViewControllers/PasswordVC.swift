//
//  PasswordVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/18/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class PasswordVC: UIViewController {
    
    @IBOutlet weak var loginUnderline: UILabel!
    @IBOutlet weak var signupUnderline: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var changeLoginContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var greetingViewContainer: UIView!
    
    var emailId = ""
    var password = ""
    var authorizationKey = ""
    let textFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 52, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup() {
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        greetingView.configure(isLogin: true)
        greetingViewContainer.addSubview(greetingView)
        
        let changeView = ChangeEmailOrNumberView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 40))
        changeView.changeButton.setTitle("CHANGE EMAIL", for: .normal)
        changeView.backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchUpInside)
        changeLoginContainer.addSubview(changeView)
        
        textFieldView.nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        textFieldContainer.addSubview(textFieldView)
        textFieldView.textField.placeholder = "Password"
        textFieldView.textField.delegate = self
        textFieldView.textField.autocapitalizationType = .none
        textFieldView.textField.autocorrectionType = .no
        textFieldView.textField.isSecureTextEntry = true
        textFieldView.nextButtonImageView.image = UIImage(named: "okey-green")
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginUnderline.backgroundColor = UIColor.customOrange()
        signupUnderline.backgroundColor = .clear
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        loginUnderline.isHidden = false
        signupUnderline.isHidden = true
        loginButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
        signupButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        forgotPasswordButton.setTitleColor(UIColor.customOrange(), for: .normal)
        forgotPasswordButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        // temp
        //textFieldView.textField.text = "css123"
    }
    
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        let vc = GlobalMethods.getViewController(storyBoardID: "ForgotPasswordVC") as! ForgotPasswordVC
        vc.authorizationKey = authorizationKey
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func backButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextButtonTapped() {
        textFieldDidEndEditing(textFieldView.textField)
        let parameters = [
            "type": 1,
            "email": emailId,
            "password": password] as [String: Any]
        print(parameters)
        self.showHUD()
        WebService.parseData(urlStr: API.login(), parameters: parameters, authorizationKey: nil, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = LoginverifyOTPResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                    let authorizationKey = dictData!.data.token_type + " " + dictData!.data.access_token
                    vc.authorizationKey = authorizationKey
                    GlobalMethods.saveAuthKey(authKey: authorizationKey)
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    // * Failure
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            /*let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                //if let statusDict = dict["status"] as? NSDictionary {
                if dict["statusCode"] as! Int == 1 {
                    let vc = GlobalMethods.getViewController(storyBoardID: "DashboardVC") as! DashboardVC
                    let tokenType = resultDict["token_type"] as! String
                    let accessToken = resultDict["access_token"] as! String
                    let authKey = tokenType + " " + accessToken
                    print(authKey)
                    vc.authorizationKey = authKey
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                //}
            }
            
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(signupVC, animated: false)
    }
}

extension PasswordVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        password = textField.text ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
