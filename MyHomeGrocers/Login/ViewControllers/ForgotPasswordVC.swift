//
//  ForgotPasswordVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/12/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var greetViewContainer: UIView!
    @IBOutlet weak var textFieldContainer: UIView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    let textFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 52, height: 50))
    var username = ""
    var authorizationKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setup()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewWillAppear(_ animated: Bool) {
        textFieldView.textField.text = ""
    }
    
    func setup() {
        textFieldView.textField.delegate = self
        
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        greetingView.titleLabel.text = "Forgot your Password ?"
        greetingView.descriptionLabel.text = "Enter registered Mobile Number. We'll send a OTP for you to enter before Signing in."
        greetViewContainer.addSubview(greetingView)
        
        textFieldView.nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        textFieldContainer.addSubview(textFieldView)
        textFieldView.textField.autocapitalizationType = .none
        textFieldView.textField.autocorrectionType = .no
        textFieldView.textField.placeholder = "Mobile Number"
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        termsLabel.textColor = UIColor.customGray()
        termsLabel.lineBreakMode = .byWordWrapping
        termsLabel.numberOfLines = 0
        termsLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        termsLabel.textAlignment = .left
        
        let fullString = "Remember your password? LOGIN"
        
        let loginString = "LOGIN"
        let rangeOfTermsString = (fullString as NSString).range(of: loginString)
        
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: UIColor.customOrange(), NSAttributedString.Key.font:  UIFont(name: "Poppins-Bold", size: 15) as Any],
                                       range: rangeOfTermsString)
        
        
        termsLabel.attributedText = attributedString
    }
    
    @objc func nextButtonTapped() {
        getOtp()
    }
    
    func getOtp() {
        textFieldDidEndEditing(textFieldView.textField)
        let parameters = [
            "type": "telephone",
            "telephone_dial_code": "91",
            "telephone_iso2_code": "in",
            "telephone": username as Any] as [String: Any]
        print(parameters)
        self.showHUD()
        WebService.parseData(urlStr: API.forgotPasswordMobileOtp(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("result is:\(result as? NSDictionary)")
            
            let dict = result as! NSDictionary
            if dict["statusCode"] as! Int == 0 {
                Alert.showAlert(message: dict["message"] as! String)
            }
            else {
                let dictData = LoginMobileResponse(json: result as! [String : Any])
                if dictData != nil {
                    // * Success
                    let authorizationKey = dictData!.data.token_type + " " + dictData!.data.access_token
                    let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                    vc.authorizationKey = authorizationKey
                    vc.mobileNumber = self.username
                    vc.isFromForgotPassword = true
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
            }
            
            /*let dict = result as! NSDictionary
            
            if let resultDict = dict["data"] as? NSDictionary {
                //   if let statusDict = dict["status"] as? NSDictionary {
                if dict["statusCode"] as! Int == 1 {
                    let tokenType = resultDict["token_type"] as! String
                    let accessToken = resultDict["access_token"] as! String
                    let authorizationKey = tokenType + " " + accessToken
                    let vc = GlobalMethods.getViewController(storyBoardID: "VerificationCodeVC") as! VerificationCodeVC
                    vc.authorizationKey = authorizationKey
                    vc.mobileNumber = self.username
                    vc.isFromForgotPassword = true
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
            else if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["telephone"] != nil {
                    Alert.showAlert(message: errorDict["telephone"] as! String)
                }
            }  else if let statusDict = dict["status"] as? NSDictionary {
                if statusDict["code"] as! Int == 0 {
                    Alert.showAlert(message: statusDict["message"] as! String)
                }
            }
            else {
                Alert.showAlert(message: "Unknown error, Please try again!")
            }*/
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPasswordVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        username = textField.text ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
