//
//  ResetPasswordVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/14/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginUnderline: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var signupUnderline: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var greetView: UIView!
    
    let newPasswordTextFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 52, height: 50))
    let confirmPasswordTextFieldView = TextFieldView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 52, height: 50))
    
    var otp = ""
    var authorizationKey = ""
    var newPassword = ""
    var confirmPassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setup() {
        let greetingView = GreetingView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 36, height: 90))
        greetingView.backgroundColor = .clear
        greetingView.titleLabel.text = "Change Password?"
        greetingView.descriptionLabel.text = ""
        greetingView.descriptionLabel.isHidden = true
        greetView.addSubview(greetingView)
        
        containerView.layer.cornerRadius = 36
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        containerView.layer.shadowColor = UIColor.customOrange().cgColor
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowOffset = CGSize(width: 0, height: 25)
        containerView.layer.shadowRadius = 20
        
        //textFieldView.nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        newPasswordView.addSubview(newPasswordTextFieldView)
        newPasswordTextFieldView.textField.delegate = self
        newPasswordTextFieldView.nextButton.isHidden = true
        newPasswordTextFieldView.textField.placeholder = "New Password"
        confirmPasswordView.addSubview(confirmPasswordTextFieldView)
        confirmPasswordTextFieldView.textField.delegate = self
        confirmPasswordTextFieldView.nextButton.isHidden = true
        confirmPasswordTextFieldView.textField.placeholder = "Confirm Password"
        
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginUnderline.backgroundColor = UIColor.customOrange()
        signupUnderline.backgroundColor = .clear
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
        loginUnderline.isHidden = false
        signupUnderline.isHidden = true
        loginButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
        signupButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        
        confirmButton.layer.cornerRadius = 25
        confirmButton.layer.masksToBounds = true
        confirmButton.backgroundColor = UIColor.customOrange()
        confirmButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 25)
    }

    @IBAction func actionButtonTapped(_ sender: Any) {
        view.endEditing(true)
        if newPasswordTextFieldView.textField.text != confirmPasswordTextFieldView.textField.text {
            Alert.showAlert(message: "Passwords not matching")
        } else if newPasswordTextFieldView.textField.text == "" || confirmPasswordTextFieldView.textField.text == "" {
            Alert.showAlert(message: "Password can't be empty")
        } else {
            newPassword = newPasswordTextFieldView.textField.text!
            confirmPassword = confirmPasswordTextFieldView.textField.text!
            resetPassword()
        }
    }
    
    func resetPassword() {
        let parameters = [
            "otp"  : otp,
            "password": newPassword,
            "confirm"  : confirmPassword] as [String: Any]
        self.showHUD()
        WebService.parseData(urlStr: API.verifyOtpAndSetPassword(), parameters: parameters, authorizationKey: authorizationKey, method: .POST, showHud: true, successHandler: { (result) in
            print("ResetPasswordResult:\(result)")
            let dict = result as! NSDictionary
            if let errorDict = dict["error"] as? NSDictionary {
                if errorDict["password"] != nil {
                    Alert.showAlert(message: errorDict["password"] as! String)
                } else if errorDict["error_description"] != nil {
                    Alert.showAlert(message: errorDict["error_description"] as! String)
                }
            }
            //else if let statusDict = dict["status"] as? NSDictionary {
                else if dict["statusCode"] as! Int == 1 {
                    let okAction = (UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) in
                        let vc = GlobalMethods.getViewController(storyBoardID: "LoginViewController") as! LoginViewController
                        print(self.authorizationKey)
                        vc.authorizationKey = self.authorizationKey
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }))
                    
                    Alert.showAlertforOkAction(message: dict["message"] as! String, okAction: okAction)
                } else if dict["statusCode"] as! Int == 0 {
                    Alert.showAlert(message: dict["message"] as! String)
                } else {
                    Alert.showAlert(message: "Unknown error, Please try again!")
                }
          //  }
            self.dismissHUD(isAnimated: true)
        }) { (errMsg) in
            self.dismissHUD(isAnimated: true)
            Alert.showAlert(message: errMsg)
        }
    }
}

extension ResetPasswordVC: UITextFieldDelegate {
    
}
