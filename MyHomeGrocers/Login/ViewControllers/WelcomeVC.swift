//
//  WelcomeVC.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/29/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    
    let backgroundImages = ["Screen-1","Screen-2","Screen-3","Screen-4"]
    let subImages = ["Screen-1-sub","Screen-2-sub","Screen-3-sub","Screen-4-sub"]
    var titles = ["EXPLORE","PICK","CONFIRM","ENJOY"]
    var descriptions = ["from a wide range of Indian Groceries & Delivery Availability","from 100's of Brands & a wide range of Categories", "Place your Order & Add to your Wishlist for easier checkout", "Get it Delivered at your Doorstep"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        skipButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 22)
        skipButton.setTitleColor(UIColor.customOrange(), for: .normal)
        
//        myCollectionViewFlowLayout.itemSize = CGSize(width: 320, height: 548)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return backgroundImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WelcomeImageCollectionCell", for: indexPath) as! WelcomeImageCollectionCell
        cell.backgroundImageView.image = UIImage(named: backgroundImages[indexPath.row])
        cell.subImageView.image = UIImage(named: subImages[indexPath.row])
        cell.titleLabel.text = titles[indexPath.row]
        cell.descriptionLabel.text = descriptions[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewFlowLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: deviceFrame.size.width, height: deviceFrame.size.height)
    }
        
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentIndex:CGFloat = self.collectionView.contentOffset.x / self.collectionView.frame.size.width
        pageControl.currentPage = Int(currentIndex)
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        let vc = GlobalMethods.getViewController(storyBoardID: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension WelcomeVC {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width - 10, height: frameSize.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
}
