//
//  TextFieldView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 7/21/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class TextFieldView: UIView {
    let root = UIView()
    let textField = UITextField()
    let nextButton = UIButton()
    let nextButtonImageView = UIImageView()
    let separatorLine = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.placeholder = "Email ID / Mobile Number"
        separatorLine.backgroundColor = UIColor.customOrange()
        nextButtonImageView.image = UIImage(named: "next_arrow")
    }
    
    func layout() {
        root.flex.define { (flex) in
            flex.addItem().direction(.row).alignItems(.center).define { (flex) in
                flex.addItem(textField).grow(1)
                flex.addItem(nextButton).width(20).height(20).define { (flex) in
                    flex.addItem(nextButtonImageView).width(20).height(20).marginRight(8)
                }
            }
            flex.addItem(separatorLine).marginTop(4).height(1)
        }
        self.addSubview(root)
    }
    
    func configure(placeHolder: String) {
        textField.placeholder = placeHolder
        setNeedsLayout()
    }
}
