//
//  BackAndLogoView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/4/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class BackAndLogoView: UIView {
    let root = UIView()
    let backButton = UIButton()
    let logoView = UIImageView()
    let logoContainer = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        logoView.image = UIImage(named: "Logo")
        backButton.setImage(UIImage(named: "Back arrow"), for: .normal)
    }
    
    func layout() {
        root.flex.direction(.row).define { (flex) in
            flex.addItem(backButton).marginLeft(24).width(50).height(50)
            
            flex.addItem(logoContainer).alignItems(.end).marginRight(24).define { (flex) in
                flex.addItem(logoView).width(120).height(60)
            }.grow(1)
            
            }
        self.addSubview(root)
    }
    
}
