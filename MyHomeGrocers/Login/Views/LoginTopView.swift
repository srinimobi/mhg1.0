//
//  LoginTopView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 7/19/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class LoginTopView: UIView {

    let root = UIView()
    let logoView = UIImageView()
    let loginButton = UIButton()
    let loginUnderline = UIView()
    let signupButton = UIButton()
    let signupUnderline = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        loginButton.setTitle("Login", for: .normal)
        signupButton.setTitle("Signup", for: .normal)
        loginUnderline.backgroundColor = UIColor.customOrange()
        signupUnderline.backgroundColor = UIColor.customOrange()
        loginButton.setTitleColor(UIColor.customOrange(), for: .normal)
        signupButton.setTitleColor(UIColor.customOrange(), for: .normal)
        logoView.image = UIImage(named: "Logo")
        configure(isLogin: true)
    }
    
    
    func layout() {
        root.flex.direction(.row).alignItems(.center).define { (flex) in
            flex.addItem(logoView).width(120).height(60).marginLeft(24)
            flex.addItem().direction(.row).alignItems(.center).justifyContent(.end).marginRight(16).define { (flex) in
                    flex.addItem().marginRight(16).direction(.column).define { (flex) in
                        flex.addItem(loginButton)
                        flex.addItem(loginUnderline).width(80).height(2)
                    }
                
                    flex.addItem().direction(.column).define { (flex) in
                        flex.addItem(signupButton)
                        flex.addItem(signupUnderline).width(80).height(2)
                    }
                }.grow(1)
            }
        self.addSubview(root)
    }
    
    func configure(isLogin: Bool) {
        if isLogin {
            loginUnderline.isHidden = false
            signupUnderline.isHidden = true
            loginButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
            signupButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        } else {
            loginUnderline.isHidden = true
            signupUnderline.isHidden = false
            signupButton.titleLabel?.font = UIFont(name: "Poppins-Bold", size: 20)
            loginButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        }
        setNeedsLayout()
    }
}
