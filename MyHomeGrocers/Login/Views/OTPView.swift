//
//  OTPView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/18/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class OTPView: UIView {
    let root = UIView()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    let stackView = UIStackView()
    let tf1 = UITextField(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let tf2 = UITextField()
    let tf3 = UITextField()
    let tf4 = UITextField()
    let tf5 = UITextField()
    let tf6 = UITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        titleLabel.text = "Verification Code"
        titleLabel.textColor = .black
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 23)
        
        descriptionLabel.text = "Please enter the verification code sent to +1 12345 6789"
        descriptionLabel.textColor = .black
        descriptionLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        descriptionLabel.numberOfLines = 2
        //        stackView.axis  = NSLayoutConstraint.Axis.vertical
        //        stackView.distribution  = UIStackView.Distribution.equalSpacing
        //        stackView.alignment = UIStackView.Alignment.center
        //        stackView.spacing = 16.0
        //
        tf1.layer.borderColor = UIColor.customOrange().cgColor
        tf1.layer.borderWidth = 2
        tf1.layer.cornerRadius = 24
        tf1.layer.masksToBounds = true
        tf1.textAlignment = .center
        
        tf2.layer.borderColor = UIColor.customOrange().cgColor
        tf2.layer.borderWidth = 2
        tf2.layer.cornerRadius = 24
        tf2.layer.masksToBounds = true
        tf2.textAlignment = .center
        
        tf3.layer.borderColor = UIColor.customOrange().cgColor
        tf3.layer.borderWidth = 2
        tf3.layer.cornerRadius = 24
        tf3.layer.masksToBounds = true
        tf3.textAlignment = .center
        
        tf4.layer.borderColor = UIColor.customOrange().cgColor
        tf4.layer.borderWidth = 2
        tf4.layer.cornerRadius = 24
        tf4.layer.masksToBounds = true
        tf4.textAlignment = .center
        
        tf5.layer.borderColor = UIColor.customOrange().cgColor
        tf5.layer.borderWidth = 2
        tf5.layer.cornerRadius = 24
        tf5.layer.masksToBounds = true
        tf5.textAlignment = .center
        
        tf6.layer.borderColor = UIColor.customOrange().cgColor
        tf6.layer.borderWidth = 2
        tf6.layer.cornerRadius = 24
        tf6.layer.masksToBounds = true
        tf6.textAlignment = .center
        //
        //        stackView.addArrangedSubview(tf1)
        //        stackView.addArrangedSubview(tf2)
        //        stackView.addArrangedSubview(tf3)
        //        stackView.addArrangedSubview(tf4)
        //        stackView.addArrangedSubview(tf5)
        //        stackView.addArrangedSubview(tf6)
        //        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func layout() {
        root.flex.direction(.column).define { (flex) in
            flex.addItem(titleLabel).marginHorizontal(16)
            flex.addItem(descriptionLabel).marginHorizontal(16).marginTop(12).height(44)
            flex.addItem().direction(.row).alignItems(.center).justifyContent(.center).marginHorizontal(16).marginTop(30).define { (flex) in
                flex.addItem(tf1).width(48).height(48).marginHorizontal(5)
                flex.addItem(tf2).width(48).height(48).marginHorizontal(5)
                flex.addItem(tf3).width(48).height(48).marginHorizontal(5)
                flex.addItem(tf4).width(48).height(48).marginHorizontal(5)
                flex.addItem(tf5).width(48).height(48).marginHorizontal(5)
                flex.addItem(tf6).width(48).height(48).marginHorizontal(5)
            }
        }
        self.addSubview(root)
    }
    
    func OTPString() -> String {
        return       tf1.text! +
                "" + tf2.text! +
                "" + tf3.text! +
                "" + tf4.text! +
                "" + tf5.text! +
                "" + tf6.text!
    }
}
