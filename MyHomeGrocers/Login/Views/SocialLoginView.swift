//
//  SocialLoginView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout
//import FBSDKLoginKit
//import GoogleSignIn

class SocialLoginView: UIView {
    let root = UIView()
    let textLabel = UILabel()
    let googleButton = UIButton()
    let facebookButton = UIButton()
    let appleButton = UIButton()
    let googleContainer = UIView()
    let googleImageView = UIImageView()
    let facebookContainer = UIView()
    let facebookImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        textLabel.text = "Connect Using:"
        textLabel.font = UIFont(name: "Poppins-Medium", size: 15)

        googleContainer.layer.cornerRadius = 40
        googleContainer.layer.masksToBounds = true
        googleImageView.image = UIImage(named: "gmail")
        
        facebookContainer.layer.cornerRadius = 40
        facebookContainer.layer.masksToBounds = true
        facebookImageView.image = UIImage(named: "facebook")

    }
    
    func layout() {
        root.flex.direction(.row).define { (flex) in
            flex.addItem(textLabel)
            flex.addItem(googleImageView).width(60).height(60)
            flex.addItem(facebookImageView).width(60).height(60)
        }
        self.addSubview(root)
    }
}
