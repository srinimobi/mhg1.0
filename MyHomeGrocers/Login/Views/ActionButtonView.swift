//
//  ActionButtonView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 7/21/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class ActionButtonView: UIView {
    let root = UIView()
    let checkboxImage = UIImageView()
    let checkboxButton = UIButton()
    let actionButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        checkboxButton.setTitleColor(.darkGray, for: .normal)
        checkboxButton.titleLabel?.lineBreakMode = .byWordWrapping
        checkboxButton.titleLabel?.numberOfLines = 0
        checkboxButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        checkboxButton.titleLabel?.textAlignment = .left
        
        actionButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 25)
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.backgroundColor = UIColor.customOrange()
        actionButton.layer.cornerRadius = 24
        actionButton.layer.masksToBounds = true
        
        configure(isLogin: true)
    }
    
    func layout() {
        root.flex.direction(.column).define { (flex) in
            flex.addItem().marginLeft(16).direction(.row) .define { (flex) in
                flex.addItem(checkboxImage).width(16).height(16)
                flex.addItem(checkboxButton).grow(1)
            }
            flex.addItem(actionButton).marginTop(24).width(180).height(52).alignSelf(.center)
        }
        self.addSubview(root)
    }
    
    func configure(isLogin: Bool) {
        if isLogin {
//            checkboxButton.setTitle("Remember me", for: .normal)
            checkboxButton.isHidden = true
            checkboxButton.flex.isIncludedInLayout = false
            checkboxImage.isHidden = true
            checkboxImage.flex.isIncludedInLayout = false
            actionButton.setTitle("Next", for: .normal)
        } else {
            checkboxButton.isHidden = false
            checkboxButton.flex.isIncludedInLayout = true
            checkboxImage.isHidden = false
            checkboxImage.flex.isIncludedInLayout = true
            checkboxButton.setTitle("By selecting you'll be accepting Terms and Conditions", for: .normal)
            actionButton.setTitle("Signup", for: .normal)
        }
        
        setNeedsLayout()
    }
    
    func configurePasswordView() {
        checkboxButton.setTitle("By selecting you'll be accepting Terms and Conditions", for: .normal)
        actionButton.setTitle("Change Password", for: .normal)
    }
}
