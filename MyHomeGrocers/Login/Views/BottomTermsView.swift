//
//  BottomTermsView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/16/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class BottomTermsView: UIView {
    let root = UIView()
    let checkboxButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        checkboxButton.setTitleColor(.darkGray, for: .normal)
        checkboxButton.titleLabel?.lineBreakMode = .byWordWrapping
        checkboxButton.titleLabel?.numberOfLines = 0
        checkboxButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        checkboxButton.titleLabel?.textAlignment = .center
        checkboxButton.setTitle("By creating an account, you agree to the MyHomeGrocers Terms of Service and Privacy Policy", for: .normal)
    }
    
    func layout() {
        root.flex.direction(.column).define { (flex) in
                flex.addItem(checkboxButton).grow(1)
        }
        self.addSubview(root)
    }
}
