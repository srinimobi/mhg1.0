//
//  ChangeEmailOrNumberView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/17/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit

class ChangeEmailOrNumberView: UIView {

    let root = UIView()
    let backButton = UIButton()
    let backImageView = UIImageView()
    let changeButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        backImageView.image = UIImage(named: "Back arrow")
        
        changeButton.setTitle("CHANGE NUMBER", for: .normal)
        changeButton.backgroundColor = UIColor.customOrange()
        changeButton.setTitleColor(.white, for: .normal)
        changeButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 13)
        changeButton.layer.cornerRadius = 20
        changeButton.layer.masksToBounds = true
    }
    
    
    func layout() {
        root.flex.direction(.row).define { (flex) in
            flex.addItem(backButton).width(30).height(30).marginLeft(20).define { (flex) in
                flex.addItem(backImageView).width(30).height(30)
            }
            flex.addItem().alignItems(.end).define { (flex) in
                flex.addItem(changeButton).width(140).height(40).alignSelf(.end).marginRight(20)
            }.grow(1)
        }
        self.addSubview(root)
    }
}
