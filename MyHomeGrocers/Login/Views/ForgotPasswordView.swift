//
//  ForgotPasswordView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 8/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class ForgotPasswordView: UIView {
    let root = UIView()
    let actionButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        actionButton.setTitle("Forgot Password?", for: .normal)
        actionButton.setTitleColor(UIColor.customOrange(), for: .normal)
        actionButton.titleLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
    }
    
    func layout() {
        root.flex.alignItems(.end).define { (flex) in
            flex.addItem(actionButton)
        }
        self.addSubview(root)
    }
}
