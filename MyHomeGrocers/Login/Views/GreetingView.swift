//
//  GreetingView.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 7/20/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout

class GreetingView: UIView {

    let root = UIView()
    let titleLabel = UILabel()
    let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        root.pin.all(pin.safeArea)
        root.flex.layout()
    }
    
    func setup() {
        titleLabel.textColor = UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 1)
        descriptionLabel.textColor = UIColor.customGray()
        titleLabel.font = UIFont(name: "Poppins-Medium", size: 26)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont(name: "Poppins-Medium", size: 15)
        configure(isLogin: true)
    }
    
    func layout() {
        root.flex.marginLeft(0).define { (flex) in
            flex.addItem(titleLabel)
            flex.addItem(descriptionLabel).marginTop(8)
        }
        self.addSubview(root)
    }
    
    func configure(isLogin: Bool) {
        if isLogin {
            titleLabel.text = "Welcome Back,"
            descriptionLabel.text = "Login to continue"
        } else {
            titleLabel.text = "Hey, get on board"
            descriptionLabel.text = "Signup to continue"
        }
        setNeedsLayout()
    }
    
    func configurePasswordView() {
        titleLabel.text = "Forgot your Password?"
        descriptionLabel.text = "Please update to continue"
    }
}
