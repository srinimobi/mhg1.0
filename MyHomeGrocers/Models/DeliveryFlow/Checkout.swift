//
//  Checkout.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 12/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct CheckoutResponse {
    let statusCode: Int
    let message: String
    let data: (totals: NSArray,
               mhg_rewards: NSDictionary,
               shipping_methods: NSDictionary)
    //let total_savings: Double
}

extension CheckoutResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
              //let total_savings = json["total_savings"] as? Double
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let totals = dataJSON["totals"] as? NSArray,
              let mhg_rewards = dataJSON["mhg_rewards"] as? NSDictionary,
              let shipping_methods = dataJSON["shipping_methods"] as? NSDictionary
        else {
            return nil
        }
        
        let data = (totals,
                    mhg_rewards,
                    shipping_methods)
        
        self.statusCode = statusCode
        self.message = message
        //self.total_savings = total_savings
        self.data = data
    }
}
