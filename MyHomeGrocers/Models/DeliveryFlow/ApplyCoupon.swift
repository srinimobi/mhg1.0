//
//  ApplyCoupon.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 12/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct ApplyCouponResponse {
    let statusCode: Int
    let message: String
    let status: (String)
}

extension ApplyCouponResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["status"] as? [String: Any],
              let message1 = dataJSON["message"] as? String
        else {
            return nil
        }
        
        let status = (message1)
        
        self.statusCode = statusCode
        self.message = message
        self.status = status
    }
}
