//
//  SearchItem.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct SearchItemResponse {
    let statusCode: Int
    let message: String
    let data: (NSArray)
}

extension SearchItemResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let list = dataJSON["list"] as? NSArray
        else {
            return nil
        }
        
        let data = (list)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
