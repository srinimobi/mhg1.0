//
//  AccountDetails.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 09/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct AccountDetailsResponse {
    let statusCode: Int
    let message: String
    let data: (firstname: String,
               lastname: String,
               email: String,
               telephone: String)
}

extension AccountDetailsResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let firstname = dataJSON["firstname"] as? String,
              let lastname = dataJSON["lastname"] as? String,
              let email = dataJSON["email"] as? String,
              let telephone = dataJSON["telephone"] as? String
              //let scope = dataJSON["scope"] as? NSNull
        else {
            return nil
        }
        
        let data = (firstname,
                    lastname,
                    email,
                    telephone)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
