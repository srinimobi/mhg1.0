//
//  SignupVerifyOTP.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct SignupVerifyOTPResponse {
    let statusCode: Int
    let message: String
}

extension SignupVerifyOTPResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        self.statusCode = statusCode
        self.message = message
    }
}
