//
//  DeleteCartItem.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 11/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct DeleteCartItemResponse {
    let statusCode: Int
    let message: String
    let data: (total_product_count: Int,
               total_price: String)
}

extension DeleteCartItemResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let total_product_count = dataJSON["total_product_count"] as? Int,
              let total_price = dataJSON["total_price"] as? String
        else {
            return nil
        }
        
        let data = (total_product_count,
                    total_price)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
