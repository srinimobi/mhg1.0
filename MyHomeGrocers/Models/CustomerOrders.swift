//
//  CustomerOrders.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 09/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct CustomerOrdersResponse {
    let statusCode: Int
    let message: String
    let data: (orders: NSArray,
               total_pages: Int)
}

extension CustomerOrdersResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let orders = dataJSON["orders"] as? NSArray,
              let total_pages = dataJSON["total_pages"] as? Int
        else {
            return nil
        }
        
        let data = (orders,
                    total_pages)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}

// * Customer Dict Data From Array
struct CustomerOrderDict {
    let order_id: String
    let status: String
    let total: String
    let delivery_time: String
}

extension CustomerOrderDict {
    init?(json: [String: Any]) {
        guard let order_id = json["order_id"] as? String,
              let status = json["status"] as? String,
              let total = json["total"] as? String,
              let delivery_time = json["delivery_time"] as? String
        else {
            return nil
        }
        
        self.order_id = order_id
        self.status = status
        self.total = total
        self.delivery_time = delivery_time
    }
}
