//
//  SuccessResponse.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 09/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct SuccessResponse {
    let statusCode: Int
    let message: String
}

extension SuccessResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        self.statusCode = statusCode
        self.message = message
    }
}
