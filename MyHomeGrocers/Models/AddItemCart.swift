//
//  AddItemCart.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 11/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct AddItemCartResponse {
    let statusCode: Int
    let message: String
    let data: (total: String,
               totalItemsCart: Int,
               totalItemsCartAmount: String)
}

extension AddItemCartResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let total = dataJSON["total"] as? String,
              let totalItemsCart = dataJSON["totalItemsCart"] as? Int,
              let totalItemsCartAmount = dataJSON["totalItemsCartAmount"] as? String
        else {
            return nil
        }
        
        let data = (total,
                    totalItemsCart,
                    totalItemsCartAmount)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
