//
//  SignupEmail.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct SignupEmailResponse {
    let statusCode: Int
    let message: String
    let data: (access_token: String,
               expires_in: String,
               token_type: String)
}

extension SignupEmailResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let access_token = dataJSON["access_token"] as? String,
              let expires_in = dataJSON["expires_in"] as? String,
              let token_type = dataJSON["token_type"] as? String
              //let scope = dataJSON["scope"] as? NSNull
        else {
            return nil
        }
        
        let data = (access_token,
                    expires_in,
                    token_type)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
