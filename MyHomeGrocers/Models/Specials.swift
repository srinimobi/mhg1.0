//
//  Specials.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct SpecialsResponse {
    let statusCode: Int
    let message: String
    let data: (products: NSArray,
               count: String,
               page_number: String,
               per_page: String,
               total_pages: Int)
}

extension SpecialsResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let products = dataJSON["products"] as? NSArray,
              let count = dataJSON["count"] as? String,
              let page_number = dataJSON["page_number"] as? String,
              let per_page = dataJSON["per_page"] as? String,
              let total_pages = dataJSON["total_pages"] as? Int
        else {
            return nil
        }
        
        let data = (products,
                    count,
                    page_number,
                    per_page,
                    total_pages)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
