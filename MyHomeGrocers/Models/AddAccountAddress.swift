//
//  AddAccountAddress.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 06/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct AddAccountAddressResponse {
    let statusCode: Int
    let message: String
    //let data: (NSArray)
}

extension AddAccountAddressResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        self.statusCode = statusCode
        self.message = message
    }
}
