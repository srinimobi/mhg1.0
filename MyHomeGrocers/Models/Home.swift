//
//  Home.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct HomeResponse {
    let statusCode: Int
    let message: String
    let data: (banner: NSArray,
               categories: NSArray,
               products: NSDictionary,
               previously_ordered_products: NSArray?,
               cart_products: NSArray?,
               mhg_reward_balance: Double?)
}

extension HomeResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let banner = dataJSON["banner"] as? NSArray,
              let categories = dataJSON["categories"] as? NSArray,
              let products = dataJSON["products"] as? NSDictionary,
              let previously_ordered_products = dataJSON["previously_ordered_products"] as? NSArray?,
              let cart_products = dataJSON["cart_products"] as? NSArray?,
              let mhg_reward_balance = dataJSON["mhg_reward_balance"] as? Double?
        else {
            return nil
        }
        
        let data = (banner,
                    categories,
                    products,
                    previously_ordered_products,
                    cart_products,
                    mhg_reward_balance)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}

// * Product Dict Data From Array
struct ProductsDictResponse {
    let filters: NSDictionary
    let sorts: NSDictionary
    let new_arrivals: NSArray
    let in_offer: NSArray
}

extension ProductsDictResponse {
    init?(json: [String: Any]) {
        guard let filters = json["filters"] as? NSDictionary,
              let sorts = json["sorts"] as? NSDictionary,
              let new_arrivals = json["new_arrivals"] as? NSArray,
              let in_offer = json["in_offer"] as? NSArray
        else {
            return nil
        }
        
        self.filters = filters
        self.sorts = sorts
        self.new_arrivals = new_arrivals
        self.in_offer = in_offer
    }
}
