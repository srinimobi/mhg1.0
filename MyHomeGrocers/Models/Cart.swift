//
//  Cart.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 10/06/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct CartResponse {
    let statusCode: Int
    let message: String
    let data: (weight: String,
               products: NSArray,
               vouchers: NSArray,
               totals: NSArray,
               total_raw: NSDictionary,
               total_product_count: Int,
               currency: NSDictionary)
}

extension CartResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let weight = dataJSON["weight"] as? String,
              let products = dataJSON["products"] as? NSArray,
              let vouchers = dataJSON["vouchers"] as? NSArray,
              let totals = dataJSON["totals"] as? NSArray,
              let total_raw = dataJSON["total_raw"] as? NSDictionary,
              let total_product_count = dataJSON["total_product_count"] as? Int,
              let currency = dataJSON["currency"] as? NSDictionary
        else {
            return nil
        }
        
        let data = (weight,
                    products,
                    vouchers,
                    totals,
                    total_raw,
                    total_product_count,
                    currency)
                
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}
