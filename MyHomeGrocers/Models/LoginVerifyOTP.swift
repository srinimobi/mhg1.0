//
//  LoginVerifyOTP.swift
//  MyHomeGrocers
//
//  Created by srinivas yennamsetti on 23/05/21.
//  Copyright © 2021 Uma Maheswari. All rights reserved.
//

import Foundation

struct LoginverifyOTPResponse {
    let statusCode: Int
    let message: String
    let data: (customer_id: String,
               store_id: String,
               referral_code: String,
               access_token: String,
               expires_in: String,
               token_type: String)
}

extension LoginverifyOTPResponse {
    init?(json: [String: Any]) {
        guard let statusCode = json["statusCode"] as? Int,
              let message = json["message"] as? String
        else {
            return nil
        }
        
        guard let dataJSON = json["data"] as? [String: Any],
              let customer_id = dataJSON["customer_id"] as? String,
              let store_id = dataJSON["store_id"] as? String,
              let referral_code = dataJSON["referral_code"] as? String,
              let access_token = dataJSON["access_token"] as? String,
              let expires_in = dataJSON["expires_in"] as? String,
              let token_type = dataJSON["token_type"] as? String
        else {
            return nil
        }
        
        let data = (customer_id,
                    store_id,
                    referral_code,
                    access_token,
                    expires_in,
                    token_type)
        
        self.statusCode = statusCode
        self.message = message
        self.data = data
    }
}

/*
 {
     "statusCode": 1,
     "message": "Login successful",
     "data": {
         "customer_id": "4974",
         "customer_group_id": "1",
         "store_id": "0",
         "language_id": "1",
         "firstname": "Sasi",
         "lastname": "iOS",
         "email": "sasi.ios@gmail.com",
         "telephone": "9703103109",
         "telephone_old": null,
         "fax": "",
         "wishlist": [],
         "newsletter": "0",
         "address_id": "0",
         "ip": "160.238.75.125",
         "profile_image": "",
         "status": "1",
         "safe": "0",
         "code": "",
         "date_added": "2021-05-22 02:09:41",
         "approved": "0",
         "verified": "1",
         "refer_code": null,
         "referral_code": "23461099",
         "social_id": null,
         "social_provider": null,
         "telephone_iso2_code": "in",
         "telephone_dial_code": "91",
         "telephone_verified": "1",
         "telephone_skipped": "0",
         "delivery_directions": null,
         "pending_payment_waive": "0",
         "custom_fields": [],
         "account_custom_field": null,
         "wishlist_total": "0",
         "cart_count_products": 0,
         "access_token": "7e5c44d80d6f608915fd51dd332a724221674b05",
         "expires_in": "2628000",
         "token_type": "Bearer",
         "scope": null
     }
 }
 */
