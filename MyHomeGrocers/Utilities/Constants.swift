//
//  Constants.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/3/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import Foundation
import UIKit

let deviceFrame : CGRect = UIScreen.main.bounds
let appDel = UIApplication.shared.delegate as! AppDelegate
let appDelegate = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
let appTitle = "MyHomeGrocers"
