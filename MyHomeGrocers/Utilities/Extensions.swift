//
//  Extensions.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 9/2/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    class func customOrange() -> UIColor {
        return UIColor(red:255/255, green:92/255 ,blue:1/255 , alpha:1.00)
    }
    
    class func customGray() -> UIColor {
        return UIColor(red:115/255, green:115/255 ,blue:115/255 , alpha:1.00)
    }
    
    class func descriptionGray() -> UIColor {
        return UIColor(red:112/255, green:112/255 ,blue:112/255 , alpha:1.00)
    }
    
    class func lightOrange() -> UIColor {
        return UIColor(red:255/255, green:224/255 ,blue:207/255 , alpha:1.00)
    }
    
    class func termsColor() -> UIColor {
        return UIColor(red:9/255, green:9/255 ,blue:9/255 , alpha:1.00)
    }
}
