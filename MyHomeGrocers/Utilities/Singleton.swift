//
//  Singleton.swift
//  Apartment
//
//  Created by Uma Maheswari on 10/28/20.
//  Copyright © 2020 switch. All rights reserved.
//

import Foundation
import MBProgressHUD

class Singleton {
    static var sharedInstance = Singleton()
    private init() {}

    var authorizationKey: String?
}

var selectedAddress = "Delivering to Delivery area, with flat name, pin code, city etc.,"

extension UIViewController {

    func showHUD(){
        DispatchQueue.main.async{
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }

    func dismissHUD(isAnimated:Bool) {
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: isAnimated)
        }
    }
}
