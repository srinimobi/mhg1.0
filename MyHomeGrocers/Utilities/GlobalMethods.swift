//
//  GlobalMethods.swift
//  Created by Uma B on 6/21/18.
//  Copyright © 2018 Uma B. All rights reserved.
//

import UIKit

class GlobalMethods: NSObject {
    class func  getViewController(storyBoardID : NSString) -> UIViewController{
        let storyboard :UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        return  storyboard.instantiateViewController(withIdentifier: storyBoardID as String)
    }
    
    class func getIndexPathOf(txtFld:UITextField,tbl:UITableView) -> IndexPath {
        let point = txtFld.superview?.convert(CGPoint(x: 0,y: txtFld.frame.origin.y), to: tbl)
        return tbl.indexPathForRow(at: point!)!
    }
    
    class func dynamicTableViewHeight(tableView:UITableView)  {
        tableView.estimatedRowHeight = 400
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    class func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func isValidPhoneNumber(phoneNumber: String) -> Bool {
//        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
//        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
//        let result = phoneTest.evaluate(with: phoneNumber)
//        return result
        
        let numbersSet = CharacterSet(charactersIn: "0123456789")

        let textCharacterSet = CharacterSet(charactersIn: phoneNumber)

        if textCharacterSet.isSubset(of: numbersSet) {
            return true
        } else {
            return false
        }
    }
    
    class func getImageUrl(result:NSDictionary)-> String{
        if let aee = result.value(forKeyPath: "result.files.file") as? NSArray , aee.count > 0 , let details = aee.object(at: 0) as? NSDictionary{
            return "/containers/users/download/"+(details.value(forKey: "name") as! String)
        }
        return ""
    }
    
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let size = image.size.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    // * NSUserDefaults
    class func saveAuthKey(authKey: String) {
        UserDefaults.standard.set(authKey, forKey: "AuthKey")
    }
    
    class func retriveAuthKey() -> String {
        return UserDefaults.standard.string(forKey: "AuthKey")!
    }
    
    class func removeUserDefaults() {
        UserDefaults.standard.removeObject(forKey: "AuthKey")
    }
}
