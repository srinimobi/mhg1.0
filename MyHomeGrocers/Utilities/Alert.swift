//
//  Alert.swift
//  MyHomeGrocers
//
//  Created by Uma Maheswari on 10/3/20.
//  Copyright © 2020 Uma Maheswari. All rights reserved.
//

import Foundation
import UIKit

class Alert: NSObject {
    class func showAlert(message:String) {
        let alert = UIAlertController(title: appTitle , message: message , preferredStyle: .alert)
        let okAction = (UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(okAction)
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    class func showAlertforOkAction(message:String,okAction:UIAlertAction) {
        let alert = UIAlertController(title: appTitle , message: message , preferredStyle: .alert)
        alert.addAction(okAction)
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    class func showWithActions(title:String,message:String,okAction:UIAlertAction,cancelAction:UIAlertAction)  {
        let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    class func showAlertForLogout(okAction:UIAlertAction , vc : UIViewController){
        let cancelAction = (UIAlertAction(title: "NO", style: .cancel, handler: nil))
        let alert = UIAlertController(title: appTitle, message: "Are you sure, do you want to Logout?" , preferredStyle: .alert)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        vc.present(alert, animated: true, completion: nil)
    }
    class func showAlertForCurrentLocation(){
        let okAction = (UIAlertAction(title: "Settings", style: .default, handler: {
            (alert: UIAlertAction!) in
            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
        }))
        let cancelAction = (UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.showWithActions(title: "Location Services", message: "Please enable your location Services to continue.", okAction: okAction, cancelAction: cancelAction)
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

